
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%								%%%%%%
		%%%%%%		  PROGRAMME PRINCIPAL DECODEUR MICDA :		%%%%%%
        %%%%%%								%%%%%%
        %%%%%%             Debits 16, 24, 32 et 40 kbits/s              %%%%%%
        %%%%%%								%%%%%%
		%%%%%%		     Recommandation CCITT G726			%%%%%%
		%%%%%%								%%%%%%
        %%%%%%             Francois PINIER ( Avril 1995 )		%%%%%%
        %%%%%%								%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        REMARQUE :
        ----------

                RAZ = 1    ===> Phase d'initialisation du decodeur
                RAZ = 0    ===> Fonctionnement normal du decodeur

*/

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%	     Declaration du paquetage global des modules	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

#include "g726common.h"
#include "g726.h"

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%		Definition des valeurs initiales de la		%%%
	%%%			recommandation CCITT G726		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

#define A1_INIT  	0
#define A2_INIT  	0
#define B_INIT   	0
#define AP_INIT  	0
#define DML_INIT 	0
#define DMS_INIT 	0
#define DQ_INIT  	32
#define PK1_INIT 	0
#define PK2_INIT 	0
#define SR1_INIT 	32
#define SR2_INIT 	32
#define TD_INIT  	0
#define YL_INIT  	34816			
#define YU_INIT  	544

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%		Definition des variables du decodeur MICDA	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

static int A1, A2, AP, DML, DMS, TD, YL, YU;

static int A1R  = A1_INIT;
static int A2R  = A2_INIT;
static int APR  = AP_INIT;
static int DMLP = DML_INIT;
static int DMSP = DMS_INIT;
static int DQ0  = DQ_INIT;
static int TDR  = TD_INIT;
static int YLP  = YL_INIT;
static int YUP  = YU_INIT;

static int LOI   = LAW_NONE;
static int DEBIT = DEBIT_32KBS;

static TABLEAU3 PK       = { PK1_INIT, PK2_INIT, 0 };
static TABLEAU3 SR       = { SR1_INIT, SR2_INIT, 0 };
static TABLEAU6 B;
static TABLEAU6 DQ_DELAY = { DQ_INIT, DQ_INIT, DQ_INIT, DQ_INIT, DQ_INIT, DQ_INIT };
static TABLEAU6 BR	 = { B_INIT,  B_INIT,  B_INIT,  B_INIT,  B_INIT,  B_INIT  };

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%		Architecture du decodeur MICDA conforme a la	%%%
		%%%		recommandation CCITT G726			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	=========================================================================================================
	|	Les calculs des blocs "RETARD" s'effectuent simultanement, et les calculs des differentes	|
	|	variables s'effectuent a partir des valeurs initiales issues de ces blocs "RETARD"		|
	|	conformement au paragraphe 4 de la recommandation CCITT G726.					|
	=========================================================================================================

*/
int InitDeCodeurG726 (int newLOI,int newDEBIT)
{
	int J;

	if (newLOI != LAW_NONE && newLOI != LAW_A && newLOI != LAW_MU)
		return 0;

	LOI = newLOI;

	if (newDEBIT != DEBIT_16KBS && newDEBIT !=DEBIT_24KBS &&
		newDEBIT != DEBIT_32KBS && newDEBIT !=DEBIT_40KBS)
		return 0;

	DEBIT = newDEBIT;

    YL    = YL_INIT;		YU    = YU_INIT;	DML   = DML_INIT;	DMS   = DMS_INIT;
    AP    = AP_INIT;		TD    = TD_INIT;	PK[1] = PK1_INIT;	PK[2] = PK2_INIT;
    SR[1] = SR1_INIT;		SR[2] = SR2_INIT;	A1    = A1_INIT;	A2    = A2_INIT;
    for (J = 0; J < 6; J++)
	{
		 DQ_DELAY[J] = DQ_INIT;
		 B[J]        = B_INIT;
	}
	return 1;
}

int DeCodeurG726(int ENTREE,int iLOI,int iDEBIT,int RAZ)
{
	if (RAZ)
		InitDeCodeurG726 (iLOI, iDEBIT);
	else {
		LOI = iLOI;
		DEBIT = iDEBIT;
	}
	return QuickDeCodeurG726(ENTREE);
}

int QuickDeCodeurG726(int ENTREE)
{
	TABLEAU6 BP, U, WB;
	int	 A1P, A1T, A2P, A2T, AL, APP, AX, DLX, DLNX, DQ, DQL, DQLN, DQS, DSX, DX, FI, J,
		 SE, SEZ, SIGPK, SLX, SORTIE, SP, SRFB, TDP, TR, WA1, WA2, WI, Y, YUT;
/*
	if (RAZ)				
	{
	     YL    = YL_INIT;		YU    = YU_INIT;	DML   = DML_INIT;	DMS   = DMS_INIT;
	     AP    = AP_INIT;		TD    = TD_INIT;	PK[1] = PK1_INIT;	PK[2] = PK2_INIT;
	     SR[1] = SR1_INIT;		SR[2] = SR2_INIT;	A1    = A1_INIT;	A2    = A2_INIT;
	     for (J = 0; J < 6; J++)
		{
		 DQ_DELAY[J] = DQ_INIT;
		 B[J]        = B_INIT;
		}
	    }
	else */

		/* Phase d'exploitation du decodeur */
	    {
	     YL    = YLP;		YU    = YUP;		DML   = DMLP;		DMS   = DMSP;
	     AP    = APR;		TD    = TDR;		PK[2] = PK[1];		PK[1] = PK[0];
	     SR[2] = SR[1];		SR[1] = SR[0];		A1    = A1R;		A2    = A2R;
	     for (J = 5; J > 0; J--)
		DQ_DELAY[J] = DQ_DELAY[J - 1];
	     DQ_DELAY[0] = DQ0;
	     for (J = 0; J < 6; J++)
		B[J] = BR[J];
	    }
	WA1 = FMULT(A1, SR[1]);
	WA2 = FMULT(A2, SR[2]);
	for (J = 0; J < 6; J++)
	    WB[J] = FMULT(B[J], DQ_DELAY[J]);
	ACCUM(WA1, WA2, WB, &SE, &SEZ);
	AL   = LIMA(AP);
	Y    = MIX(AL, YU, YL);
	WI   = FUNCTW(ENTREE, DEBIT);
	YUT  = FILTD(WI, Y);
	YUP  = LIMB(YUT);
	YLP  = FILTE(YUP, YL);
	FI   = FUNCTF(ENTREE, DEBIT);
	DMSP = FILTA(FI, DMS);
	DMLP = FILTB(FI, DML);
	RECONST(ENTREE, &DQLN, &DQS, DEBIT);
	DQL = ADDA(DQLN, Y);
	DQ  = ANTILOG(DQL, DQS);
	TR  = TRANS(TD, YL, DQ);
	ADDC(DQ, SEZ, &PK[0], &SIGPK);
	A2T   = UPA2(PK[0], PK[1], PK[2], A1, A2, SIGPK);
	A2P   = LIMC(A2T);
	A2R   = TRIGB(TR, A2P);
	A1T   = UPA1(PK[0], PK[1], A1, SIGPK);
	A1P   = LIMD(A1T, A2P);
	A1R   = TRIGB(TR, A1P);
	TDP   = TONE(A2P);
	AX    = SUBTC(DMSP, DMLP, TDP, Y);
	APP   = FILTC(AX, AP);
	APR   = TRIGA(TR, APP);
	TDR   = TRIGB(TR, TDP);
	SRFB  = ADDB(DQ, SE);
	SR[0] = FLOATB(SRFB);

	SP    = COMPRESS(SRFB, LOI);
	SLX   = EXPAND(SP, LOI);
	DX    = SUBTA(SLX, SE);
	LOG(DX, &DLX, &DSX);
	DLNX   = SUBTB(DLX, Y);
	SORTIE = SYNC(ENTREE, SP, DLNX, DSX, LOI, DEBIT);
	DQ0    = FLOATA(DQ);
	for (J = 0; J < 6; J++)
	    {
	     U[J]  = XOR(DQ_DELAY[J], DQ);
	     BP[J] = UPB(U[J], B[J], DQ, DEBIT);
	     BR[J] = TRIGB(TR, BP[J]);
	    }

	// Uniform PCM without law encoding
	if (LOI==LAW_NONE) 
		return LIMO(SRFB);
	else
		return SORTIE;
    }
