#ifndef _GLOBAL_H
#define _GLOBAL_H

// BOOL definition
typedef int BOOLEAN;
#define	TRUE 1
#define FALSE 0

// ERROR defines
#define	   SUCCESS							0x00
#define	   ERR_PREMATURE_EOF				0x01
#define		ERR_P_EOF_POST_MARK			0x01
#define		ERR_P_EOF_CHANGE_GRP			0x02
#define		ERR_P_EOF_DATA_MISSING		0x03
#define		ERR_P_EOF_SEARCH_KEY			0x04
#define     ERR_INTERNAL					0x02
#define		ERR_INT_BAD_GRP_FILE_NAME	0x01
#define		ERR_INT_MAIN_ERROR			0x02
#define     ERR_POST						   0x03
#define		ERR_POST_WAV_FILE_NAME		0x01
#define		ERR_POST_NOT_WAVE				0x02
#define		ERR_POST_MARK_TOO_LONG		0x03
#define		ERR_POST_WAV_LONG_NAME		0x04
#define		ERR_POST_WAV_SIZE				0x05
#define		ERR_POST_FORMAT				0x06
#define		ERR_POST_ENTITY_NUM			0x07
#define     ERR_IO							0x04
#define		ERR_FILE_LOCKED				0x01
#define		ERR_FILE_ON_LOCK				0x02
#define     ERR_FILE_MISSING_FOR_RESOURCE_ID  0x03

/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
#define     ERR_POST_UNKNOWN_FEATURE   0x038

#define	   API_SUCCESS							   0x00
#define	   API_ERR_PREMATURE_EOF				0x01
#define		API_ERR_P_EOF_POST_MARK				0x011
#define		API_ERR_P_EOF_CHANGE_GRP			0x012
#define		API_ERR_P_EOF_DATA_MISSING			0x013
#define		API_ERR_P_EOF_SEARCH_KEY			0x014
#define     API_ERR_INTERNAL					   0x02
#define		API_ERR_INT_BAD_GRP_FILE_NAME		0x021
#define		API_ERR_INT_MAIN_ERROR				0x022
#define     API_ERR_POST						   0x03
#define		API_ERR_POST_WAV_FILE_NAME			0x031
#define		API_ERR_POST_NOT_WAVE				0x032
#define		API_ERR_POST_MARK_TOO_LONG			0x033
#define		API_ERR_POST_WAV_LONG_NAME			0x034
#define		API_ERR_POST_WAV_SIZE				0x035
#define		API_ERR_POST_FORMAT					0x036
#define		API_ERR_POST_ENTITY_NUM				0x037
#define     API_ERR_POST_UNKNOWN_FEATURE     0x038
#define     API_ERR_POST_UNKNOWN_OPERATION   0x039

#define     API_ERR_IO							   0x04
#define		API_ERR_FILE_LOCKED					0x041
#define		API_ERR_FILE_ON_LOCK				   0x042
#define	   API_ERR_MISSING_FILE             0x043
#define	   API_ERR_MEMALLOC                 0x044
#define	   API_ERR_REMOVING_FILE            0x045
#define	   API_ERR_STDOUT                   0x046
#define	   API_ERR_READING_FILE             0x047
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */


// Other defines
#define PCM_SIZE_PER_MINUTE			480000	// 8000bytes/s * 60seconds
#define MAX_FILE_NAME_LENGTH			255
#define MAX_POST_MARK_LENGTH			255
#define MAX_LINE_LENGTH					1024
#define MAX_HTML_FILE_NAME_LENGTH		255
#define MAX_HTML_LINE					2048
#define MAX_MAIN_ERROR_STRING_LENGTH	1024

#define DEBUG							1		// Debug on

/* @BD@OXO@CROXOC-4249@191009@degtoun1@ */
 #define REVERSE_ENDIANISM		// Define for PowerPC
/* @ED@OXO@CROXOC-4249@191009@degtoun1@ */

#endif
