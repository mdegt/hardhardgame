# Local Makefile (Initially Generated)
# vim: noet list:

ifeq ($(TARGET),oxo)

include  $(TOOLDIR)/_header.mk
-include $(TOOLDIR)/$(PROJECT_COMP)_header.mk

# Common Flags to C and c++
$(_MODULE)_CPPFLAGS:=
# Flags to C and c++
$(_MODULE)_CFLAGS  :=
# Flags for c++
$(_MODULE)_CXXFLAGS:=-I$(_MODULE)/
LSRCS:=$(notdir $(wildcard $(_MODULE)/*.cc))
BINARY:=wave.cgi
#User specific and local rules Go Here
# We use local rules because  we don t want the objects be part of the lp_cm

$(_MODULE)_OBJS := $(addsuffix .o,$(addprefix $($(_MODULE)_OUTPUT)/,$(basename $(LSRCS))))
$(_MODULE)_SRCS := $(addprefix $(_MODULE)/, $(LSRCS))
$(_MODULE)_BINARY :=$(addprefix $($(_MODULE)_OUTPUT)/,$(BINARY))

# @BM@OXOC_3.0@CROXOC-000@170412@leboudec@
$($(_MODULE)_OUTPUT)/%.o: $(_MODULE)/%.cc $($(_MODULE)_OUTPUT)/.f
	@echo -e "Building g++ $@"
	@echo -e "-----------------"
	$(Q)$(COMPILE.cc) $(CC_PROJECT_CONFIG)  $($(<D)_CXXFLAGS) $($(call module-from-path ,$<)_CXXFLAGS) $($(<D)_INCLUDE) -o $@ $<
# @EM@OXOC_3.0@CROXOC-000@170412@leboudec@

$($(_MODULE)_OUTPUT)/wave.cgi:$($(_MODULE)_OBJS) $($(_MODULE)_OUTPUT)/.f
	$(LINK.cc) -o $@ $(filter %.o,$^) -lstdc++

build:: $($(_MODULE)_BINARY)
# allows use to call make call_handling/CM_SYS/....
$(_MODULE): $($(_MODULE)_BINARY)


# Do not remove below
ifneq ($(strip $(SRCS) $(HSRCS)),)
include $(TOOLDIR)/_footer.mk
-include $(TOOLDIR)/$(PROJECT_COMP)_footer.mk
else
SRCS:=
BINARY:=
KSRCS:=
HSRCS:=
endif
endif
