
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%								%%%%%%
		%%%%%%		   DESCRIPTION DES MODULES MICDA :		%%%%%%
        %%%%%%								%%%%%%
		%%%%%%		   Debits 16, 24, 32 et 40 kbits/s		%%%%%%
        %%%%%%								%%%%%%
		%%%%%%		      Recommandation CCITT G726			%%%%%%
		%%%%%%								%%%%%%
        %%%%%%               Francois PINIER ( Avril 1995 )		%%%%%%
        %%%%%%								%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	REMARQUES :
	-----------

	   1)
		LAW = 1    ===> Codage en loi A
		LAW = 0    ===> Codage en loi mu

		Seuls les modules suivants sont fonction de la loi de codage:

			* COMPRESS
			* EXPAND
			* SYNC

	   2)
		DEBIT = 40    ===> Debit MICDA = 40 kbits/s
		DEBIT = 32    ===> Debit MICDA = 32 kbits/s
		DEBIT = 24    ===> Debit MICDA = 24 kbits/s
		DEBIT = 16    ===> Debit MICDA = 16 kbits/s

		Seuls les modules suivants sont fonction du debit MICDA :

			* FUNCTF
			* FUNCTW
			* QUAN
			* RECONST
			* SYNC
			* UPB

	   3) Le module QUAN est utilise uniquement dans le codeur

	   4) Les modules COMPRESS et SYNC sont utilises uniquement dans le decodeur

*/

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%		Definition des types et constantes		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

#define puissance17	131072
#define puissance16	65536
#define puissance15	32768
#define puissance14	16384

#define MAX_17bits	131071
#define MAX_16bits	65535
#define MAX_15bits	32767
#define MAX_14bits	16383

#define A2UL		12288		/* Parametres de la fonction LIMC */
#define A2LL		53248

#define OME		15360		/* Parametre de la fonction LIMD */



typedef int TABLEAU3[3];		/* Definition d'un tableau contenant trois entiers */
typedef int TABLEAU6[6];		/* Definition d'un tableau contenant six entiers   */



/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du code MIC loi A ou             %%%
		%%%     loi mu en code MIC uniforme (linearisation)		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int EXPAND(int S, int LAW);
   
    

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de calcul de la difference par soustraction    %%%
		%%%     du signal estime et du signal d'entree (ou du signal    %%%
		%%%     reconstitue quantifie dans le decodeur)			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int SUBTA(int SL,int SE);
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du signal de difference du       %%%
		%%%     domaine lineaire au domaine logarithmique		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

void LOG(int D,int *DL,int  *DS);
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de quantification du signal de difference      %%%
		%%%     dans le domaine logarithmique				%%%
		%%%		( Utilise dans le codeur uniquement )		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int QUAN(int DLN,int DS,int DEBIT);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de cadrage de la version logarithmique du      %%%
		%%%     signal de difference en soustrayant le facteur		%%%
		%%%     d'echelle						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int SUBTB(int DL,int  Y);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'addition du facteur d'echelle a la version   %%%
		%%%     logarithmique du signal de difference quantifie		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int ADDA(int DQLN,int  Y);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du signal de difference          %%%
		%%%     quantifie du domaine logarithmique dans le domaine	%%%
		%%%     lineaire						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int ANTILOG(int DQL,int  DQS);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de reconstitution du signal de difference      %%%
		%%%     quantifie dans le domaine logarithmique			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

void RECONST(int I,int  *DQLN,int  *DQS,int  DEBIT);
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation du facteur d'echelle           %%%
		%%%     a adaptation rapide					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTD(int WI,int  Y);
   
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation du facteur d'echelle           %%%
		%%%     a adaptation lente					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTE(int YUP,int  YL);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de transposition de la valeur de sortie du     %%%
	%%%     quantificateur dans le domaine logarithmique du		%%%
	%%%     multiplicateur du facteur d'echelle			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FUNCTW(int I,int  DEBIT);
    
    

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de limitation du facteur d'echelle du          %%%
		%%%     quantificateur						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int LIMB(int YUT);
int LIMO(int SR);

    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de formation d'une combinaison lineaire des    %%%
		%%%     facteurs d'echelle a adaptation rapide et lente du	%%%
		%%%	quantificateur						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int MIX(int AL,int  YU,int  YL);
   
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation de la moyenne a court terme    %%%
		%%%     de F(I)							%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTA(int FI,int  DMS);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation de la moyenne a long terme     %%%
		%%%     de F(I)							%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTB(int FI,int  DML);
  
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de filtrage passe-bas du parametre de	    	%%%
		%%%     controle de la vitesse					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTC(int AX,int  AP);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de projection de la valeur de sortie du	%%%
		%%%     quantificateur sur la fonction F(I)			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FUNCTF(int I,int  DEBIT);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de limitation du parametre de controle		%%%
		%%%     de la vitesse						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int LIMA(int AP);
  
    

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de calcul de la valeur absolue de la	    	%%%
		%%%     difference des fonctions a court et a long terme de la	%%%
		%%%	suite des codes MICDA puis de realisation des		%%%
		%%%	comparaisons de seuils pour actualiser le parametre de	%%%
		%%%	controle de la vitesse d'adaptation du quantificateur	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int SUBTC(int DMSP,int  DMLP,int  TDP,int  Y);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de declenchement du controle de la vitesse    	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int TRIGA(int TR,int  APP);
    
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'addition des sorties du predicteur pour	%%%
		%%%     former la valeur estimee partielle du signal		%%%
		%%%	(a partir du predicteur du sixieme ordre) et la valeur	%%%
		%%%	estimee du signal					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

void ACCUM(int WA1, int WA2,TABLEAU6 WB,int *SE,int *SEZ);
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'addition du signal de difference quantifie   %%%
	%%%     et de la valeur estimee du signal pour former le	%%%
	%%%	signal reconstitue					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int ADDB(int DQ,int  SE);

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de calcul du signe de la somme du signal de    %%%
	%%%     difference quantifie et de la valeur estimee partielle	%%%
	%%%	du signal						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

void ADDC(int DQ,int  SEZ,int  *PK0,int  *SIGPK);

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du code amplitude-signe a	%%%
		%%%     16 bits en code virgule flottante			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FLOATA(int DQ);
   
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du code complement a deux    	%%%
		%%%     (16 bits) en code virgule flottante			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FLOATB(int SR);
   
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de multiplication des coefficients de    	%%%
		%%%     prediction par le signal de difference quantifie ou le	%%%
		%%%	signal reconstitue correspondant			%%%
		%%%	(la multiplication est effectuee en virgule flottante)	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FMULT(int AN,int  SRN);
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de limitation du coefficient A2 du predicteur  %%%
		%%%     du second ordre						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int LIMC(int A2T);
    

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de limitation du coefficient A1 du predicteur  %%%
		%%%     du second ordre						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int LIMD(int A1T,int  A2P);
    

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de declenchement du predicteur  		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int TRIGB(int TR,int  ANP);
    
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation du coefficient A1 du  		%%%
		%%%     predicteur du second ordre				%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int UPA1(int PK0,int  PK1,int  A1,int  SIGPK);

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation du coefficient A2 du  		%%%
		%%%     predicteur du second ordre				%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */
	
int UPA2(int PK0,int  PK1,int  PK2,int  A1,int  A2,int  SIGPK);

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation des coefficients du  		%%%
		%%%     predicteur du sixieme ordre				%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int UPB(int UN,int  BN,int  DQ,int  DEBIT);

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction "OU EXCLUSIF" a un bit du signe du signal de  	%%%
		%%%     difference et du signe du signal de difference retarde	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int XOR(int DQN,int  DQ);

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de detection de transition	 		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int TRANS(int TD,int  YL,int DQ);


/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de detection du signal a bande etroite  	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int TONE(int A2P);

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du format MIC uniforme en  	%%%
		%%%     format MIC loi A ou loi mu ===> compression		%%%
		%%%	     ( Utilise dans le decodeur uniquement )		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int COMPRESS(int SR,int  LAW);

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de recodage de l'echantillon MIC de sortie  	%%%
		%%%     dans le decodeur pour le codage synchrone en cascade	%%%
		%%%			( Utilise dans le decodeur uniquement )		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int SYNC(int I,int  SP,int  DLNX,int  DSX,int  LAW,int  DEBIT);
