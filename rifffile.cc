/* rifffile.cpp

	Copyright (c) 1996, 1988 by Timothy J. Weber.

	See rifffile.txt for documentation.
*/
#include <string.h> /* Avoid compiler error for strcmp */
#include "rifffile.h"
#include <iostream>
#include "global.h"
#include "proto.h"
#include "extern.h"
/* @BA@OXO@CROXOC-6592@191009@degtoun1@ */
//#include <linux/kernel.h>
//#include <asm/byteorder.h>
/* @EA@OXO@CROXOC-6592@191009@degtoun1@ */

using namespace std;
void memory_represent(char *value, int n) {
   int i;
   for (i = 0; i < n; i++)
      print_debug(" %.2x", value[i]);
}

/***************************************************************************
	macros and constants
***************************************************************************/

// define REVERSE_ENDIANISM if the endianism of the host platform is not Intel
// (Intel is little-endian)
#ifdef REVERSE_ENDIANISM
   #define SWAP_32(int32) (  \
 	(((int32) & 0x000000FFL) << 24) +  \
 	(((int32) & 0x0000FF00L) << 8) +  \
 	(((int32) & 0x00FF0000L) >> 8) +  \
 	(((int32) & 0xFF000000L) >> 24))
#endif

/***************************************************************************
	typedefs and class definitions
***************************************************************************/

/***************************************************************************
	prototypes for static functions
***************************************************************************/

/***************************************************************************
	static variables
***************************************************************************/
 static string ValueExtraType[numExtraTypes];

/***************************************************************************
	member functions for RiffFile
***************************************************************************/

RiffFile::RiffFile(const char *name, char *sender):
	fp(stdin/*fopen(name, "rb")*/)	// eba_
{
   print_debug("RiffFile-1: %s - %s", name, sender);

	if (fp && !rewind()) {

      print_debug("RiffFile-1: closing  %p ", fp);

		fclose(fp);
		fp = 0;
	}
	// eba_
	// Check size
}

RiffFile::~RiffFile()
{
	if (fp)
		fclose(fp);
}

bool RiffFile::rewind()
{
	// clear the chunk stack
	while (!chunks.empty())
		chunks.pop();

	// rewind to the start of the file
	/* // eba_
	if (fseek(fp, 0, SEEK_SET))
		return false;
	*/ // eba_

	// look for a valid RIFF header
	RiffChunk topChunk(*this);

	if (feof(fp) || strcmp(topChunk.name, "RIFF"))
		return false;

	// found; push it on the stack, and leave the put pointer in the same place
	// as the get pointer.
   //print_debug( "topChunk.size=%lu", topChunk.size );

   formSize = topChunk.size;
	chunks.push(topChunk);
	return true;
}

bool RiffFile::push(const char* chunkType)
{
	// eba_
/* @BA@OXO_8.2@crms00443931@130710@vskrishn@ */
	std::string dummy;
/* @EA@OXO_8.2@crms00443931@130710@vskrishn@ */
/* @BD@OXO_8.2@crms00443931@130710@vskrishn@ */
       // string dummy[1]; 
//	unsigned long counter;
/* @ED@OXO_8.2@crms00443931@130710@vskrishn@ */
	// eba_
	// can't descend if we haven't started out yet.
	if (chunks.empty())
		return false;

	// first, go to the start of the current chunk, if we're looking for a named
	// chunk.
	/* // eba_
	if (chunkType)
		if (fseek(fp, chunks.top().start, SEEK_SET))
			return false;
	*/ // eba_

	// read chunks until one matches or we exhaust this chunk
	while (!feof(fp) /*&& ftell(fp) < chunks.top().after*/) {	// eba_
		RiffChunk chunk(*this);

		if (!feof(fp)) {
			// see if the subchunk type matches
			if (!chunkType || strcmp(chunk.name, chunkType) == 0) {
				// found; synchronize the put pointer, push the chunk, and succeed
				chunks.push(chunk);
				return true;
			} else {
				// not found; go to the next one.
				/* // eba_
				if (fseek(fp, chunk.after, SEEK_SET))
					return false;
				*/ // eba_
				// eba_
/* @BD@OXO_8.2@crms00443931@130710@vskrishn@ */

			//	for( counter = 0; counter < chunk.size ; counter++ )
                                      //fread(dummy,1,1,fp);
/* @ED@OXO_8.2@crms00443931@130710@vskrishn@ */
/* @BA@OXO_8.2@crms00443931@130710@vskrishn@ */
                                        dummy.resize(chunk.size,'\0');
					cin.read((char*)dummy.c_str(), chunk.size);
/* @EA@OXO_8.2@crms00443931@130710@vskrishn@ */
				// eba_
			}
		}
	}

	// couldn't find it; synchronize the put pointer and return error.
	/* // eba_
	fseek(fp, chunks.top().start, SEEK_SET);
	*/ // eba_
	return false;
}

bool RiffFile::pop()
{
	// if we've only got the top level chunk (or not even that), then we can't
	// go up.
	if (chunks.size() < 2)
		return false;

	// Position the get and put pointers at the end of the current subchunk.
	/* // eba_
	fseek(fp, chunks.top().after, SEEK_SET);
	*/ // eba_

	// Pop up the stack.
	chunks.pop();
	return true;
}

long RiffFile::chunkSize() const
{
	if (!chunks.empty())
		return chunks.top().size;
	else
		return 0;
}

const char* RiffFile::chunkName() const
{
	if (!chunks.empty())
		return chunks.top().name;
	else
		return 0;
}

const char* RiffFile::subType() const
{
	if (!chunks.empty() && chunks.top().subType[0])
		return chunks.top().subType;
	else
		return 0;
}

bool RiffFile::getNextExtraItem(string& type, string& value)
/* ORIGINAL CODE
 {
	// if the current chunk is LIST/INFO, then try to read another subchunk.
	if (strcmp(chunkName(), "LIST") == 0
		&& strcmp(subType(), "INFO") == 0)	{
		if (push()) {
			if (readExtraItem(type, value))
				return true;
			else				// unrecognized type.  Continue on.
				return getNextExtraItem(type, value);
		} else {
			// got to the end of the LIST/INFO chunk.  Pop back out and continue
			// looking.
			pop();
			return getNextExtraItem(type, value);
		}
	// we're not in a LIST/INFO chunk, so look for the next DISP or LIST/INFO.
	} else {
		push();

		if (strcmp(chunkName(), "DISP") == 0) {
			// DISP chunk: read and pop back out.
			return readExtraItem(type, value);
		} else if (strcmp(chunkName(), "LIST") == 0
			&& strcmp(subType(), "INFO") == 0)
		{
			// LIST/INFO chunk: read first element
			return getNextExtraItem(type, value);
		} else {
			// Some other chunk.  Pop back out and move on.
			if (pop())
				return getNextExtraItem(type, value);
			else
				return false;
		}
	}
}
*/

 {
	bool retcode;
	// if the current chunk is LIST/INFO, then try to read another subchunk.
	if (strcmp(chunkName(), "LIST") == 0
		&& strcmp(subType(), "INFO") == 0)
	{
		retcode = (push() && readExtraItem(type, value));
		pop();
		    // if current subchunk not known 
			// got to next up to the end of the LIST/INFO chunk.
		return retcode?true:getNextExtraItem(type, value);

		// we're not in a LIST/INFO chunk, so look for the next LIST/INFO.
	} else {
		retcode = (push() && getNextExtraItem(type, value));
		pop();
		return retcode;
		
	}
}


// Reads extra data from the current chunk.
// check if subchunk type is known and
// return its label in type and value in value 
bool RiffFile::readExtraItem(string& type, string& value)
{
	int i;			// eba_
	// see if it's one we recognize
	bool found = false;
	type = chunkName();
	for (/*int */i = 0; i < numExtraTypes; i++) {		// eba_
		if (strcmp(type.c_str(), extraTypes[i].typeName) == 0) {
			type = extraTypes[i].realName;
			found = true;
			break;
		}
	}

	// DISP chunks skip four bytes before the display name starts.
	/*
	if (strcmp(chunkName(), "DISP") == 0) {
		fgetc(filep());
		fgetc(filep());
		fgetc(filep());
		fgetc(filep());
	}
	*/

	// read the value, if we recognize the type
	if (found) {
		int c;
		value = "";
		while ((c = fgetc(filep())) != '\0' && c != EOF)
			value += char(c);
		// JLB Store actual value of Extra information
		ValueExtraType[i] = value;
	}

	// whether we recognize it or not, pop back out.
	// JLB pop(); transfered in GetNextExtraItem
	return found;
}

bool RiffFile::GetExtraItemOfType(const string& type, string& value)
{
	for (int i = 0; i < numExtraTypes; i++) {
		if (strcmp(type.c_str(), extraTypes[i].typeName) == 0) {
			value = ValueExtraType[i];
			return !value.empty();
		}
	}
	return false;
}

bool RiffFile::SetExtraItem(const std::string& type, std::string& value)
{
	if (value.size() <512)
	  for (int i = 0; i < numExtraTypes; i++) {
		if (strcmp(type.c_str(), extraTypes[i].typeName) == 0) {
			ValueExtraType[i]=value;
			return true;
		}
	}
	return false;
}




/***************************************************************************
	member functions for RiffChunk
***************************************************************************/

RiffChunk::RiffChunk(RiffFile& parent)
{
   unsigned long _size;
	// read the chunk name
   unsigned long bytes;
	bytes = fread(name, 1, 4, parent.filep());
	name[4] = '\0';

	// read the chunk size
	fread(&_size, 4, 1, parent.filep());
// reverse the endianism of the chunk size.
#ifdef REVERSE_ENDIANISM
 	size = SWAP_32(_size);
#endif

	// if this is a RIFF or LIST chunk, read its subtype.
	if (strcmp(name, "RIFF") == 0
		|| strcmp(name, "LIST") == 0)
	{
		fread(subType, 1, 4, parent.filep());
		subType[4] = '\0';
		// subtract the subtype from the size of the data.
		size -= 4;
	} else
		*subType = '\0';

	// the chunk starts after the name and size.
	/* // eba_
	start = ftell(parent.filep());

	// the next chunk starts after this one, but starts on a word boundary.
	after = start + size;
	if (after % 2)
		after++;
	*/ // eba_
}

/***************************************************************************
	main()
***************************************************************************/

//#ifdef TEST_RIFFFILE		// eba_

#include <iostream>

/*
static void reportProblem()
{
	cout << "  *** ERROR: Result incorrect." << endl;
}
*/

/*
static void checkResult(bool got, bool expected)
{
	if (got)
		cout << "success." << endl;
	else 
		cout << "fail." << endl;

	if (got != expected)
		reportProblem();
}
*/

/*
static void pause()
{
	cout << "Press Enter to continue." << endl;
	cin.get();
}
*/

void showChunk(RiffFile& file, int indent, bool expandLists)
{
	for (int i = 0; i < indent; i++)
		cout << ' ';

	cout << "Chunk type: " << file.chunkName();

	if (file.subType())
		cout << " (" << file.subType() << ")";

	cout << ", " << file.chunkSize() << " bytes" << endl;

	// show all subchunks
	if (strcmp(file.chunkName(), "RIFF") == 0
		|| (expandLists && strcmp(file.chunkName(), "LIST") == 0))
	{
		while (file.push()) {
			showChunk(file, indent + 2, expandLists);
			file.pop();
		}
	}
}

#ifdef MAIN_RIFF

int main(int argc, const char* argv[])
{
	if (argc == 1)
		cout << "Lists the chunks in a named RIFF file." << endl;
	else {
		RiffFile file(argv[1]);

		if (!file.filep()) {
			cout << "No RIFF form in file " << argv[1] << "." << endl;
		} else {
			cout << "Without expanding lists:" << endl;
			showChunk(file, 0, false);

			cout << endl << "Expanding lists:" << endl;
			showChunk(file, 0, true);
		}
	}

	return 0;
}

#endif
//#endif		// eba_


