/* @BA@alize500@XTSce50587@051123@dop@           */
/* Replaced file type *.htm with *.shtml         */
/* @EA@alize500@XTSce50587@051123@dop@           */

// prog1.cpp : Defines the entry point for the console application.
//

// Includes
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include "global.h"
#include "proto.h"
#include "extern.h"
#include <ctype.h>

#include <unistd.h>
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
extern "C" {
#include "cd_typ/t_type.h"
#include "cd_lit/l_config_files.h"
#include "cd_typ/g_service_tools.h"
#include "cd_proc/p_service_tools.h"
}
*/
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

// Global variables
char	POST_Mark[MAX_POST_MARK_LENGTH];
FILE	*fDebug = NULL;
extern char destination[17];


/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
//small tools
char *ltrim(char *str, const char *seps)
{
    size_t totrim;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    totrim = strspn(str, seps);
    if (totrim > 0) {
        size_t len = strlen(str);
        if (totrim == len) {
            str[0] = '\0';
        }
        else {
            memmove(str, str + totrim, len + 1 - totrim);
        }
    }
    return str;
}

char *rtrim(char *str, const char *seps)
{
    int i;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    i = strlen(str) - 1;
    while (i >= 0 && strchr(seps, str[i]) != NULL) {
        str[i] = '\0';
        i--;
    }
    return str;
}

char *trim(char *str, const char *seps)
{
    return ltrim(rtrim(str, seps), seps);
}
//end of small tools

unsigned int get_preannouncement_capacity()
{
//do not limited the message duration(nb_prea_capacity) regarding to the number of messages managed by the license
//if the license authorized 20 msg the nb_prea_capacity should be 320s but not 320/20=16s

   return (nb_prea_capacity=320);
}

unsigned int get_moh_capacity()
{
	FILE *f_moh_cap;
	char moh_cap_buf[130];//NB_MOH_CAPACITY               | 10
   char *search_for = "|";

	print_debug( "In get_moh_capacity");

   nb_moh_capacity = 2;

	system("cat /tmp/xxx | grep MOH_CAPACITY > /tmp/.moh_cap.xxx");
	f_moh_cap = fopen("/tmp/.moh_cap.xxx", "r");

	if( f_moh_cap != NULL )
   {
      fread(moh_cap_buf, 1, 128, f_moh_cap);
      char *token = strstr(moh_cap_buf, (char*)search_for);

      if(token !=NULL)
      {
         strcpy(moh_cap_buf,trim(token+2, NULL));
         nb_moh_capacity = atoi(moh_cap_buf);
         fclose(f_moh_cap);
      }
   }
//	print_debug( "MOH capacity is:%d minutes", nb_moh_capacity );
   return nb_moh_capacity;
}
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
unsigned int get_nb_aa_welcome_capacity()
{
   //Only specified in OMC helps
   return  (nb_aa_welcome_capacity=120);//2 minutes MAX
}

unsigned int get_nb_aa_goodbye_capacity()
{
   //Only specified in OMC helps
   return  (nb_aa_goodbye_capacity=20);//20s MAX
}
unsigned int get_nb_at_welcome_capacity()
{
   //Only specified in OMC helps
   return  (nb_at_welcome_capacity=120);//2 minutes MAX
}

unsigned int get_nb_at_goodbye_capacity()
{
   //Only specified in OMC helps
   return  (nb_at_goodbye_capacity=20);//20s MAX
}

unsigned int get_nb_gal_audio_capacity()
{
   //Only specified in OMC helps
   return  (nb_gal_audio_capacity=120);//2 minutes MAX
}
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */

char get_post_param_from_WedDiag(char *sender)
{
   int	Group_Number_For_Entity = 1;	   // Entity information is in the first group
   int	Group_Number_For_Filename = 2;	// Filename information is in the second group
   char	recv_char,				            // Character received from the distant POST method
		aLine[MAX_LINE_LENGTH],	            // An input line on the standard input (POSTed data)
		entity_num,                         // Entity number
      wav_file_name[MAX_FILE_NAME_LENGTH],// File name of the WAV file
		*beginOffset,			               // Begin offset for a searched string
		*endOffset;				               // End offset for a searched string
	BOOLEAN POST_Mark_Complete,		      // Boolean, TRUE if the POST mark is arrived and valid
		entity_num_found,		               // TRUE is the WAV file name is found
		file_name_found;		               // TRUE is the WAV file name is found
		int position,			      	      // Position in a string
		current_grp_nb;			            // Current group number in the POST message
/* @BD@OXO@CROXOC-4249@191007@degtoun1@ */
/*
 *	FILE *f_moh_cap;
 *	char moh_cap_buf[130];
 *	int  moh_cap_buf_offset;
 *	char moh_buf[130];
 *	int  moh_buf_offset;
 */
/* @ED@OXO@CROXOC-4249@191007@degtoun1@ */

	// Initialize all the variables
	memset( (void*)POST_Mark, '\0', 255 );
   memset( wav_file_name, '\0', sizeof( wav_file_name));

	POST_Mark_Complete = FALSE;
	entity_num_found = FALSE;
	file_name_found = FALSE;
	position = 0;
	current_grp_nb = 0;

/* @BA@OXO@CROXOC-4249@191007@degtoun1@ */
   nb_moh_capacity = get_moh_capacity();
   print_debug( "In get_post_param_from_WedDiag - MOH capacity is %d minutes", nb_moh_capacity );
/* @EA@OXO@CROXOC-4249@191007@degtoun1@ */

	// Read the header (from POST)
   // Get the mark (unique string to separate all the fields)
   // Read the first line until the '\n' character
   do
   {
      recv_char = getchar();

      // Test if EOF arrives => Error
      if( recv_char == (char)EOF )
         print_error( ERR_PREMATURE_EOF, ERR_P_EOF_POST_MARK, sender );

      // Add character to the POST mark string
      POST_Mark[position] = recv_char;
      position++;

      // Test end of the POST mark
      if( recv_char == '\n' )
         POST_Mark_Complete = TRUE;

   } while( (!POST_Mark_Complete) && ((position+1) < MAX_POST_MARK_LENGTH) );

   // Test if the POST mark is arrived
   if( !POST_Mark_Complete )
      print_error( ERR_POST, ERR_POST_MARK_TOO_LONG, sender );

   print_debug( "POST mark received: %s", POST_Mark );

   // We are in the first group
   current_grp_nb = 1;


	// Get the entity number
	if( current_grp_nb > Group_Number_For_Entity )
		print_error( ERR_INTERNAL, ERR_INT_BAD_GRP_FILE_NAME, sender );

	while( current_grp_nb < Group_Number_For_Entity )
	{
		go_to_next_group(sender);
		current_grp_nb++;
	}

   print_debug( "Checking entity number" );
	do
	{
      read_line( aLine, MAX_LINE_LENGTH, sender );

		// If the POST mark is found, there is an error
		if( strcmp( aLine, POST_Mark ) == 0 )
			print_error( ERR_POST, ERR_POST_ENTITY_NUM, sender );

		beginOffset = strstr( aLine, "entity=" );
		if( beginOffset != NULL )
		{
			entity_num_found = TRUE;
			entity_num = *(beginOffset+7);
			print_debug( "Found entity number : %c", entity_num );
		}
	} while( !entity_num_found );

   if (entity_num < '1' || entity_num > '4')
    		print_error( ERR_POST, ERR_POST_ENTITY_NUM, sender );

	// Get the WAV file name
	// Go to the right group number
	if( current_grp_nb > Group_Number_For_Filename )
		print_error( ERR_INTERNAL, ERR_INT_BAD_GRP_FILE_NAME, sender );

	while( current_grp_nb < Group_Number_For_Filename )
	{
		go_to_next_group(sender);
		current_grp_nb++;
	}

	// Look for the WAV filename
	print_debug( "Look for the posted file name (must be a WAV file)" );
	do
	{
      read_line( aLine, MAX_LINE_LENGTH, sender );

      // If the POST mark is found, there is an error
		if( strcmp( aLine, POST_Mark ) == 0 )
			print_error( ERR_POST, ERR_POST_WAV_FILE_NAME, sender );

		// Look for the string : 'filename="'
		beginOffset = strstr( aLine, "filename=\"" );
		if( beginOffset != NULL )
		{
			file_name_found = TRUE;
			endOffset = strstr( beginOffset+10, "\"" );	// 10 = strlen( filename=" )
			if( endOffset == NULL )
				print_error( ERR_POST, ERR_POST_WAV_FILE_NAME,sender );

			// Check the length of the WAV file name
			if( endOffset-(beginOffset+10)+1 > MAX_FILE_NAME_LENGTH )
				print_error( ERR_POST, ERR_POST_WAV_LONG_NAME, sender );

			strncpy( wav_file_name, beginOffset+10, endOffset-(beginOffset+10) );
			wav_file_name[endOffset-(beginOffset+10)] = '\0';
			print_debug( "Found WAV file name : %s", wav_file_name );
		}
	} while( !file_name_found );

	// Check if the file is a WAV file (with .wav extension)
	if( !( strlen( wav_file_name ) > 4	// min : ?.wav
		&& wav_file_name[strlen(wav_file_name)-4] == '.'
		&& (    wav_file_name[strlen(wav_file_name)-3] == 'w'
		     || wav_file_name[strlen(wav_file_name)-3] == 'W'
		   )
		&& (    wav_file_name[strlen(wav_file_name)-2] == 'a'
		     || wav_file_name[strlen(wav_file_name)-2] == 'A'
		   )
		&& (    wav_file_name[strlen(wav_file_name)-1] == 'v'
		     || wav_file_name[strlen(wav_file_name)-1] == 'V'
		   )
		) )
   {
	   print_debug( "WAV file name is not valid" );
		print_error( ERR_POST, ERR_POST_NOT_WAVE, sender );
   }
	print_debug( "WAV file name is valid" );

	// Read two lines to set the pointer to the begin of the data
   read_line( aLine, MAX_LINE_LENGTH, sender );
   read_line( aLine, MAX_LINE_LENGTH, sender );

	// Check WAV format
	// Done in Wave project

   return entity_num;
}

char * get_api_operation(char *sender)
{
   // Variables
   int	Group_Number_For_oper = 1;	// Entity information is in the first group
   char	 myLine[MAX_LINE_LENGTH];
	char	recv_char,				// Character received from the distant POST method
			aLine[MAX_LINE_LENGTH],	// An input line on the standard input (POSTed data)
			wav_file_name[MAX_FILE_NAME_LENGTH],// File name of the WAV file
			*beginOffset,			// Begin offset for a searched string
			*endOffset;				// End offset for a searched string
	BOOLEAN POST_Mark_Complete;		// Boolean, TRUE if the POST mark is arrived and valid
	int		position,				// Position in a string
			current_grp_nb;			// Current group number in the POST message
/* @BD@OXO@CROXOC-4249@191007@degtoun1@ */
/*
 *	char moh_buf[130];
 *	int  moh_buf_offset;
 */
/* @ED@OXO@CROXOC-4249@191007@degtoun1@ */
   static char operation_buffer[7];

	// Initialize all the variables
	memset( (void*)POST_Mark, '\0', 255 );
   memset(myLine, '\0', sizeof(myLine));
   memset(operation_buffer, '\0', sizeof(operation_buffer));
	POST_Mark_Complete = FALSE;
   BOOLEAN operation_found=FALSE;
	position = 0;
	current_grp_nb = 0;

   // Read the header (from POST)
   // Get the mark (unique string to separate all the fields)
   // Read the first line until the '\n' character
   do
   {
      recv_char = getchar();
      // Test if EOF arrives => Error
      if(recv_char == (char)EOF )
         print_api_error( API_ERR_PREMATURE_EOF, API_ERR_P_EOF_POST_MARK, sender );

      // Add character to the POST mark string
      POST_Mark[position] = recv_char;
      position++;

      // Test end of the POST mark
      if( recv_char == '\n' )
         POST_Mark_Complete = TRUE;

   } while( (!POST_Mark_Complete) && ((position+1) < MAX_POST_MARK_LENGTH) );

   // Test if the POST mark is arrived
   if( !POST_Mark_Complete )
      print_api_error( API_ERR_POST, API_ERR_POST_MARK_TOO_LONG,sender );

   print_debug( "POST mark received: %s", POST_Mark );

   // We are in the first group
   current_grp_nb = 1;


  	if( current_grp_nb > Group_Number_For_oper)
      print_api_error( API_ERR_INTERNAL, API_ERR_INT_BAD_GRP_FILE_NAME, sender );//uncomment


	while( current_grp_nb < Group_Number_For_oper)
	{
		go_to_next_group(sender);
		current_grp_nb++;
	}

   // Look for operation todo
   print_debug( "Look for operation" );
   do
   {
      read_line( aLine, MAX_LINE_LENGTH, sender );

      // If the POST mark is found, there is an error
      if( strcmp( aLine, POST_Mark ) == 0 )
         print_api_error( API_ERR_POST, API_ERR_POST, sender );//uncomment

      beginOffset = strstr( aLine, "oper=" );

      if( beginOffset != NULL )
      {
         operation_found = TRUE;
         strcpy(myLine, (beginOffset+5));
         strcpy(operation_buffer, trim(myLine, NULL));
  			print_debug( "Operation found:%s", operation_buffer);
      }
   } while( !operation_found );

   return trim(operation_buffer, NULL);
}

char *get_post_param_from_api(char *sender, char *operation)
{
   int	Group_Number_For_dest = 2;	         // Entity information is in the first group
   int	Group_Number_For_entity = 3;	      // Entity information is in the first group
   int	Group_Number_For_Filename = 4;	   // Filename information is in the second group
   char	 myLine[MAX_LINE_LENGTH];
	char	recv_char,				               // Character received from the distant POST method
			aLine[MAX_LINE_LENGTH],	            // An input line on the standard input (POSTed data)
			wav_file_name[MAX_FILE_NAME_LENGTH],// File name of the WAV file
			*beginOffset,			               // Begin offset for a searched string
			*endOffset;				               // End offset for a searched string
	BOOLEAN 		entity_num_found,		         // TRUE is the WAV file name is found
			file_name_found;		               // TRUE is the WAV file name is found
	int		position,				            // Position in a string
			current_grp_nb;			            // Current group number in the POST message

   static char	entity_str[4];

	// Initialize all the variables
	entity_num_found = FALSE;
	file_name_found = FALSE;

  	memset(entity_str, '\0', sizeof(entity_str));
   memset(myLine, '\0', sizeof(myLine));
   memset( wav_file_name, '\0', sizeof( wav_file_name));

	position = 0;
   current_grp_nb = 2;


   if( current_grp_nb > Group_Number_For_dest )
   {
		print_api_error( API_ERR_INTERNAL, API_ERR_INT_BAD_GRP_FILE_NAME, sender );
	}

	while( current_grp_nb < Group_Number_For_dest )
	{
		go_to_next_group(sender);
		current_grp_nb++;
	}

	// Look for the file destination
	print_debug( "Look for destination" );

	do
	{
       read_line(aLine, MAX_LINE_LENGTH, sender);
       read_line(aLine, MAX_LINE_LENGTH, sender);


  		// If the POST mark is found, there is an error
		if( strcmp( aLine, POST_Mark ) == 0 )
			print_api_error( API_ERR_POST, API_ERR_POST, sender );//uncomment

		beginOffset = strstr( aLine, "dest=" );
		if( beginOffset != NULL )
		{
         file_name_found = TRUE;
			strcpy(myLine, (beginOffset+5));
         strcpy(destination, trim(myLine, NULL));
         print_debug( "Destination found:%s", destination);
		}

	} while( !file_name_found );

   current_grp_nb = 3;
	// Get the entity number
	if( current_grp_nb > Group_Number_For_entity ){
      print_api_error( API_ERR_INTERNAL, API_ERR_INT_BAD_GRP_FILE_NAME, sender );//uncomment

   }
	while( current_grp_nb < Group_Number_For_entity )
	{
		go_to_next_group(sender);
		current_grp_nb++;
	}

	print_debug( "Checking entity number" );
	do
	{
      read_line( aLine, MAX_LINE_LENGTH,sender);
      read_line( aLine, MAX_LINE_LENGTH,sender);

// If the POST mark is found, there is an error
		if( strcmp( aLine, POST_Mark ) == 0 )
			print_api_error( API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender );

		beginOffset = strstr( aLine, "entity=" );

		if( beginOffset != NULL )
		{
			entity_num_found = TRUE;
			strcpy(entity_str,trim((beginOffset+7),NULL));
			print_debug( "Found entity number : %s", entity_str );
/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
         if(strcmp(destination, "aa")==0)
         {
            if(strcmp(entity_str, "normal")==0)
               strcpy(entity_str, "1");
            else if(strcmp(entity_str, "restricted")==0)
               strcpy(entity_str, "2");
            else if(strcmp(entity_str, "goodbye")==0)
               strcpy(entity_str, "3");
         }
         if(strcmp(destination, "audiotexts")==0)
         {
            if(strcmp(entity_str, "normal")==0)
               strcpy(entity_str, "1");
            else if(strcmp(entity_str, "restricted")==0)
               strcpy(entity_str, "2");
            else if(strcmp(entity_str, "goodbye")==0)
               strcpy(entity_str, "3");
         }
         if(strcmp(destination, "mailboxes")==0)
         {
            if(strcmp(entity_str, "general")==0)
               strcpy(entity_str, "1");
            else if(strcmp(entity_str, "notification")==0)
               strcpy(entity_str, "2");
         }
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */
		}
	} while( !entity_num_found );

   read_line( aLine, MAX_LINE_LENGTH, sender );

/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
   if(strcmp(destination, "moh")==0)
   {
        print_debug( "MOH capacity is %d minutes", get_moh_capacity());
   }
   else if(strcmp(destination, "greetings")==0)
   {
      print_debug( "Preannouncement capacity is %d secondes", get_preannouncement_capacity());
   }
   else if(strcmp(destination, "aa")==0)
   {
      int num=atoi(entity_str);
      if(num ==1 || num ==2)
      {
         print_debug( "AA welcome capacity is %d secondes", get_nb_aa_welcome_capacity());
      }
      else if(num ==3)
      {
         print_debug( "AA goodbye capacity is %d secondes", get_nb_aa_goodbye_capacity());
      }
      else
      {
         print_api_error( API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender );
      }
   }
   else if(strcmp(destination, "audiotexts")==0)
   {
      int num=atoi(entity_str);
      if(num ==1 || num ==2)
      {
         print_debug( "AT welcome capacity is %d seconds", get_nb_at_welcome_capacity());
      }
      else if(num ==3)
      {
         print_debug( "AT goodbye capacity is %d seconds", get_nb_at_goodbye_capacity());
      }
      else
      {
         print_api_error( API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender );
      }
   }
   else if(strcmp(destination, "mailboxes")==0)
   {
      print_debug( "GalMbx audio capacity is %d seconds", get_nb_gal_audio_capacity());
   }
   else
   {
      print_debug( "Unknow feature");
      print_api_error(API_ERR_POST, API_ERR_POST_UNKNOWN_FEATURE, sender );
   }
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */

   if(strcmp(operation, "PUT") == 0)
	{
      // Look for the WAV filename
      do
      {
         read_line( aLine, MAX_LINE_LENGTH, sender );

         // If the POST mark is found, there is an error
         if( strcmp( aLine, POST_Mark ) == 0 )
            print_api_error( API_ERR_POST, API_ERR_POST_WAV_FILE_NAME, sender );

         // Look for the string : 'filename="'
         beginOffset = strstr( aLine, "filename=\"" );
         if( beginOffset != NULL )
         {
            file_name_found = TRUE;
            endOffset = strstr( beginOffset+10, "\"" );	// 10 = strlen( filename=" )
            if( endOffset == NULL )
               print_api_error( API_ERR_POST, API_ERR_POST_WAV_FILE_NAME,sender );

            // Check the length of the WAV file name
            if( endOffset-(beginOffset+10)+1 > MAX_FILE_NAME_LENGTH )
               print_api_error( API_ERR_POST, API_ERR_POST_WAV_LONG_NAME,sender);

            strncpy( wav_file_name, beginOffset+10, endOffset-(beginOffset+10) );
            wav_file_name[endOffset-(beginOffset+10)] = '\0';
            print_debug( "Found WAV file name : %s", wav_file_name );
         }
      } while( !file_name_found );

      // Check if the file is a WAV file (with .wav extension)
      if(file_name_found)
      {
         if( !( strlen( wav_file_name ) > 4	// min : ?.wav
            && wav_file_name[strlen(wav_file_name)-4] == '.'
            && (    wav_file_name[strlen(wav_file_name)-3] == 'w'
                 || wav_file_name[strlen(wav_file_name)-3] == 'W'
               )
            && (    wav_file_name[strlen(wav_file_name)-2] == 'a'
                 || wav_file_name[strlen(wav_file_name)-2] == 'A'
               )
            && (    wav_file_name[strlen(wav_file_name)-1] == 'v'
                 || wav_file_name[strlen(wav_file_name)-1] == 'V'
               )
            ) )
         {
            print_debug( "WAV file name is not valid" );
            print_api_error( API_ERR_POST, API_ERR_POST_NOT_WAVE, sender);
         }
         else
            print_debug( "WAV file name is valid" );
      }
      else
         print_api_error( API_ERR_POST, API_ERR_POST_WAV_FILE_NAME, sender );

      // Read two lines to set the pointer to the begin of the data

      read_line( aLine, MAX_LINE_LENGTH, sender );
      read_line( aLine, MAX_LINE_LENGTH, sender );
   }
	return entity_str;
}
/*----------------------------------------------------------------------*/
/* main                                                                 */
/*----------------------------------------------------------------------*/
/*  Return: int                                                         */
/*             The return value                                         */
/*----------------------------------------------------------------------*/
/* The programm take no arguments, only the standard input is used      */
/*----------------------------------------------------------------------*/
char * mainProg1(char *sender, char * operation)
{
   static char str_entity[4];
   char entity;
   memset(str_entity, '\0', sizeof(str_entity));

   if(strcmp(sender, "api")==0)
      return(get_post_param_from_api(sender, operation));
   else
   {
      str_entity[0] = get_post_param_from_WedDiag(sender);
      return str_entity;
   }
}

/*----------------------------------------------------------------------*/
/* print_error                                                          */
/*----------------------------------------------------------------------*/
/*  Return: void                                                        */
/*  Input : int error                                                   */
/*             Error #                                                  */
/*          int subError                                                */
/*             Sub-error #                                              */
/*----------------------------------------------------------------------*/
/* Opens the right HTML page to display the error message               */
/*----------------------------------------------------------------------*/
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
 *void print_error( int error, int subError )*.)
 */
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
void print_error( int error, int subError, char *sender)
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
{
	char buff[5000];
	int		err, subErr, nbCar;
   unsigned int  minI;

/* @BD@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */
/*	unsigned int 	i, minI, global_wav_size_sendOrig; */
/* @ED@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */

	err = error;
	subErr = subError;
	// Print the error description

   switch( error )
	{
	case SUCCESS:
		{	// Success
			subErr = SUCCESS;
			break;
		}
	case ERR_PREMATURE_EOF:
		{	// Encounter EOF prematurely
			switch( subError )
			{
			case ERR_P_EOF_POST_MARK:
				{	// Encountered EOF while looking for the POST mark
					break;
				}
			case ERR_P_EOF_CHANGE_GRP:
				{	// Encountered EOF while changing group (data missing)
					break;
				}
			case ERR_P_EOF_DATA_MISSING:
				{	// Encountered EOF prematurely (data missing)
					break;
				}
			case ERR_P_EOF_SEARCH_KEY:
				{	// Encountered EOF while searching a key (string) in the data
					break;
				}
			default: subErr = 0;
			}
			break;
		}
	case ERR_INTERNAL:
		{	// Internal error
			switch( subError )
			{
			case ERR_INT_BAD_GRP_FILE_NAME:
				{	// Error, group of filename is already passed (internal error)
					break;
				}
			case ERR_INT_MAIN_ERROR:
				{	// Error in WAVE main
					break;
				}
			default: subErr = 0;
			}
			break;
		}
	case ERR_POST:
		{	// Error on data posted by the distant
			switch( subError )
			{
			case ERR_POST_WAV_FILE_NAME:
				{	// Error, the posted WAV file name cannot be retrieved

					break;
				}
			case ERR_POST_NOT_WAVE:
				{	// The file send is not a WAV file
					break;
				}
			case ERR_POST_MARK_TOO_LONG:
				{	// POST mark is too long
					break;
				}
			case ERR_POST_WAV_LONG_NAME:
				{	// WAV file name is too long
					break;
				}
			case ERR_POST_WAV_SIZE:
				{	// WAV size is too long
					break;
				}
			case ERR_POST_FORMAT:
				{	// Bad WAV format
					break;
				}
			case ERR_POST_ENTITY_NUM:
				{
					break;
				}
				default: subErr = 0;
			}
			break;
		}
	case ERR_IO:
		{	// Internal error
			switch( subError )
			{
			case ERR_FILE_LOCKED:
				{	// Error, file is locked
					break;
				}
			case ERR_FILE_ON_LOCK:
				{	// Error, cannot lock file
					break;
				}
         case ERR_FILE_MISSING_FOR_RESOURCE_ID:
				{	// Error, cannot lock file
					break;
				}

				default: subErr = 0;
			}
			break;
		}
	default: err = 0; subErr = 0; break;
	}
	//print_debug( "Read all the %d characters from the stdin", global_wav_size_send );
	// Empty the stdin

/* @BD@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */
/*
 * global_wav_size_sendOrig = global_wav_size_send;	// Save the file size
   print_debug( "Read all the %d characters from the stdin", global_wav_size_send );

	for (i=0; i<global_wav_size_sendOrig;)
	{
	  if( global_wav_size_send < 5000 )
	    minI = global_wav_size_send;
	  else
	    minI = 5000;
	  i+=minI;
	  nbCar=fread( buff, 1, minI, stdin );
	  global_wav_size_send-=minI;
	}
	// Abort the program
	global_wav_size_send = global_wav_size_sendOrig;

   fclose (stdin);
*/
/* @ED@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */

/* @BA@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */
   if(stdin != NULL)
   {
      // Empty the stdin
      do {
         minI=5000;
         nbCar = fread( buff, 1, minI, stdin );
      } while(nbCar>0);

    fclose (stdin);
   }

	// Abort the program
	print_debug( "End of program" );

/* @EA@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */

   open_html( err, subErr );
   exit(-1);
}

/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
void print_api_error( int error, int subError, char *sender)
{
	char buff[5000];
	int		err, subErr, nbCar;
   unsigned int minI;
/* @BD@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */
/*	unsigned int 	i, minI, global_wav_size_sendOrig; */
/* @ED@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */

	err = error;
	subErr = subError;
	// Print the error description
	switch( error )
	{
   case API_SUCCESS:
		{	// Success
			subErr = API_SUCCESS;
			break;
		}
   case API_ERR_PREMATURE_EOF:
		{	// Encounter EOF prematurely
			switch( subError )
			{
         case API_ERR_P_EOF_POST_MARK:
				{	// Encountered EOF while looking for the POST mark
					break;
				}
         case API_ERR_P_EOF_CHANGE_GRP:
				{	// Encountered EOF while changing group (data missing)
					break;
				}
         case API_ERR_P_EOF_DATA_MISSING:
				{	// Encountered EOF prematurely (data missing)
					break;
				}
         case API_ERR_P_EOF_SEARCH_KEY:
				{	// Encountered EOF while searching a key (string) in the data
					break;
				}
			default: subErr = 0;
			}
			break;
		}
   case API_ERR_INTERNAL:
		{	// Internal error
			switch( subError )
			{
         case API_ERR_INT_BAD_GRP_FILE_NAME:
				{	// Error, group of filename is already passed (internal error)
					break;
				}
         case API_ERR_INT_MAIN_ERROR:
				{	// Error in WAVE main
					break;
				}
			default: subErr = 0;
			}
			break;
		}
   case API_ERR_POST:
		{	// Error on data posted by the distant
			switch( subError )
			{
         case API_ERR_POST_WAV_FILE_NAME:
				{	// Error, the posted WAV file name cannot be retrieved
					break;
				}
         case API_ERR_POST_NOT_WAVE:
				{	// The file send is not a WAV file
					break;
				}
         case API_ERR_POST_MARK_TOO_LONG:
				{	// POST mark is too long
					break;
				}
         case API_ERR_POST_WAV_LONG_NAME:
				{	// WAV file name is too long
					break;
				}
         case API_ERR_POST_WAV_SIZE:
				{	// WAV size is too long
					break;
				}
         case API_ERR_POST_FORMAT:
				{	// Bad WAV format
					break;
				}
         case API_ERR_POST_ENTITY_NUM:
				{
					break;
				}
			case API_ERR_POST_UNKNOWN_FEATURE:
				{
					break;
				}
				case API_ERR_POST_UNKNOWN_OPERATION:
				{
					break;
				}
				default: subErr = 0;
			}
			break;
		}
   case API_ERR_IO:
		{	// Internal error
			switch( subError )
			{
         case API_ERR_FILE_LOCKED:
				{	// Error, file is locked
					break;
				}
         case API_ERR_FILE_ON_LOCK:
				{	// Error, cannot lock file
					break;
				}
			case API_ERR_MISSING_FILE:
				{	// Error, missing file
            break;
				}
				case API_ERR_MEMALLOC:
				{	// Error, memory allocation failed
					break;
				}
				case API_ERR_REMOVING_FILE:
				{	// Error, cannot remove file
					break;
				}
				case API_ERR_STDOUT:
				{	// Error, cannot open/write stdout
					break;
				}
				case API_ERR_READING_FILE:
				{	// Error, cannot read file
					break;
				}
					default: subErr = 0;
			}
			break;
		}
	default: err = 0; subErr = 0; break;
	}
	//print_debug( "Read all the %d characters from the stdin", global_wav_size_send );
	// Empty the stdin
/* @BD@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */
/*
	global_wav_size_sendOrig = global_wav_size_send;	// Save the file size
	for (i=0; i<global_wav_size_sendOrig;)
	{
	  if( global_wav_size_send < 5000 )
	    minI = global_wav_size_send;
	  else
	    minI = 5000;
	  i+=minI;
	  nbCar=fread( buff, 1, minI, stdin );
	  global_wav_size_send-=minI;
//	  print_debug( "Read a line (%d) (%d characters)", i, nbCar );
	}
	fclose (stdin);
	// Abort the program
	print_debug( "End of program" );
	global_wav_size_send = global_wav_size_sendOrig;
*/
/* @ED@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */

/* @BA@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */
   if(stdin != NULL)
   {
      // Empty the stdin
      do {
         minI=5000;
         nbCar = fread( buff, 1, minI, stdin );
      } while(nbCar>0);

    fclose (stdin);
   }
	// Abort the program
	print_debug( "End of program" );

/* @EA@OXOC_4.0@CROXOC-7225@200207@degtoun1@ */

   send_error_to_api(err, subErr);
   exit( -1 );
}
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

/*----------------------------------------------------------------------*/
/* print_debug                                                          */
/*----------------------------------------------------------------------*/
/*  Return: void                                                        */
/*  Input : const char *fmt                                             */
/*             Display the formatted string                             */
/*			...                                                         */
/*				String arguments                                        */
/*----------------------------------------------------------------------*/
/* For debug (write debug string as comment in the output HTML page     */
/*----------------------------------------------------------------------*/
void print_debug( const char *fmt, ... )
{
	va_list ap;

	if( DEBUG )
	{
		if( fDebug == NULL )
		{
			fDebug = fopen( "/current/vmu/debugmoh.htm", "wt" );
			if( fDebug == NULL )
				return;
		}
		va_start( ap, fmt );
		fwrite( "<!-- ", 1, 5, fDebug );	// Comment TAG
		vfprintf( fDebug, fmt, ap );
		fwrite( " -->\n", 1, 5, fDebug );	// Comment TAG
		va_end( ap );
		fflush( fDebug );
	}

	/*
	if( DEBUG )
		printf( "<!-- %s -->", string );
	*/
}

/*----------------------------------------------------------------------*/
/* go_to_next_group                                                     */
/*----------------------------------------------------------------------*/
/*  Return: void                                                        */
/*----------------------------------------------------------------------*/
/* Go the next group in the data (after the next POST mark)             */
/*----------------------------------------------------------------------*/
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/* void go_to_next_group( void )
*  @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
void go_to_next_group(char *sender)
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
{
	// Variables
	BOOLEAN	POST_mark_found;	// Boolean true if the POST mark is found, group passed
	char	read_char;			// Character read
	unsigned int position;		// Position in the line CHAR[position]

	// Initialize the variables
	POST_mark_found = FALSE;
	position = 0;

	do
	{
		read_char = getchar();

		// Test if EOF arrives => Error
		if( read_char == (char)EOF )
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
 *			print_error( ERR_PREMATURE_EOF, ERR_P_EOF_CHANGE_GRP );
 */
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
      {
         if(strcmp(sender, "api")==0)
			   print_api_error( API_ERR_PREMATURE_EOF, API_ERR_P_EOF_CHANGE_GRP, sender );
         else
			   print_error( ERR_PREMATURE_EOF, ERR_P_EOF_CHANGE_GRP, sender );
      }
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

		if( (strlen(POST_Mark) > position)
			&& (read_char == POST_Mark[position]) )
		{	// it's good
			position++;
		}
		else
		{	// POST mark is not in this line, pass the line
			while( read_char != '\n' )
			{
				read_char = getchar();

				if( read_char == (char)EOF )
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
 *					print_error( ERR_PREMATURE_EOF, ERR_P_EOF_CHANGE_GRP );
 */
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
            {
					if(strcmp(sender, "api")==0)
                  print_api_error( API_ERR_PREMATURE_EOF, API_ERR_P_EOF_CHANGE_GRP, sender );
               else
                  print_error( ERR_PREMATURE_EOF, ERR_P_EOF_CHANGE_GRP, sender );
            }
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
			}

			position = 0;
			continue;
		}

		if( read_char == '\n' )	// POST mark found
			POST_mark_found = TRUE;

	} while( !POST_mark_found );
}

/*----------------------------------------------------------------------*/
/* read_line                                                            */
/*----------------------------------------------------------------------*/
/*  Return: int                                                         */
/*				Number of characters read                               */
/*  Input : int maxChar                                                 */
/*				Maximum length for the output string                    */
/*  Output: char *line                                                  */
/*				String containing the retrieved line                    */
/*----------------------------------------------------------------------*/
/* Read a line from the POSTed data (until the '\n')                    */
/*----------------------------------------------------------------------*/
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
 *int read_line( char *line, int maxChar)
 */
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
int read_line( char *line, int maxChar, char *sender )
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
{
	int		position = 0,	// Position in line
			local_counter;	// Number of characters read
	char	read_char;		// Character read

	if( maxChar < 1 )
		return 0;

	memset( line, '\0', maxChar );
	local_counter = 0;

	do
	{
		read_char = getchar();

		if( read_char == (char)EOF )
		{
			//print_debug( "Read: %s\n", line );
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
 *			print_error( ERR_PREMATURE_EOF, ERR_P_EOF_DATA_MISSING );
 */
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

			if(strcmp(sender, "api")==0)
            print_api_error( API_ERR_PREMATURE_EOF, API_ERR_P_EOF_DATA_MISSING, sender );
         else
            print_error( ERR_PREMATURE_EOF, ERR_P_EOF_DATA_MISSING, sender );

/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
		}
		else
			local_counter++;

		line[position] = read_char;
		position++;

	} while( (position < maxChar-1) && (read_char != '\n') );

	while( read_char != '\n' )
	{
		read_char = getchar();

		if( read_char == (char)EOF )
		{
			//print_debug( "Read: %s\n", line );
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
 *			print_error( ERR_PREMATURE_EOF, ERR_P_EOF_DATA_MISSING );
 */
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
			if(strcmp(sender, "api")==0)
            print_api_error( API_ERR_PREMATURE_EOF, API_ERR_P_EOF_DATA_MISSING, sender);
         else
            print_error( ERR_PREMATURE_EOF, ERR_P_EOF_DATA_MISSING, sender);

/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
		}
		else
			local_counter++;
	}

	return local_counter;
}

/*----------------------------------------------------------------------*/
/* send_error_to_api(int error, int subError)                           */
/*----------------------------------------------------------------------*/
/*  Return: void                                                        */
/*  Input : int error                                                   */
/*				Error #                                                     */
/*  Output: int subError                                                */
/*				Sub-error #                                                 */
/*----------------------------------------------------------------------*/
/* Send Json string to API {"error":err, "msg":"error description"}     */
/*----------------------------------------------------------------------*/
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
void send_error_to_api( int error, int subError )
{
   char buf[128];
	memset(buf, '\0', sizeof(buf));

	switch( error )
	{
	case API_SUCCESS:
		{	// Success
         sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "success");
			break;
		}
	case API_ERR_PREMATURE_EOF:
		{	// Encounter EOF prematurely
			switch( subError )
			{
			case API_ERR_P_EOF_POST_MARK:
				{	// Encountered EOF while looking for the POST mark
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Encountered EOF while looking for the POST mark");
					break;
				}
			case API_ERR_P_EOF_CHANGE_GRP:
				{	// Encountered EOF while changing group (data missing)
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Encountered EOF while changing group (data missing)");
					break;
				}
			case API_ERR_P_EOF_DATA_MISSING:
				{	// Encountered EOF prematurely (data missing)
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Encountered EOF prematurely (data missing)");
					break;
				}
			case API_ERR_P_EOF_SEARCH_KEY:
				{	// Encountered EOF while searching a key (string) in the data
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Encountered EOF while searching a key (string) in the data");
					break;
				}
			default: sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Encountered EOF prematurely-default");
			}
			break;
		}
	case API_ERR_INTERNAL:

		{	// Internal error
			switch( subError )
			{
			case API_ERR_INT_BAD_GRP_FILE_NAME:
				{	// Error, group of filename is already passed (internal error)
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error, group of filename is already passed (internal error)");
					break;
				}
			case API_ERR_INT_MAIN_ERROR:
				{	// Error in WAVE main
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error in WAVE main");
					break;
				}
			default: sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error in WAVE main");
			}
			break;
		}
	case API_ERR_POST:
		{	// Error on data posted by the distant
			switch( subError )
			{
			case API_ERR_POST_WAV_FILE_NAME:
				{	// Error, the posted WAV file name cannot be retrieved
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error, the posted WAV file name cannot be retrieved");
					break;
				}
			case API_ERR_POST_NOT_WAVE:
				{	// The file send is not a WAV file
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "The file send is not a WAV file");
					break;
				}
			case API_ERR_POST_MARK_TOO_LONG:
				{	// POST mark is too long
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "POST mark is too long");
					break;
				}
			case API_ERR_POST_WAV_LONG_NAME:
				{	// WAV file name is too long
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "WAV file name is too long");
					break;
				}
			case API_ERR_POST_WAV_SIZE:
				{	// WAV size is too long
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "WAV size is too long");
					break;
				}
			case API_ERR_POST_FORMAT:
				{	// Bad WAV format
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Bad WAV format");
					break;
				}
			case API_ERR_POST_ENTITY_NUM:
				{	// Bad entity number
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Bad id number");
					break;
				}
         case API_ERR_POST_UNKNOWN_FEATURE:
         {	//unknown feature
            sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Unknown feature");
         }
         break;
         case API_ERR_POST_UNKNOWN_OPERATION:
         {	//Unknown API operation
            sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Unknown API operation");
         }
         break;
			default: sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error on data posted by the distant");
			}
			break;
		}
	case API_ERR_IO:
		{	// I/O error
			switch( subError )
			{
			case API_ERR_FILE_LOCKED:
				{
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error, file is locked");
					break;
				}
			case API_ERR_FILE_ON_LOCK:
				{
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error, cannot lock file");
					break;
				}
         case API_ERR_MISSING_FILE:
				{
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error, file missing");
					break;
				}
          case API_ERR_MEMALLOC:
				{
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error, memory allocation failed");
					break;
				}
          case API_ERR_REMOVING_FILE:
				{
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error, cannot remove file");
					break;
				}
         case API_ERR_STDOUT:
				{
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error, cannot open or write to stdout");
					break;
				}
         case API_ERR_READING_FILE:
				{
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "Error, cannot read file contents");
					break;
				}
			default:
               sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "I/O default");
			}
			break;
		}
	default: sprintf(buf, "{\"error\":%d, \"msg\":\"%s\"}", subError, "default");
	}
   printf(buf);
}
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

/*----------------------------------------------------------------------*/
/* open_html                                                            */
/*----------------------------------------------------------------------*/
/*  Return: void                                                        */
/*  Input : int error                                                   */
/*				Error #                                                 */
/*  Output: int subError                                                */
/*				Sub-error #                                             */
/*----------------------------------------------------------------------*/
/* Display the HTML page (error description)                            */
/*----------------------------------------------------------------------*/
void open_html( int error, int subError )
{
	// Variables
/* @BA@alize500@XTSce50587@051123@dop@           */
	char	html_file_name[MAX_HTML_FILE_NAME_LENGTH];
/* @EA@alize500@XTSce50587@051123@dop@           */
/* @BD@alize500@XTSce50587@051123@dop@           */
#if (0)
	char	html_file_name[MAX_HTML_FILE_NAME_LENGTH],
							// HTML file name (error description)
			buf[MAX_HTML_LINE];
							// HTML line content (all the file content)
	FILE	*fHTML;			// File descriptor for the errors
	int		nbRead;			// Number of characters read by the fread function
#endif
/* @ED@alize500@XTSce50587@051123@dop@           */

	// Initialization
	memset( html_file_name, '\0', MAX_HTML_FILE_NAME_LENGTH );

	switch( error )
	{
	case SUCCESS:
		{	// Success
			strcpy( html_file_name, "../htmlerr/success.shtml" );
			print_debug( "Error : SUCCESS - SUCCESS" );
			break;
		}
	case ERR_PREMATURE_EOF:
		{	// Encounter EOF prematurely
			switch( subError )
			{
			case ERR_P_EOF_POST_MARK:
				{	// Encountered EOF while looking for the POST mark
					strcpy( html_file_name, "../htmlerr/peof_pst.shtml" );
					print_debug( "Error : ERR_PREMATURE_EOF - ERR_P_EOF_POST_MARK" );
					break;
				}
			case ERR_P_EOF_CHANGE_GRP:
				{	// Encountered EOF while changing group (data missing)
					strcpy( html_file_name, "../htmlerr/peof_grp.shtml" );
					print_debug( "Error : ERR_PREMATURE_EOF - ERR_P_EOF_CHANGE_GRP" );
					break;
				}
			case ERR_P_EOF_DATA_MISSING:
				{	// Encountered EOF prematurely (data missing)
					strcpy( html_file_name, "../htmlerr/peof_mis.shtml" );
					print_debug( "Error : ERR_PREMATURE_EOF - ERR_P_EOF_DATA_MISSING" );
					break;
				}
			case ERR_P_EOF_SEARCH_KEY:
				{	// Encountered EOF while searching a key (string) in the data
					strcpy( html_file_name, "../htmlerr/peof_key.shtml" );
					print_debug( "Error : ERR_PREMATURE_EOF - ERR_P_EOF_SEARCH_KEY" );
					break;
				}
			default: strcpy( html_file_name, "../htmlerr/peof_def.shtml" );
					 print_debug( "Error : ERR_PREMATURE_EOF - default" );
			}
			break;
		}
	case ERR_INTERNAL:
		{	// Internal error
			switch( subError )
			{
			case ERR_INT_BAD_GRP_FILE_NAME:
				{	// Error, group of filename is already passed (internal error)
					strcpy( html_file_name, "../htmlerr/int_grp.shtml" );
					print_debug( "Error : ERR_INTERNAL - ERR_INT_BAD_GRP_FILE_NAME" );
					break;
				}
			case ERR_INT_MAIN_ERROR:
				{	// Error in WAVE main
					strcpy( html_file_name, "../htmlerr/int_main.shtml" );
					print_debug( "Error : ERR_INTERNAL - ERR_INT_MAIN_ERROR" );
					break;
				}
			default: strcpy( html_file_name, "../htmlerr/int_def.shtml" );
					 print_debug( "Error : ERR_INTERNAL - default" );
			}
			break;
		}
	case ERR_POST:
		{	// Error on data posted by the distant
			switch( subError )
			{
			case ERR_POST_WAV_FILE_NAME:
				{	// Error, the posted WAV file name cannot be retrieved
					strcpy( html_file_name, "../htmlerr/pst_wavf.shtml" );
					print_debug( "Error : ERR_POST - ERR_POST_WAV_FILE_NAME" );
					break;
				}
			case ERR_POST_NOT_WAVE:
				{	// The file send is not a WAV file
					strcpy( html_file_name, "../htmlerr/pst_nwav.shtml" );
					print_debug( "Error : ERR_POST - ERR_POST_NOT_WAVE" );
					break;
				}
			case ERR_POST_MARK_TOO_LONG:
				{	// POST mark is too long
					strcpy( html_file_name, "../htmlerr/pst_mark.shtml" );
					print_debug( "Error : ERR_POST - ERR_POST_MARK_TOO_LONG" );
					break;
				}
			case ERR_POST_WAV_LONG_NAME:
				{	// WAV file name is too long
					strcpy( html_file_name, "../htmlerr/pst_lwav.shtml" );
					print_debug( "Error : ERR_POST - ERR_POST_WAV_LONG_NAME" );
					break;
				}
			case ERR_POST_WAV_SIZE:
				{	// WAV size is too long
					strcpy( html_file_name, "../htmlerr/pst_swav.shtml" );
					print_debug( "Error : ERR_POST - ERR_POST_WAV_SIZE" );
					break;
				}
			case ERR_POST_FORMAT:
				{	// Bad WAV format
					strcpy( html_file_name, "../htmlerr/pst_fwav.shtml" );
					print_debug( "Error : ERR_POST - ERR_POST_FORMAT" );
					break;
				}
			case ERR_POST_ENTITY_NUM:
				{	// Bad entity number
					strcpy( html_file_name, "../htmlerr/pst_ent.shtml" );
					print_debug( "Error : ERR_POST - ERR_POST_ENTITY_NUM" );
					break;
				}
			default: strcpy( html_file_name, "../htmlerr/pst_def.shtml" );
					 print_debug( "Error : ERR_POST - default" );
			}
			break;
		}
	case ERR_IO:
		{	// I/O error
			switch( subError )
			{
			case ERR_FILE_LOCKED:
				{
					strcpy( html_file_name, "../htmlerr/io_lockd.shtml" );
					print_debug( "Error : ERR_IO - ERR_FILE_LOCKED" );
					break;
				}
			case ERR_FILE_ON_LOCK:
				{
					strcpy( html_file_name, "../htmlerr/io_lock.shtml" );
					print_debug( "Error : ERR_IO - ERR_FILE_ON_LOCK" );
					break;
				}
         case ERR_FILE_MISSING_FOR_RESOURCE_ID:
         {
            strcpy( html_file_name, "../htmlerr/io_fm4re.shtml" );
            print_debug( "Error : ERR_IO - ERR_FILE_MISSING_FOR_RESOURCE_ID" );
            break;
         }

			default: strcpy( html_file_name, "../htmlerr/io_def.shtml" );
					 print_debug( "Error : ERR_IO - default" );
			}
			break;
		}
	default: strcpy( html_file_name, "../htmlerr/default.shtml" );
			 print_debug( "Error : default" );
	}

	print_debug( "Error HTML file name : %s", html_file_name);

/* @BA@alize500@XTSce50587@051123@dop@           */
        /* HTTP header */
	printf("Content-Type: text/html\r\n\r\n");
        /* HTML header for html redirection */
	printf("<html><head>\r\n\
	        <title>Submit response</title>\r\n\
	        <meta http-equiv=\"REFRESH\" content=\"1; URL=%s\">\r\n\
		     </head></html>\r\n", 
         html_file_name);
}

#if (0)
{ compensate for next commentarized closing bracket that can trouble some editors
#endif
/* @EA@alize500@XTSce50587@051123@dop@           */
/* @BD@alize500@XTSce50587@051123@dop@           */ 
#if (0)
	// Open the file
	fHTML = fopen( html_file_name, "rt" );
	if( fHTML == NULL )
	{
		print_debug( "Error on open the HTML file name : %s", html_file_name );
		print_default_HTML();
		goto DEBUG_PRINT;
		return;
	}
	print_debug( "File successfully open" );

	// Read and print the HTML file content
	do
	{
		nbRead = 0;
		memset( buf, '\0', MAX_HTML_LINE );

		nbRead = fread( buf, sizeof(char), MAX_HTML_LINE, fHTML );

		// Enable some parameters in this file (ie: file name, format, ...)
		add_info( buf, MAX_HTML_LINE );
		printf( "%s", buf );

	} while( nbRead != 0 );

	// Close file
	fclose( fHTML );
	print_debug( "File closed" );

DEBUG_PRINT:
	printf("\n\n<!-- DEBUG : -->\n");

	// Close the debug file (WRITE mode)
	fclose( fDebug );

	// Open the debug file
	//fHTML = fopen( "../htmlerr/debug.htm", "rt" );
	fHTML = fopen( "/current/vmu/debugmoh.htm", "rt" );
	if( fHTML == NULL )
	{
		printf("<!-- DEBUG FILE NULL -->\n");
		return;
	}

	// Read and print the debug file content
	do
	{
		nbRead = 0;
		memset( buf, '\0', MAX_HTML_LINE );

		nbRead = fread( buf, sizeof(char), MAX_HTML_LINE, fHTML );

		printf( "%s", buf );

	} while( nbRead != 0 );

	// Close file
	fclose( fHTML );

//	fHTML = fopen( "/current/vmu/debugmoh.htm", "wt" );
//	if( fHTML != NULL )
//		fclose( fHTML );


	printf("<!-- END DEBUG -->\n");
}

/*----------------------------------------------------------------------*/
/* print_default_HTML                                                   */
/*----------------------------------------------------------------------*/
/*  Return: void                                                        */
/*----------------------------------------------------------------------*/
/* Print the default error, HTML page not found                         */
/*----------------------------------------------------------------------*/
void print_default_HTML( void )
{
	printf("Content-type: text/plain\n");
	printf("Expires:Tue, 19 Dec 2000 23:42:06 UTC\n");
	printf("\nMOH Encoder\n\n");
	printf( "Error, HTML description file not found\n\n" );
}


/*----------------------------------------------------------------------*/
/* search_key                                                           */
/*----------------------------------------------------------------------*/
/*  Return: void                                                        */
/*  Input : char *key                                                   */
/*				Key to search in tha data                               */
/*----------------------------------------------------------------------*/
/* Search the key, the pointer is just after this key on return         */
/*----------------------------------------------------------------------*/
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
 *void search_key( char *key )
 */
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
void search_key( char *key, char * sender )
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
{
	// Variables
	BOOLEAN	key_found;		// Boolean true if the key is found
	char	read_char;		// Character read
	unsigned int position;	// Position in the key string

	// Initialize the variables
	key_found = FALSE;
	position = 0;

	do
	{
		read_char = getchar();

		// Test if EOF arrives => Error
		if( read_char == EOF )
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
 *			print_error( ERR_PREMATURE_EOF, ERR_P_EOF_SEARCH_KEY );
 */
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
      {
			if(strcmp(sender, "api") == 0)
            print_api_error( API_ERR_PREMATURE_EOF, API_ERR_P_EOF_SEARCH_KEY, sender );
         else
            print_error( ERR_PREMATURE_EOF, ERR_P_EOF_SEARCH_KEY, sender);
      }
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

		if( (strlen(key) > position)
			&& (read_char == key[position]) )
		{	// it's good
			position++;
		}
		else
		{
			position = 0;
			continue;
		}

		if( position == strlen( key ) )	// Key found
			key_found = TRUE;

	} while( !key_found );
}

/*----------------------------------------------------------------------*/
/* add_info                                                             */
/*----------------------------------------------------------------------*/
/*  Return: void                                                        */
/*  Input : char *buf                                                   */
/*				Buffer containing eventually some TAG to replace by some*/
/*				informations                                            */
/*			int max_size                                                */
/*				Max size for the buf string                             */
/*----------------------------------------------------------------------*/
/* Search the key, the pointer is just after this key on return         */
/*----------------------------------------------------------------------*/
void add_info( char *buf, int max_size )
{
	char	*buf2,		// modified buffer
			*offset,
			bufInt[20];	// Buffer for the numbers
	int		bufLen,
			tagLen,
			oneMore;

	oneMore = 1;
	buf2 = (char*)(malloc( sizeof(char) * max_size ));

	while( oneMore == 1 )
	{
		// TAG : TAG-MOH--ERR_INT_MAIN_ERROR--MOH-TAG ===> global_main_error
		if(    (offset = strstr( buf, "TAG-MOH--ERR_INT_MAIN_ERROR--MOH-TAG") )
			&& (offset != NULL) )
		{	// Found TAG : TAG-MOH--ERR_INT_MAIN_ERROR--MOH-TAG
			memset( buf2, '\0', max_size );
			strncpy( buf2, buf, offset-buf );	// Copy the begin of the string
			bufLen = strlen( buf2 );
			strncat( buf2, global_main_error, max_size-bufLen );
			bufLen = strlen( buf2 );
			tagLen = strlen( "TAG-MOH--ERR_INT_MAIN_ERROR--MOH-TAG" );
			strncat( buf2, offset+tagLen, max_size-bufLen );
			strcpy( buf, buf2 );
		}
		// TODO
		// TAG : TAG-MOH--ERR_POST_FORMAT_CHANNELS--MOH-TAG ===> global_channels
		else if(    (offset = strstr( buf, "TAG-MOH--ERR_POST_FORMAT_CHANNELS--MOH-TAG") )
				 && (offset != NULL) )
		{
			memset( buf2, '\0', max_size );
			strncpy( buf2, buf, offset-buf );	// Copy the begin of the string
			bufLen = strlen( buf2 );
			// Create the string containing the number
			if( global_channels == 1 )
				strcpy( bufInt, "Mono" );
			else
				strcpy( bufInt, "Stereo" );

			strncat( buf2, bufInt, max_size-bufLen );
			bufLen = strlen( buf2 );
			tagLen = strlen( "TAG-MOH--ERR_POST_FORMAT_CHANNELS--MOH-TAG" );
			strncat( buf2, offset+tagLen, max_size-bufLen );
			strcpy( buf, buf2 );
		}
		// TAG : TAG-MOH--ERR_POST_FORMAT_SAMPLESPERSEC--MOH-TAG ===> global_SamplesPerSec
		else if(    (offset = strstr( buf, "TAG-MOH--ERR_POST_FORMAT_SAMPLESPERSEC--MOH-TAG") )
				 && (offset != NULL) )
		{
			memset( buf2, '\0', max_size );
			strncpy( buf2, buf, offset-buf );	// Copy the begin of the string
			bufLen = strlen( buf2 );
			// Create the string containing the number
			sprintf( bufInt, "%ld", global_SamplesPerSec );

			strncat( buf2, bufInt, max_size-bufLen );
			bufLen = strlen( buf2 );
			tagLen = strlen( "TAG-MOH--ERR_POST_FORMAT_SAMPLESPERSEC--MOH-TAG" );
			strncat( buf2, offset+tagLen, max_size-bufLen );
			strcpy( buf, buf2 );
		}
		// TAG : TAG-MOH--ERR_POST_FORMAT_BITSPERSAMPLE--MOH-TAG ===> global_BitsPerSample
			// 1 = Mono, 2 = Stereo
		else if(    (offset = strstr( buf, "TAG-MOH--ERR_POST_FORMAT_BITSPERSAMPLE--MOH-TAG") )
				 && (offset != NULL) )
		{
			memset( buf2, '\0', max_size );
			strncpy( buf2, buf, offset-buf );	// Copy the begin of the string
			bufLen = strlen( buf2 );
			// Create the string containing the number
			sprintf( bufInt, "%hd", global_BitsPerSample );

			strncat( buf2, bufInt, max_size-bufLen );
			bufLen = strlen( buf2 );
			tagLen = strlen( "TAG-MOH--ERR_POST_FORMAT_BITSPERSAMPLE--MOH-TAG" );
			strncat( buf2, offset+tagLen, max_size-bufLen );
			strcpy( buf, buf2 );
		}
		// TAG : TAG-MOH--ERR_POST_WAV_SIZE_LIMIT--MOH-TAG ===> MAX_PCM_WAV_SIZE
		else if(    (offset = strstr( buf, "TAG-MOH--ERR_POST_WAV_SIZE_LIMIT--MOH-TAG") )
				 && (offset != NULL) )
		{
			memset( buf2, '\0', max_size );
			//print_debug( "Found WAV_SIZE_LIMIT tag" );
			strncpy( buf2, buf, offset-buf );	// Copy the begin of the string
			bufLen = strlen( buf2 );
			// Create the string containing the number
			sprintf( bufInt, "%d", (PCM_SIZE_PER_MINUTE*nb_moh_capacity-46) );

			strncat( buf2, bufInt, max_size-bufLen );
			bufLen = strlen( buf2 );
			tagLen = strlen( "TAG-MOH--ERR_POST_WAV_SIZE_LIMIT--MOH-TAG" );
			strncat( buf2, offset+tagLen, max_size-bufLen );
			strcpy( buf, buf2 );
			//print_debug( "New buffer is:%s", buf );
		}
		// TAG : TAG-MOH--ERR_POST_WAV_SIZE_SEND--MOH-TAG ===> global_wav_size_send
		else if(    (offset = strstr( buf, "TAG-MOH--ERR_POST_WAV_SIZE_SEND--MOH-TAG") )
				 && (offset != NULL) )
		{
			memset( buf2, '\0', max_size );
			//print_debug( "Found WAV_SIZE_SEND tag" );
			strncpy( buf2, buf, (int)(offset-buf) );	// Copy the begin of the string
			bufLen = strlen( buf2 );
			// Create the string containing the number
			sprintf( bufInt, "%ld", global_wav_size_send );

			strncat( buf2, bufInt, max_size-bufLen );
			bufLen = strlen( buf2 );
			tagLen = strlen( "TAG-MOH--ERR_POST_WAV_SIZE_SEND--MOH-TAG" );
			strncat( buf2, offset+tagLen, max_size-bufLen );
			strcpy( buf, buf2 );
			//print_debug( "New buffer is:%s", buf );
		}
		else
			oneMore = 0;	// STOP
	}
}
#endif
/* @ED@alize500@XTSce50587@051123@dop@           */


