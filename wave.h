/* wave.h - Copyright (c) 1996, 1998 by Timothy J. Weber */

#ifndef __WAVE_H
#define __WAVE_H

// Wave constant , see MICROSOFT wave definition
#define  WAVE_FORMAT_PCM			1 
#define	 WAVE_FORMAT_MULAW      7	//5 : BUG CORRECTION
#define  WAVE_FORMAT_ALAW			6
#define	 WAVE_FORMAT_G726_ADPCM	100

#define DEBIT_40KBS 40
#define DEBIT_32KBS 32
#define DEBIT_24KBS 24
#define DEBIT_16KBS 16

/* Headers required to use this module */
#include <stdio.h>
#include "rifffile.h"

/***************************************************************************
	macros, constants, and enums
***************************************************************************/

/***************************************************************************
	typedefs, structs, classes
***************************************************************************/

class WaveFile
{
public:
	WaveFile();
	~WaveFile();

/* @BD@OXOC_3.2@CROXOC-ESSAI@190325@degtoun1@ */
/*
 *	bool OpenRead(const char* name);
 */
/* @ED@OXOC_3.2@CROXOC-ESSAI@190325@degtoun1@ */

/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
	bool OpenRead(const char* name, char* sender, char * str_entity);
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */
	bool OpenWrite(const char* name);
	bool OpenCCitt(const char* name, bool ReadOnly);

	bool ResetToStart();
	bool Close();
	
	unsigned short GetFormatType() const
		{ return formatType; };
	void SetFormatType(unsigned short type)
		{ formatType = type; changed = true; };
	bool IsCompressed() const
		{ return formatType != 1; };

	unsigned short GetNumChannels() const
		{ return numChannels; };
	void SetNumChannels(unsigned short num)
		{ numChannels = num; changed = true; };

	unsigned long GetSampleRate() const
		{ return sampleRate; };
	void SetSampleRate(unsigned long rate)
		{ sampleRate = rate; changed = true; };

	unsigned long GetBytesPerSecond() const
		{ return bytesPerSecond; };
	void SetBytesPerSecond(unsigned long bytes)
		{ bytesPerSecond = bytes; changed = true; };

	unsigned short GetBytesPerSample() const
		{ return bytesPerSample; };
	void SetBytesPerSample(unsigned short bytes)
		{ bytesPerSample = bytes; changed = true; };

	unsigned short GetBitsPerChannel() const
		{ return bitsPerChannel; };
	void SetBitsPerChannel(unsigned short bits)
		{ bitsPerChannel = bits; changed = true; };

	unsigned long GetNumSamples() const
		{ return (GetBytesPerSample())?
			GetDataLength() / GetBytesPerSample(): 0; };
	void SetNumSamples(unsigned long num)
		{ SetDataLength(num * GetBytesPerSample()); };

	float GetNumSeconds() const
		{ return GetBytesPerSecond()?
			float(GetDataLength()) / GetBytesPerSecond(): 0; };

	unsigned long GetDataLength() const
		{ return dataLength; };
	void SetDataLength(unsigned long numBytes)
		{ dataLength = numBytes; changed = true; };

	bool FormatMatches(const WaveFile& other);

	void CopyFormatFrom(const WaveFile& other);

	FILE* GetFile()
		{ return readFile? readFile->filep(): writeFile; };

	RiffFile* GetRiffFile()
		{ return readFile? readFile : 0; };

	bool WriteHeaderToFile(FILE* fp);
	bool WriteExtraItemToFile();

	bool GetFirstExtraItem(std::string& type, std::string& value);
	bool GetNextExtraItem(std::string& type, std::string& value);

	bool SetHeader(int fmt,int rate);

	bool CopyFrom(WaveFile& other);
	void ConvertFromPCM(const char *infilename, const char *outfilename);
   bool ConvertFrom(WaveFile& other, bool ToADPCM, int law,int rate);

   void ConvertFromADPCM(const char *infilename, const char *outfilename);

	bool ConvertFromCCitt();
	bool ConvertToCCitt(int SamplesToSuppress);

	const char* GetError() const
		{ return error; };
	void ClearError()
		{ error = 0; };

protected:
	RiffFile* readFile;
	FILE* writeFile;
	FILE* CCittFile;

	unsigned short formatType;
	unsigned short numChannels;
	unsigned long sampleRate;
	unsigned long bytesPerSecond;
	unsigned short bytesPerSample;
	unsigned short bitsPerChannel;
	unsigned long dataLength;
	unsigned long ExtraLength;		// length of LIST/INFO elements
	unsigned short ExtFmtLength;	// length of extended fmt (compressed)
	
	char* ExtFmt;
	const char* error;
	bool changed;  // true if any parameters changed since the header was last written
};

/***************************************************************************
	public variables
***************************************************************************/

#ifndef IN_WAVE
#endif

/***************************************************************************
	function prototypes
***************************************************************************/

#endif
/* __WAVE_H */

