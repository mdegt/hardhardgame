// Proto
/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*
 *char mainProg0();
 *void print_error( int error, int subError );
 *void go_to_next_group( void );
 *int	 read_line( char *line, int maxChar );
 */
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
   char *mainProg1( char *sender, char *operation);
   void print_error( int error, int subError, char *sender);
   void print_api_error( int error, int subError, char *sender);
   void send_error_to_api(int error, int subError);
   int read_line( char *line, int maxChar, char *sender);
   void search_key( char *key, char * sender );
   void go_to_next_group(char *sender);
   int isfilexists(const char * filename);

/* @BD@OXO@CROXOC-4249@191003@degtoun1@ */
/*
 *   char *get_filename_n_path(char *str_entity, char *destination, char *sender );
 */
/* @ED@OXO@CROXOC-4249@191003@degtoun1@ */
/* @BA@OXO@CROXOC-4249@191003@degtoun1@ */
   char *get_filename_n_path(char *str_entity, char *sender );
/* @EA@OXO@CROXOC-4249@191003@degtoun1@ */
/* @BD@OXO@CROXOC-4249@191003@degtoun1@ */
/*
 *   void get_audio_file_from_system(char *str_entity, char *destination, char *sender);
 */
/* @ED@OXO@CROXOC-4249@191003@degtoun1@ */
/* @BA@OXO@CROXOC-4249@191003@degtoun1@ */
   void get_audio_file_from_system(char *str_entity,  char *sender);
/* @EA@OXO@CROXOC-4249@191003@degtoun1@ */
/* @BD@OXO@CROXOC-4249@191003@degtoun1@ */
/*
 *   void delete_audio_file_from_system(char *str_entity, char *destination, char *sender);
 */
/* @ED@OXO@CROXOC-4249@191003@degtoun1@ */
/* @BA@OXO@CROXOC-4249@191003@degtoun1@ */
   void delete_audio_file_from_system(char *str_entity,  char *sender);
/* @EA@OXO@CROXOC-4249@191003@degtoun1@ */
/* @BD@OXO@CROXOC-4249@191003@degtoun1@ */
/*
 *   int put_audio_file_to_system(char *str_entity, char *destination, char * sender);
 */
/* @ED@OXO@CROXOC-4249@191003@degtoun1@ */
/* @BA@OXO@CROXOC-4249@191003@degtoun1@ */
   int put_audio_file_to_system(char *str_entity, char * sender);
/* @EA@OXO@CROXOC-4249@191003@degtoun1@ */

   char get_post_param_from_WedDiag(char *sender);
   char * get_api_operation(char *sender);
   char *get_post_param_from_api(char *sender, char *operation);
   char *ltrim(char *str, const char *seps);
   char *rtrim(char *str, const char *seps);
   char *trim(char *str, const char *seps);
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
   void print_debug( const char *fmt, ... );
   void open_html( int error, int subError );
/* @BD@alize500@XTSce50587@051123@dop@           */
/* void print_default_HTML( void ); */
/* void search_key( char *key ); */
/* void add_info( char *buf, int max_size ); */
/* @ED@alize499@XTSce50587@051123@dop@           */
