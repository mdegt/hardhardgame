#define LAW_NONE 0
#define LAW_A 1
#define	LAW_MU 2

#define	DEBIT_40KBS 40
#define	DEBIT_32KBS 32
#define	DEBIT_24KBS 24
#define	DEBIT_16KBS 16

extern int CodeurG726(int ENTREE,int LOI,int DEBIT,int RAZ);
extern int QuickCodeurG726(int ENTREE);   
extern int InitCodeurG726 (int newLOI, int newDEBIT);

extern int DeCodeurG726(int ENTREE,int LOI,int DEBIT,int RAZ);
extern int QuickDeCodeurG726(int ENTREE);
extern int InitDeCodeurG726 (int newLOI,int newDEBIT);
