/* rifffile.h - Copyright (c) 1996, 1998 by Timothy J. Weber */

#ifndef __RIFFFILE_H
#define __RIFFFILE_H

/* Headers required to use this module */
#include <stack>
#include <string>
#include <vector>

#include <stdio.h>

/***************************************************************************
	macros, constants, and enums
***************************************************************************/

struct TypeRecord {
	char* typeName;  // four-letter name
	char* realName;  // English name
};

const int numExtraTypes = 24;	// eba_ : Add type int
const TypeRecord extraTypes[numExtraTypes] = {
	{ "DISP", "Display name      " },
	{ "IARL", "Archival location " },
	{ "IART", "Artist            " },
	{ "ICMS", "Commissioned      " },
	{ "ICMT", "Comments          " },
	{ "ICOP", "Copyright         " },
	{ "ICRD", "Creation date     " },
	{ "ICRP", "Cropped           " },
	{ "IDIM", "Dimensions        " },
	{ "IDPI", "Dots Per Inch     " },
	{ "IENG", "Engineer          " },
	{ "IGNR", "Genre             " },
	{ "IKEY", "Keywords          " },
	{ "ILGT", "Lightness         " },
	{ "IMED", "Medium            " },
	{ "INAM", "Name              " },
	{ "IPLT", "Palette Setting   " },
	{ "IPRD", "Product           " },
	{ "ISBJ", "Subject           " },
	{ "ISFT", "Software          " },
	{ "ISHP", "Sharpness         " },
	{ "ISRC", "Source            " },
	{ "ISRF", "Source Form       " },
	{ "ITCH", "Technician        " },
};

/***************************************************************************
	typedefs, structs, classes
***************************************************************************/

class RiffFile;
class RiffChunk {
public:
	char name[5];
	unsigned long size;  // the length, read from the second chunk header entry
	char subType[5];  // valid for RIFF and LIST chunks
	long start;  // the file offset in bytes of the chunk contents
	long after;  // the start of what comes after this chunk

	// initialize at the file's current read position, and mark the file as bad
	// if there's an error.
	RiffChunk()
		{};
	RiffChunk(RiffFile& file);

	bool operator < (const RiffChunk& other) const
		{ return start < other.start; };
	bool operator == (const RiffChunk& other) const
	{ return strcmp(name, other.name) == 0
		&& size == other.size
		&& strcmp(subType, other.subType) == 0
		&& start == other.start; };

};

class RiffFile {
	FILE* fp;

	unsigned long formSize;

	std::stack<RiffChunk, std::vector<RiffChunk> > chunks;


public:

/* @BD@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
/*	RiffFile(const char *name);*/
/* @ED@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */

/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
RiffFile(const char *name, char *sender);
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
	~RiffFile();

	bool rewind();
	bool push(const char* chunkType = 0);
	bool pop();
	long chunkSize() const;
	const char* chunkName() const;
	const char* subType() const;
	bool getNextExtraItem(std::string& type, std::string& value);
	FILE* filep()
		{ return fp; };


	bool GetExtraItemOfType(const std::string& type, std::string& value);
	bool SetExtraItem(const std::string& type, std::string& value);

protected:
	bool readExtraItem(std::string& type, std::string& value);
};

/***************************************************************************
	public variables
***************************************************************************/

#ifndef IN_RIFFFILE
#endif

/***************************************************************************
	function prototypes
***************************************************************************/
//#ifdef TEST_RIFFFILE		// eba_
void showChunk(RiffFile& file, int indent, bool expandLists);
//#endif					// eba_

#endif
/* __RIFFFILE_H */
