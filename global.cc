// Global.cpp
#include "global.h"

char	global_main_error[MAX_MAIN_ERROR_STRING_LENGTH];
short	global_channels;
short	global_BitsPerSample;
long	global_SamplesPerSec;
unsigned long global_wav_size_send;
unsigned int nb_moh_capacity;
/* @BA@OXO@CROXOC-4249@191007@degtoun1@ */
unsigned int nb_prea_capacity;
/* @EA@OXO@CROXOC-4249@191007@degtoun1@ */

/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
unsigned int nb_aa_welcome_capacity;
unsigned int  nb_aa_goodbye_capacity;

unsigned int nb_at_welcome_capacity;
unsigned int nb_at_goodbye_capacity;
unsigned int nb_gal_audio_capacity;
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */

