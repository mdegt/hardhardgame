#ifndef _EXTERN_H
#define _EXTERN_H

extern char		global_main_error[MAX_MAIN_ERROR_STRING_LENGTH];
extern short	global_channels;
extern long		global_SamplesPerSec;
extern short	global_BitsPerSample;
extern unsigned long global_wav_size_send;
extern unsigned int nb_moh_capacity;
/* @BA@OXO@CROXOC-4249@191007@degtoun1@ */
extern unsigned int nb_prea_capacity;
extern char destination[17];
/* @EA@OXO@CROXOC-4249@191007@degtoun1@ */

/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
extern unsigned int nb_aa_welcome_capacity;
extern unsigned int nb_aa_goodbye_capacity;

extern unsigned int nb_at_welcome_capacity;
extern unsigned int nb_at_goodbye_capacity;
extern unsigned int nb_gal_audio_capacity;
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */
#endif

