
/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%															%%%%%%
		%%%%%%		   DESCRIPTION DES MODULES MICDA :					%%%%%%
        %%%%%%															%%%%%%
		%%%%%%		   Debits 16, 24, 32 et 40 kbits/s					%%%%%%
        %%%%%%															%%%%%%
		%%%%%%		      Recommandation CCITT G726						%%%%%%
		%%%%%%															%%%%%%
        %%%%%%               Francois PINIER ( Avril 1995 )				%%%%%%
        %%%%%%															%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	REMARQUES :
	-----------

1) 		LAW = LAW_A    ===> Codage en loi A
		LAW = LAW_MU    ===> Codage en loi mu

		Seuls les modules suivants sont fonction de la loi de codage:

			* COMPRESS
			* EXPAND
			* SYNC

2)		DEBIT = 40    ===> Debit MICDA = 40 kbits/s
		DEBIT = 32    ===> Debit MICDA = 32 kbits/s
		DEBIT = 24    ===> Debit MICDA = 24 kbits/s
		DEBIT = 16    ===> Debit MICDA = 16 kbits/s

		Seuls les modules suivants sont fonction du debit MICDA :

			* FUNCTF
			* FUNCTW
			* QUAN
			* RECONST
			* SYNC
			* UPB

3) Le module QUAN est utilise uniquement dans le codeur

4) Les modules COMPRESS et SYNC sont utilises uniquement dans le decodeur

*/

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%		Definition des types et constantes		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */
#include "g726.h"

#define puissance17	131072
#define puissance16	65536
#define puissance15	32768
#define puissance14	16384

#define MAX_17bits	131071
#define MAX_16bits	65535
#define MAX_15bits	32767
#define MAX_14bits	16383

#define A2UL		12288		/* Parametres de la fonction LIMC */
#define A2LL		53248

#define OME		15360		/* Parametre de la fonction LIMD */

#define LIMO_high 57344
#define LIMO_middle 32767
#define LIMO_mow 8191

typedef int TABLEAU3[3];		/* Definition d'un tableau contenant trois entiers */
typedef int TABLEAU6[6];		/* Definition d'un tableau contenant six entiers   */

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du code MIC loi A ou             %%%
		%%%     loi mu en code MIC uniforme (linearisation)		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int EXPAND(int S,int LAW)
{
	int IN, SL, SIGNE, SS, SSM, SSQ, SSS;

	SIGNE = (S & 128) >> 7;			/* SIGNE = 1 si amplitude positive */

	if (LAW==LAW_A)				/* Cas de la loi A */
	    {
	     IN = (S ^ 213) & 127;

	     if (IN < 32)		SS =    1 +   2 * IN;
	     else if (IN < 48)	SS =   66 +   4 * (IN - 32);
	     else if (IN < 64)	SS =  132 +   8 * (IN - 48);
	     else if (IN < 80)	SS =  264 +  16 * (IN - 64);
	     else if (IN < 96)	SS =  528 +  32 * (IN - 80);
	     else if (IN < 112)	SS = 1056 +  64 * (IN - 96);
	     else				SS = 2112 + 128 * (IN - 112);

	     SS  += (1 - SIGNE) << 12;		/* Le bit de signe est egal a un pour les valeurs negatives */
	     SSS = SS >> 12;
	     SSM = SS & 4095;
	     SSQ = SSM << 1;
	    }
	else					/* Cas de la loi mu */
	    {
	     IN = (S ^ 255) & 127;

	     if (IN < 16)		SS =          2 * IN;
	     else if (IN < 32)	SS =   33 +   4 * (IN - 16);
	     else if (IN < 48)	SS =   99 +   8 * (IN - 32);
	     else if (IN < 64)	SS =  231 +  16 * (IN - 48);
	     else if (IN < 80)	SS =  495 +  32 * (IN - 64);
	     else if (IN < 96) 	SS = 1023 +  64 * (IN - 80);
	     else if (IN < 112) SS = 2079 + 128 * (IN - 96);
	     else				SS = 4191 + 256 * (IN - 112);

	     SS  += (1 - SIGNE) << 13; 		/* Le bit de signe est egal a un pour les valeurs negatives */
	     SSS = SS >> 13;
	     SSQ = SS & 8191;
	    }

	SL = (SSS) ? (puissance14 - SSQ) & MAX_14bits : SSQ;
	return SL;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de calcul de la difference par soustraction    %%%
		%%%     du signal estime et du signal d'entree (ou du signal    %%%
		%%%     reconstitue quantifie dans le decodeur)			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int SUBTA(int SL,int SE)

    {
	int D, SEI, SLI, SES, SLS;

	SLS = SL >> 13;
	SLI = (SLS) ? SL + 49152 : SL;
	SES = SE >> 14;
	SEI = (SES) ? SE + puissance15 : SE; 
	D   = (SLI - SEI + puissance16) & MAX_16bits;
	return D;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du signal de difference du       %%%
		%%%     domaine lineaire au domaine logarithmique		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

void LOG(int D,int *DL,int *DS)
    {
	int DQM, EXP, MANT;

	*DS = D >> 15;
	DQM = (*DS) ? (puissance16 - D) & MAX_15bits : D;

	if (DQM < 2)			EXP = 0;
	else if (DQM < 4)		EXP = 1;
	else if (DQM < 8)		EXP = 2;
	else if (DQM < 16)		EXP = 3;
	else if (DQM < 32)		EXP = 4;
	else if (DQM < 64)		EXP = 5;
	else if (DQM < 128)		EXP = 6;
	else if (DQM < 256)		EXP = 7;
	else if (DQM < 512)		EXP = 8;
	else if (DQM < 1024)	EXP = 9;
	else if (DQM < 2048)	EXP = 10;
	else if (DQM < 4096)	EXP = 11;
	else if (DQM < 8192)	EXP = 12;
	else if (DQM < puissance14)	EXP = 13;
	else				EXP = 14;

	MANT = ((DQM << 7) >> EXP) & 127;
	*DL  = (EXP << 7) + MANT;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de quantification du signal de difference      %%%
		%%%     dans le domaine logarithmique				%%%
		%%%		( Utilise dans le codeur uniquement )		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int QUAN(int DLN,int DS,int DEBIT)
    
    {
	int I;

	switch (DEBIT)
	    {
		case 40 :				/* Tables de quantification pour un debit de 40 kbits/s */
				if (DS)
				    {
					if (DLN < 68)			I = 29;
					else if (DLN < 139)		I = 28;
					else if (DLN < 198)		I = 27;
					else if (DLN < 250)		I = 26;
					else if (DLN < 298)		I = 25;
					else if (DLN < 339)		I = 24;
					else if (DLN < 378)		I = 23;
					else if (DLN < 413)		I = 22;
					else if (DLN < 445)		I = 21;
					else if (DLN < 475)		I = 20;
					else if (DLN < 502)		I = 19;
					else if (DLN < 528)		I = 18;
					else if (DLN < 553)		I = 17;
					else if (DLN < 2048)		I = 16;
					else if (DLN < 3974)		I = 31;
					else if (DLN < 4080)		I = 30;
					else				I = 29;
				    }
				else
				    {
					if (DLN < 68)			I = 2;
					else if (DLN < 139)		I = 3;
					else if (DLN < 198)		I = 4;
					else if (DLN < 250)		I = 5;
					else if (DLN < 298)		I = 6;
					else if (DLN < 339)		I = 7;
					else if (DLN < 378)		I = 8;
					else if (DLN < 413)		I = 9;
					else if (DLN < 445)		I = 10;
					else if (DLN < 475)		I = 11;
					else if (DLN < 502)		I = 12;
					else if (DLN < 528)		I = 13;
					else if (DLN < 553)		I = 14;
					else if (DLN < 2048)	I = 15;
					else if (DLN < 3974)	I = 31;
					else if (DLN < 4080)	I = 1;
					else					I = 2;
				    }
				break;
		case 32 :				/* Tables de quantification pour un debit de 32 kbits/s */
				if (DS)
				    {
					if (DLN < 80)			I = 14;
					else if (DLN < 178)		I = 13;
					else if (DLN < 246)		I = 12;
					else if (DLN < 300)		I = 11;
					else if (DLN < 349)		I = 10;
					else if (DLN < 400)		I = 9;
					else if (DLN < 2048)		I = 8;
					else if (DLN < 3972)		I = 15;
					else				I = 14;
				    }
				else
				    {
					if (DLN < 80)			I = 1;
					else if (DLN < 178)		I = 2;
					else if (DLN < 246)		I = 3;
					else if (DLN < 300)		I = 4;
					else if (DLN < 349)		I = 5;
					else if (DLN < 400)		I = 6;
					else if (DLN < 2048)		I = 7;
					else if (DLN < 3972)		I = 15;
					else				I = 1;
				    }
				break;
		case 24 :				/* Tables de quantification pour un debit de 24 kbits/s */
				if (DS)
				    {
					if (DLN < 8)			I = 7;
					else if (DLN < 218)		I = 6;
					else if (DLN < 331)		I = 5;
					else if (DLN < 2048)		I = 4;
					else				I = 7;
				    }
				else
				    {
					if (DLN < 8)			I = 7;
					else if (DLN < 218)		I = 1;
					else if (DLN < 331)		I = 2;
					else if (DLN < 2048)		I = 3;
					else				I = 7;
				    }
				break;
		default :				/* Tables de quantification pour un debit de 16 kbits/s */
				if (DS)
				    {
					if (DLN < 261)			I = 3;
					else if (DLN < 2048)		I = 2;
					else				I = 3;
				    }
				else
				    {
					if (DLN < 261)			I = 0;
					else if (DLN < 2048)		I = 1;
					else				I = 0;
				    }
				break;
	    }
	return I;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de cadrage de la version logarithmique du      %%%
		%%%     signal de difference en soustrayant le facteur		%%%
		%%%     d'echelle						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int SUBTB(int DL,int Y)
   
    {
	int DLN;

	DLN = (DL + 4096 - (Y >> 2)) & 4095;
	return DLN;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'addition du facteur d'echelle a la version   %%%
		%%%     logarithmique du signal de difference quantifie		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int ADDA(int DQLN,int  Y)
  
    {
	int DQL;

	DQL = (DQLN + (Y >> 2)) & 4095;
	return DQL;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du signal de difference          %%%
		%%%     quantifie du domaine logarithmique dans le domaine	%%%
		%%%     lineaire						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int ANTILOG(int DQL,int DQS)
    
    {
	int DEX, DMN, DQ, DQMAG, DQT, DS;

	DS    = DQL >> 11;
	DEX   = (DQL >> 7) & 15;
	DMN   = DQL & 127;
	DQT   = DMN + 128;
	DQMAG = (DS) ? 0 : (DQT << 7) >> (14 - DEX);
	DQ    = (DQS << 15) + DQMAG;
	return DQ;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de reconstitution du signal de difference      %%%
		%%%     quantifie dans le domaine logarithmique			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

void RECONST(int I,int * DQLN,int * DQS,int DEBIT)
 {

	switch (DEBIT)
	    {
		case 40 :				/* Tables de reconstitution pour un debit de 40 kbits/s */
				switch (I)
					{
						case 0  : case 31 : *DQLN = 2048;  break;
						case 1  : case 30 : *DQLN = 4030;  break;
						case 2  : case 29 : *DQLN = 28;    break;
						case 3  : case 28 : *DQLN = 104;   break;
						case 4  : case 27 : *DQLN = 169;   break;
						case 5  : case 26 : *DQLN = 224;   break;
						case 6  : case 25 : *DQLN = 274;   break;
						case 7  : case 24 : *DQLN = 318;   break;
						case 8  : case 23 : *DQLN = 358;   break;
						case 9  : case 22 : *DQLN = 395;   break;
						case 10 : case 21 : *DQLN = 429;   break;
						case 11 : case 20 : *DQLN = 459;   break;
						case 12 : case 19 : *DQLN = 488;   break;
						case 13 : case 18 : *DQLN = 514;   break;
						case 14 : case 17 : *DQLN = 539;   break;
						default		  : *DQLN = 566;   break;
					}
				*DQS = I >> 4;
				break;
		case 32 :				/* Tables de reconstitution pour un debit de 32 kbits/s */
				switch (I)
					{
						case 0  : case 15 : *DQLN = 2048;  break;
						case 1  : case 14 : *DQLN = 4;     break;
						case 2  : case 13 : *DQLN = 135;   break;
						case 3  : case 12 : *DQLN = 213;   break;
						case 4  : case 11 : *DQLN = 273;   break;
						case 5  : case 10 : *DQLN = 323;   break;
						case 6  : case 9  : *DQLN = 373;   break;
						default		  : *DQLN = 425;   break;
					}
				*DQS = I >> 3;
				break;
		case 24 :				/* Tables de reconstitution pour un debit de 24 kbits/s */
				switch (I)
					{
						case 0  : case 7 : *DQLN = 2048;  break;
						case 1  : case 6 : *DQLN = 135;   break;
						case 2  : case 5 : *DQLN = 273;   break;
						default		 : *DQLN = 373;   break;
					}
				*DQS = I >> 2;
				break;
		default :				/* Tables de reconstitution pour un debit de 16 kbits/s */
				switch (I)
					{
						case 0  : case 3 : *DQLN = 116;  break;
						default		 : *DQLN = 365;  break;
					}
				*DQS = I >> 1;
				break;
	    }

    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation du facteur d'echelle           %%%
		%%%     a adaptation rapide					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTD(int WI,int  Y)

    {
	int DIF, DIFS, DIFSX, YUT;

	DIF   = ((WI << 5) - Y + puissance17) & MAX_17bits;
	DIFS  = DIF >> 16;
	DIFSX = (DIFS) ? (DIF >> 5) + 4096 : DIF >> 5;
	YUT   = (Y + DIFSX) & 8191;
	return YUT;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation du facteur d'echelle           %%%
		%%%     a adaptation lente					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTE(int YUP,int YL)
      {
	int DIF, DIFS, DIFSX, YLP;

	DIF   = (YUP + ((1048576 - YL) >> 6)) & MAX_14bits;
	DIFS  = DIF >> 13;
	DIFSX = (DIFS) ? DIF + 507904 : DIF;
	YLP   = (YL + DIFSX) & 524287;
	return YLP;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de transposition de la valeur de sortie du     %%%
	%%%     quantificateur dans le domaine logarithmique du		%%%
	%%%     multiplicateur du facteur d'echelle			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FUNCTW(int I,int DEBIT)
    
    {
	int IM, IS, WI;

	switch (DEBIT)
	    {
		case 40 :				/* Tables de transposition pour un debit de 40 kbits/s */
				IS = I >> 4;
				IM = (IS) ? (31 - I) & 15 : I & 15;
				switch (IM)
					{
						case 0  : case 1 : WI = 14;    break;
						case 2  :	   WI = 24;    break;
						case 3  :          WI = 39;    break;
						case 4  :	   WI = 40;    break;
						case 5  :	   WI = 41;    break;
						case 6  :	   WI = 58;    break;
						case 7  :	   WI = 100;   break;
						case 8  :	   WI = 141;   break;
						case 9  :	   WI = 179;   break;
						case 10 :	   WI = 219;   break;
						case 11 :	   WI = 280;   break;
						case 12 :	   WI = 358;   break;
						case 13 :	   WI = 440;   break;
						case 14 :	   WI = 529;   break;
						default :	   WI = 696;   break;
					}
				break;
		case 32 :				/* Tables de transposition pour un debit de 32 kbits/s */
				IS = I >> 3;
				IM = (IS) ? (15 - I) & 7 : I & 7;
				switch (IM)
					{
						case 0  : WI = 4084;  break;
						case 1  : WI = 18;    break;
						case 2  : WI = 41;    break;
						case 3  : WI = 64;    break;
						case 4  : WI = 112;   break;
						case 5  : WI = 198;   break;
						case 6  : WI = 355;   break;
						default : WI = 1122;  break;
					}
				break;
		case 24 :				/* Tables de transposition pour un debit de 24 kbits/s */
				IS = I >> 2;
				IM = (IS) ? (7 - I) & 3 : I & 3;
				switch (IM)
					{
						case 0  : WI = 4092;  break;
						case 1  : WI = 30;    break;
						case 2  : WI = 137;   break;
						default : WI = 582;   break;
					}
				break;
		default :				/* Tables de transposition pour un debit de 16 kbits/s */
				IS = I >> 1;
				IM = (IS) ? (3 - I) & 1 : I & 1;
				switch (IM)
					{
						case 0  : WI = 4074;  break;
						default : WI = 439;   break;
					}
				break;
	    }
	return WI;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de limitation du facteur d'echelle du          %%%
		%%%     quantificateur						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int LIMB(int YUT)
 
    {
	int GELL, GEUL, YUP;

	GEUL = ((YUT + 11264) & MAX_14bits) >> 13;
	GELL = ((YUT + 15840) & MAX_14bits) >> 13;

	if (GELL == 1)		YUP = 544;
	else if (GEUL == 0)	YUP = 5120;
	else			YUP = YUT;

	return YUP;
    }

	/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de limitation du facteur d'echelle du          %%%
		%%%     quantificateur		14 bits 2's complement				%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int LIMO(int SR)
 
    {int SO;

	if (SR<8192)		SO = SR & 16383;
	else if (SR<32768)	SO = 8191;
	else if (SR<57344)	SO = 57344;
	else				SO = SR & 16383;

	return SO;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de formation d'une combinaison lineaire des    %%%
	%%%     facteurs d'echelle a adaptation rapide et lente du	%%%
	%%%	quantificateur						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int MIX(int AL,int  YU,int  YL)
   
    {
	int DIF, DIFM, DIFS, PROD, PRODM, Y;

	DIF  = (YU + puissance14 - (YL >> 6)) & MAX_14bits;
	DIFS = DIF >> 13;

	if (DIFS)
	    {
	     DIFM  = (puissance14 - DIF) & 8191;
	     PRODM = (DIFM * AL) >> 6;
	     PROD  = (puissance14 - PRODM) & MAX_14bits;
	    }
	else
	    {
	     DIFM  = DIF;
	     PRODM = (DIFM * AL) >> 6;
	     PROD  = PRODM;
	    }

	Y = ((YL >> 6) + PROD) & 8191;
	return Y;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation de la moyenne a court terme    %%%
	%%%     de F(I)							%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTA(int FI,int  DMS)
    
    {
	int DIF, DIFS, DIFSX, DMSP;

	DIF   = ((FI << 9) - DMS + 8192) & 8191;
	DIFS  = DIF >> 12;
	DIFSX = (DIFS) ? (DIF >> 5) + 3840 : DIF >> 5;
	DMSP  = (DIFSX + DMS) & 4095;
	return DMSP;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation de la moyenne a long terme     %%%
		%%%     de F(I)							%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTB(int FI,int  DML)
    
    {
	int DIF, DIFS, DIFSX, DMLP;

	DIF   = ((FI << 11) - DML + puissance15) & MAX_15bits;
	DIFS  = DIF >> 14;
	DIFSX = (DIFS) ? (DIF >> 7) + 16128 : DIF >> 7;
	DMLP  = (DIFSX + DML) & MAX_14bits;
	return DMLP;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de filtrage passe-bas du parametre de	    	%%%
		%%%     controle de la vitesse					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FILTC(int AX,int  AP)
    
    {
	int DIF, DIFS, DIFSX, APP;

	DIF   = ((AX << 9) - AP + 2048) & 2047;
	DIFS  = DIF >> 10;
	DIFSX = (DIFS) ? (DIF >> 4) + 896 : DIF >> 4;
	APP   = (DIFSX + AP) & 1023;
	return APP;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de projection de la valeur de sortie du	%%%
	%%%     quantificateur sur la fonction F(I)			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FUNCTF(int I,int  DEBIT)
 
    {
	int FI, IM, IS;

	switch (DEBIT)
	    {
		case 40 :				/* Tables de transposition pour un debit de 40 kbits/s */
				IS = I >> 4;
				IM = (IS) ? (31 - I) & 15 : I & 15;
				switch (IM)
					{
						case 15 : case 14 :			       FI = 6;  break;
						case 13 :				       FI = 5;  break;
						case 12 :				       FI = 4;  break;
						case 11 :				       FI = 3;  break;
						case 10 :				       FI = 2;  break;
						case 9  : case 8  : case 7 : case 6 : case 5 : FI = 1;  break;
						default	:				       FI = 0;  break;
					}
				break;
		case 32 :				/* Tables de transposition pour un debit de 32 kbits/s */
				IS = I >> 3;
				IM = (IS) ? (15 - I) & 7 : I & 7;
				switch (IM)
					{
						case 7  :		    FI = 7;  break;
						case 6  :		    FI = 3;  break;
						case 5  : case 4 : case 3 : FI = 1;  break;
						default :		    FI = 0;  break;
					}
				break;
		case 24 :				/* Tables de transposition pour un debit de 24 kbits/s */
				IS = I >> 2;
				IM = (IS) ? (7 - I) & 3 : I & 3;
				switch (IM)
					{
						case 3  : FI = 7;  break;
						case 2  : FI = 2;  break;
						case 1  : FI = 1;  break;
						default : FI = 0;  break;
					}
				break;
		default :				/* Tables de transposition pour un debit de 16 kbits/s */
				IS = I >> 1;
				IM = (IS) ? (3 - I) & 1 : I & 1;
				switch (IM)

					{
						case 1  : FI = 7;  break;
						default : FI = 0;  break;
					}
				break;
	    }
	return FI;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de limitation du parametre de controle		%%%
		%%%     de la vitesse						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int LIMA(int AP)

    {
	int AL;

	AL = (AP < 256) ? AP >> 2 : 64;
	return AL;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de calcul de la valeur absolue de la	    	%%%
		%%%     difference des fonctions a court et a long terme de la	%%%
		%%%	suite des codes MICDA puis de realisation des		%%%
		%%%	comparaisons de seuils pour actualiser le parametre de	%%%
		%%%	controle de la vitesse d'adaptation du quantificateur	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int SUBTC(int DMSP,int  DMLP,int  TDP,int  Y)
    {
	int AX, DIF, DIFM, DIFS, DTHR;

	DIF  = ((DMSP << 2) - DMLP + puissance15) & MAX_15bits;
	DIFS = DIF >> 14;
	DIFM = (DIFS) ?  (puissance15 - DIF) & MAX_14bits : DIF;
	DTHR = DMLP >> 3;
	AX   = (Y > 1535 && DIFM < DTHR && TDP == 0) ? 0 : 1;
	return AX;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de declenchement du controle de la vitesse    	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int TRIGA(int TR,int  APP)
  
    {
	int APR;

	APR = (TR) ? 256 : APP;
	return APR;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'addition des sorties du predicteur pour	%%%
	%%%     former la valeur estimee partielle du signal		%%%
	%%%	(a partir du predicteur du sixieme ordre) et la valeur	%%%
	%%%	estimee du signal					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

void ACCUM(int WA1,int  WA2,TABLEAU6 WB,int * SE,int * SEZ)
   {
	int SEI, SEZI;

	SEZI = (((((((((WB[0] + WB[1]) & MAX_16bits) + WB[2]) & MAX_16bits) + WB[3]) & MAX_16bits) + WB[4]) & MAX_16bits) + WB[5]) & MAX_16bits;
	SEI  = (((SEZI + WA2) & MAX_16bits) + WA1) & MAX_16bits;
	*SEZ = SEZI >> 1;	
	*SE  = SEI  >> 1;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'addition du signal de difference quantifie   %%%
	%%%     et de la valeur estimee du signal pour former le	%%%
	%%%	signal reconstitue					%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int ADDB(int DQ,int  SE)
 
    {
	int DQI, DQS, SEI, SES, SR;

	DQS = DQ >> 15;
	DQI = (DQS) ? (puissance16 - (DQ & MAX_15bits)) & MAX_16bits : DQ;
	SES = SE >> 14;
	SEI = (SES) ? SE + puissance15 : SE;
	SR  = (DQI + SEI) & MAX_16bits;
	return SR;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de calcul du signe de la somme du signal de    %%%
	%%%     difference quantifie et de la valeur estimee partielle	%%%
	%%%	du signal						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

void ADDC(int DQ,int  SEZ,int * PK0,int * SIGPK)
    {
	int DQI, DQS, DQSEZ, SEZI, SEZS;

	DQS    = DQ >> 15;
	DQI    = (DQS) ? (puissance16 - (DQ & MAX_15bits)) & MAX_16bits : DQ;
	SEZS   = SEZ >> 14;
	SEZI   = (SEZS) ? SEZ + puissance15 : SEZ;
	DQSEZ  = (DQI + SEZI) & MAX_16bits;
	*PK0   = DQSEZ >> 15;
	*SIGPK = (DQSEZ) ? 0 : 1;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du code amplitude-signe a	%%%
	%%%     16 bits en code virgule flottante			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FLOATA(int DQ)
 
    {
	int DQ0, DQS, EXP, MAG, MANT;

	DQS = DQ >> 15;
	MAG = DQ & MAX_15bits;

	if (MAG == 0)			EXP = 0;
	else if (MAG == 1)		EXP = 1;
	else if (MAG < 4)		EXP = 2;
	else if (MAG < 8)		EXP = 3;
	else if (MAG < 16)		EXP = 4;
	else if (MAG < 32)		EXP = 5;
	else if (MAG < 64)		EXP = 6;
	else if (MAG < 128)		EXP = 7;
	else if (MAG < 256)		EXP = 8;
	else if (MAG < 512)		EXP = 9;
	else if (MAG < 1024)		EXP = 10;
	else if (MAG < 2048)		EXP = 11;
	else if (MAG < 4096)		EXP = 12;
	else if (MAG < 8192)		EXP = 13;
	else if (MAG < puissance14)	EXP = 14;
	else				EXP = 15;

	MANT = (MAG) ? (MAG << 6) >> EXP : 32;
	DQ0  = (DQS << 10) + (EXP << 6) + MANT;
	return DQ0;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du code complement a deux    	%%%
	%%%     (16 bits) en code virgule flottante			%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FLOATB(int SR)
 
    {
	int EXP, MAG, MANT, SR0, SRS;

	SRS = SR >> 15;
	MAG = (SRS) ? (puissance16 - SR) & MAX_15bits : SR;

	if (MAG == 0)			EXP = 0;
	else if (MAG == 1)		EXP = 1;
	else if (MAG < 4)		EXP = 2;
	else if (MAG < 8)		EXP = 3;
	else if (MAG < 16)		EXP = 4;
	else if (MAG < 32)		EXP = 5;
	else if (MAG < 64)		EXP = 6;
	else if (MAG < 128)		EXP = 7;
	else if (MAG < 256)		EXP = 8;
	else if (MAG < 512)		EXP = 9;
	else if (MAG < 1024)		EXP = 10;
	else if (MAG < 2048)		EXP = 11;
	else if (MAG < 4096)		EXP = 12;
	else if (MAG < 8192)		EXP = 13;
	else if (MAG < puissance14)	EXP = 14;
	else				EXP = 15;

	MANT = (MAG) ? (MAG << 6) >> EXP : 32;
	SR0  = (SRS << 10) + (EXP << 6) + MANT;
	return SR0;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de multiplication des coefficients de    	%%%
	%%%     prediction par le signal de difference quantifie ou le	%%%
	%%%	signal reconstitue correspondant			%%%
	%%%	(la multiplication est effectuee en virgule flottante)	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int FMULT(int AN,int  SRN)

    {
	int ANEXP, ANMAG, ANMANT, ANS, SRNEXP, SRNMANT, SRNS, WAN, WANEXP, WANMAG, WANMANT, WANS;

	ANS   = AN >> 15;
	ANMAG = (ANS) ? (puissance14 - (AN >> 2)) & 8191 : AN >> 2;

	if (ANMAG == 0)			ANEXP = 0;
	else if (ANMAG == 1)		ANEXP = 1;
	else if (ANMAG < 4)		ANEXP = 2;
	else if (ANMAG < 8)		ANEXP = 3;
	else if (ANMAG < 16)		ANEXP = 4;
	else if (ANMAG < 32)		ANEXP = 5;
	else if (ANMAG < 64)		ANEXP = 6;
	else if (ANMAG < 128)		ANEXP = 7;
	else if (ANMAG < 256)		ANEXP = 8;
	else if (ANMAG < 512)		ANEXP = 9;
	else if (ANMAG < 1024)		ANEXP = 10;
	else if (ANMAG < 2048)		ANEXP = 11;
	else if (ANMAG < 4096)		ANEXP = 12;
	else				ANEXP = 13;

	ANMANT  = (ANMAG) ? (ANMAG << 6) >> ANEXP : 32;
	SRNS    = SRN >> 10;
	SRNEXP  = (SRN >> 6) & 15;
	SRNMANT = SRN & 63;
	WANS    = SRNS ^ ANS;
	WANEXP  = SRNEXP + ANEXP;
	WANMANT = ((SRNMANT * ANMANT) + 48) >> 4;
	WANMAG  = (WANEXP < 27) ? (WANMANT << 7) >> (26 - WANEXP) : ((WANMANT << 7) << (WANEXP - 26)) & MAX_15bits;
	WAN     = (WANS) ? (puissance16 - WANMAG) & MAX_16bits : WANMAG;
	return WAN;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de limitation du coefficient A2 du predicteur  %%%
	%%%     du second ordre						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int LIMC(int A2T)
 
    {
	int A2P;

	if (A2T > MAX_15bits && A2T <= A2LL)			A2P = A2LL;
	else if (A2T >= A2UL && A2T < puissance15)		A2P = A2UL;
	else							A2P = A2T;
	return A2P;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de limitation du coefficient A1 du predicteur  %%%
	%%%     du second ordre						%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int LIMD(int A1T,int  A2P)

    {
	int A1LL, A1P, A1UL;

	A1UL = (OME - A2P + puissance16) & MAX_16bits;
	A1LL = (A2P - OME + puissance16) & MAX_16bits;

	if (A1T > MAX_15bits && A1T <= A1LL)			A1P = A1LL;
	else if (A1T >= A1UL && A1T < puissance15)		A1P = A1UL;
	else							A1P = A1T;

	return A1P;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de declenchement du predicteur  		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int TRIGB(int TR,int  ANP)
 
    {
	int ANR;

	ANR = (TR) ? 0 : ANP;
	return ANR;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation du coefficient A1 du  		%%%
	%%%     predicteur du second ordre				%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int UPA1(int PK0,int  PK1,int  A1,int  SIGPK)
    {
	int A1S, A1T, PKS, UA1, UGA1, ULA1;

	PKS = PK0 ^ PK1;

	if (SIGPK == 1)			UGA1 = 0;
	else if (PKS == 0)		UGA1 = 192;
	else				UGA1 = 65344;

	A1S  = A1 >> 15;
	ULA1 = (A1S) ? (puissance16 - ((A1 >> 8) + 65280)) & MAX_16bits : (puissance16 - (A1 >> 8)) & MAX_16bits;
	UA1  = (UGA1 + ULA1) & MAX_16bits;
	A1T  = (A1 + UA1) & MAX_16bits;
	return A1T;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation du coefficient A2 du  		%%%
	%%%     predicteur du second ordre				%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */
	
int UPA2(int PK0,int  PK1,int  PK2,int  A1,int  A2,int  SIGPK)
    {
	int A1S, A2S, A2T, FA, FA1, PKS1, PKS2, UA2, UGA2, UGA2A, UGA2B, UGA2S, ULA2;

	PKS1  = PK0 ^ PK1;
	PKS2  = PK0 ^ PK2;
	UGA2A = (PKS2) ? 114688 : puissance14;
	A1S   = A1 >> 15;

	if (A1S == 0)			FA1 = (A1 < 8192) ? A1 << 2 : 8191 << 2;
	else				FA1 = (A1 > 57344) ? (A1 << 2) & MAX_17bits : 24577 << 2;

	FA    = (PKS1 == 1) ? FA1 : (puissance17 - FA1) & MAX_17bits;
	UGA2B = (UGA2A + FA) & MAX_17bits;
	UGA2S = UGA2B >> 16;

	if (SIGPK == 1)			UGA2 = 0;
	else if (UGA2S == 0)		UGA2 = UGA2B >> 7;
	else				UGA2 = (UGA2B >> 7) + 64512;

	A2S  = A2 >> 15;
	ULA2 = (A2S) ? (puissance16 - ((A2 >> 7) + 65024)) & MAX_16bits : (puissance16 - (A2 >> 7)) & MAX_16bits;
	UA2  = (UGA2 + ULA2) & MAX_16bits;
	A2T  = (A2 + UA2) & MAX_16bits;
	return A2T;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction d'actualisation des coefficients du  		%%%
	%%%     predicteur du sixieme ordre				%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int UPB(int UN,int  BN,int  DQ,int  DEBIT)
    {
	int BNP, BNS, DQMAG, UBN, UGBN, ULBN;

	DQMAG = DQ & MAX_15bits;

	if (DQMAG == 0)		UGBN = 0;
	else if (UN == 0)	UGBN = 128;
	else			UGBN = 65408;

	BNS = BN >> 15;

	if (DEBIT == 40)		/* Tables de transposition pour un debit de 40 kbits/s */

	    ULBN = (BNS) ? (puissance16 - ((BN >> 9) + 65408)) & MAX_16bits : (puissance16 - (BN >> 9)) & MAX_16bits;

	else				/* Tables de transposition pour les debits de 32, 24 et 16 kbits/s */

	    ULBN = (BNS) ? (puissance16 - ((BN >> 8) + 65280)) & MAX_16bits : (puissance16 - (BN >> 8)) & MAX_16bits;

	UBN = (UGBN + ULBN) & MAX_16bits;
	BNP = (BN + UBN) & MAX_16bits;
	return BNP;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction "OU EXCLUSIF" a un bit du signe du signal de  	%%%
	%%%     difference et du signe du signal de difference retarde	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int XOR(int DQN,int  DQ)
 
    {
	int DQNS, DQS, UN;

	DQS  = DQ >> 15;
	DQNS = DQN >> 10;
	UN   = DQS ^ DQNS;
	return UN;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de detection du signal a bande etroite  	%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int TONE(int A2P)
   
    {
	int TDP;

	TDP = (A2P > MAX_15bits && A2P < 53760) ? 1 : 0;
	return TDP;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de detection de transition	 		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int TRANS(int TD,int  YL,int  DQ)
   
    {
	int DQMAG, DQTHR, THR1, THR2, TR, YLFRAC, YLINT;

	DQMAG  = DQ & MAX_15bits;
	YLINT  = YL >> 15;
	YLFRAC = (YL >> 10) & 31;
	THR1   = (YLFRAC + 32) << YLINT;
	THR2   = (YLINT > 9) ? 31 << 10 : THR1;
	DQTHR  = (THR2 + (THR2 >> 1)) >> 1;
	TR     = (TD == 1 && DQMAG > DQTHR) ? 1 : 0;
	return TR;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de conversion du format MIC uniforme en  	%%%
		%%%     format MIC loi A ou loi mu ===> compression			%%%
		%%%	     ( Utilise dans le decodeur uniquement )			%%%
	    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int COMPRESS(int SR,int  LAW)
    
    {
	int IM, IMAG, INTS, IS, OVAL, SEGMENT, SP;

	INTS = 0 ;
	IS = SR >> 15;
	IM = (IS) ? (puissance16 - SR) & MAX_15bits : SR;

	if (LAW == LAW_MU)				/* Cas de la loi mu */
	    IMAG =  IM;
	else if (IS == 0)			/* Cas de la loi A  */
	    IMAG = IM >> 1;		
	else
	    IMAG = (IM + 1) >> 1;

	if (LAW == LAW_A)				/* Cas de la loi A */
	    {
	     if ((IS == 1) && (IMAG != 0))	/* Cas de l'echantillon negatif :					*/
		IMAG--;				/*	la limite de decision est incluse dans l'intervalle		*/

	     if (IMAG > 4095)			/* Cas de l'echantillon positif :					*/
		IMAG = 4095;			/*	la limite de decision n'est pas incluse dans l'intervalle 	*/

	     if (IMAG < 32)		SEGMENT = 0;
	     else if (IMAG < 64)	SEGMENT = 1;
	     else if (IMAG < 128)	SEGMENT = 2;
	     else if (IMAG < 256)	SEGMENT = 3;
	     else if (IMAG < 512)	SEGMENT = 4;
	     else if (IMAG < 1024)	SEGMENT = 5;
	     else if (IMAG < 2048)	SEGMENT = 6;
	     else			SEGMENT = 7;

	     switch (SEGMENT)
		{
			case 7 : INTS = (IMAG / 128) - 16;  break;
			case 6 : INTS = (IMAG / 64)  - 16;  break;
			case 5 : INTS = (IMAG / 32)  - 16;  break;
			case 4 : INTS = (IMAG / 16)  - 16;  break;
			case 3 : INTS = (IMAG / 8)   - 16;  break;
			case 2 : INTS = (IMAG / 4)   - 16;  break;
			case 1 : INTS = (IMAG / 2)   - 16;  break;
			case 0 : INTS =  IMAG / 2;	    break;
		}
	     OVAL = (IS << 7) | (SEGMENT << 4) | INTS;
	     SP   = OVAL ^ 213;
	    }
	else					/* Cas de la loi mu  */
	    {
	     if (IMAG > 8158)		IMAG = 8191;
	     else			IMAG += 33;

	     if (IMAG < 64)		SEGMENT = 0;
	     else if (IMAG < 128)	SEGMENT = 1;
	     else if (IMAG < 256)	SEGMENT = 2;
	     else if (IMAG < 512)	SEGMENT = 3;
	     else if (IMAG < 1024)	SEGMENT = 4;
	     else if (IMAG < 2048)	SEGMENT = 5;
	     else if (IMAG < 4096)	SEGMENT = 6;
	     else			SEGMENT = 7;

	     switch (SEGMENT)
		{
			case 7 : INTS = (IMAG / 256) - 16;  break;
			case 6 : INTS = (IMAG / 128) - 16;  break;
			case 5 : INTS = (IMAG / 64)  - 16;  break;
			case 4 : INTS = (IMAG / 32)  - 16;  break;
			case 3 : INTS = (IMAG / 16)  - 16;  break;
			case 2 : INTS = (IMAG / 8)   - 16;  break;
			case 1 : INTS = (IMAG / 4)   - 16;  break;
			case 0 : INTS = (IMAG / 2)   - 16;  break;
		}
	     OVAL = (IS << 7) | (SEGMENT << 4) | INTS;
	     SP   = OVAL ^ 255;
	    }
	return SP;
    }

/*      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%     Fonction de recodage de l'echantillon MIC de sortie  	%%%
		%%%     dans le decodeur pour le codage synchrone en cascade	%%%
		%%%	     ( Utilise dans le decodeur uniquement )		%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */

int SYNC(int I, int SP,int  DLNX,int  DSX,int  LAW,int  DEBIT)
    {
	int ID, IM, IS, SD, SIGNAL;

	switch (DEBIT)
	    {
		case 40 :				/* Tables de transposition pour un debit de 40 kbits/s */
				IS = I >> 4;
				IM = (IS) ? I & 15 : I + 16;
				switch (DSX)
	    			    {
					case 0  :
							if (DLNX < 68)			ID = 18;
							else if (DLNX < 139)		ID = 19;
							else if (DLNX < 198)		ID = 20;
							else if (DLNX < 250)		ID = 21;
							else if (DLNX < 298)		ID = 22;
							else if (DLNX < 339)		ID = 23;
							else if (DLNX < 378)		ID = 24;
							else if (DLNX < 413)		ID = 25;
							else if (DLNX < 445)		ID = 26;
							else if (DLNX < 475)		ID = 27;
							else if (DLNX < 502)		ID = 28;
							else if (DLNX < 528)		ID = 29;
							else if (DLNX < 553)		ID = 30;
							else if (DLNX < 2048)		ID = 31;
							else if (DLNX < 3974)		ID = 15;
							else if (DLNX < 4080)		ID = 17;
							else				ID = 18;
							break;
					default :
							if (DLNX < 68)			ID = 13;
							else if (DLNX < 139)		ID = 12;
							else if (DLNX < 198)		ID = 11;
							else if (DLNX < 250)		ID = 10;
							else if (DLNX < 298)		ID = 9;
							else if (DLNX < 339)		ID = 8;
							else if (DLNX < 378)		ID = 7;
							else if (DLNX < 413)		ID = 6;
							else if (DLNX < 445)		ID = 5;
							else if (DLNX < 475)		ID = 4;
							else if (DLNX < 502)		ID = 3;
							else if (DLNX < 528)		ID = 2;
							else if (DLNX < 553)		ID = 1;
							else if (DLNX < 2048)		ID = 0;
							else if (DLNX < 3974)		ID = 15;
							else if (DLNX < 4080)		ID = 14;
							else				ID = 13;
							break;
				    }
				break;
		case 32 :				/* Tables de transposition pour un debit de 32 kbits/s */
				IS = I >> 3;
				IM = (IS) ? I & 7 : I + 8;
				switch (DSX)
	    			    {
					case 0  :
							if (DLNX < 80)			ID = 9;
							else if (DLNX < 178)		ID = 10;
							else if (DLNX < 246)		ID = 11;
							else if (DLNX < 300)		ID = 12;
							else if (DLNX < 349)		ID = 13;
							else if (DLNX < 400)		ID = 14;
							else if (DLNX < 2048)		ID = 15;
							else if (DLNX < 3972)		ID = 7;
							else				ID = 9;
							break;
					default :
							if (DLNX < 80)			ID = 6;
							else if (DLNX < 178)		ID = 5;
							else if (DLNX < 246)		ID = 4;
							else if (DLNX < 300)		ID = 3;
							else if (DLNX < 349)		ID = 2;
							else if (DLNX < 400)		ID = 1;
							else if (DLNX < 2048)		ID = 0;
							else if (DLNX < 3972)		ID = 7;
							else				ID = 6;
							break;
				    }
				break;
		case 24 :				/* Tables de transposition pour un debit de 24 kbits/s */
				IS = I >> 2;
				IM = (IS) ? I & 3 : I + 4;
				switch (DSX)
	    			    {
					case 0  :
							if (DLNX < 8)			ID = 3;
							else if (DLNX < 218)		ID = 5;
							else if (DLNX < 331)		ID = 6;
							else if (DLNX < 2048)		ID = 7;
							else				ID = 3;
							break;
					default :
							if (DLNX < 8)			ID = 3;
							else if (DLNX < 218)		ID = 2;
							else if (DLNX < 331)		ID = 1;
							else if (DLNX < 2048)		ID = 0;
							else				ID = 3;
							break;
				    }
				break;
		default :				/* Tables de transposition pour un debit de 16 kbits/s */
				IS = I >> 1;
				IM = (IS) ? I & 1 : I + 2;
				switch (DSX)
	    			    {
					case 0  :
							if (DLNX < 261)			ID = 2;
							else if (DLNX < 2048)		ID = 3;
							else				ID = 2;
							break;
					default :

							if (DLNX < 261)			ID = 1;
							else if (DLNX < 2048)		ID = 0;
							else				ID = 1;
							break;
				    }
				break;
	    }

	if (ID == IM)
	    SD = SP;
	else if (ID < IM)				/* Cas de l'echantillon superieur */
	    {
	     if (LAW == LAW_A)					/* cas de la loi A */
		{
		 SIGNAL = SP ^ 85;			/* Inversion des bits pairs */

		 if (SIGNAL == 0)		SIGNAL = 128;
		 else if (SIGNAL < 128)		SIGNAL--;
		 else if (SIGNAL < 255)		SIGNAL++;

		 SD = SIGNAL ^ 85;
		}
	     else					/* Cas de la loi mu */
		{
		 if (SP < 127)			SD = SP + 1;
		 else if (SP == 127)		SD = 254;
		 else if (SP == 128)		SD = SP;
		 else				SD = SP - 1;
		}
	    }
	else						/* Cas de l'echantillon inferieur */
	    {
	     if (LAW == LAW_A)					/* Cas de la loi A */
		{
		 SIGNAL = SP ^ 85;			/* Inversion des bits pairs */

		 if (SIGNAL < 127)		SIGNAL++;
		 else if (SIGNAL == 128)	SIGNAL = 0;
		 else if (SIGNAL > 128)		SIGNAL--;

		 SD = SIGNAL ^ 85;		
		}
	     else					/* Cas de la loi mu */
		{
		 if (SP == 0)			SD = 0;
		 else if (SP < 128)		SD = SP - 1;
		 else if (SP < 255)		SD = SP + 1;
		 else				SD = 126;
		}
	    }
	return SD;
    }

