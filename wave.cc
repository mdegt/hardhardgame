/* wave.cpp

	Copyright (c) 1996, 1988 by Timothy J. Weber.

	See WAVE.txt for documentation.
*/

#ifndef _MSC_VER
#include <stdlib.h>
#else
// Microsoft doesn't include min, though it's part of the standard library!
template<class T> T min(T a, T b) { return a < b? a: b; }
#endif
extern "C"
{

#include <stdlib.h>
/* @BA@OXO@CROXOC-4249@191009@degtoun1@ */
//#include <linux/kernel.h>
//#include <asm/byteorder.h>
}
/* @EA@OXO@CROXOC-4249@191009@degtoun1@ */
#include <string.h> /* Avoid compiler error for strcpy */
#include "wave.h"
#include "g726.h"
#include "global.h"
#include "proto.h"
#include "extern.h"
// eba_ : Begin Add
#include <sys/file.h>
#include <errno.h>
using namespace std;

/***************************************************************************
	macros and constants
***************************************************************************/

// constants for the canonical WAVE format
const int fmtChunkLength = 16+2;  // length of fmt contents +2 for Extended fmt
const int waveHeaderLength = 20 + fmtChunkLength + 8;  // from "WAVE" to sample data
                 // "RIFF" size "WAVE" "FMT" size // fmt data // "DATA" size
// define REVERSE_ENDIANISM if the endianism of the host platform is not Intel
// (Intel is little-endian)

#ifdef REVERSE_ENDIANISM
  #define SWAP_32(int32) (  \
   (((int32) & 0x000000FFL) << 24) +  \
   (((int32) & 0x0000FF00L) << 8) +  \
   (((int32) & 0x00FF0000L) >> 8) +  \
   (((int32) & 0xFF000000L) >> 24))
 
  #define SWAP_16(int16) (  \
   (((int16) & 0x00FF) << 8) +  \
   (((int16) & 0xFF00) >> 8))
#endif
 
/***************************************************************************
	typedefs and class definitions
***************************************************************************/

/***************************************************************************
	prototypes for static functions
***************************************************************************/
/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
char destination[17];
extern char * get_api_operation(char *sender);
extern char *trim(char *str, const char *seps);
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
unsigned int formatType1, numChannels1, sampleRate1, bytesPerSecond1, bytesPerSample1, bitsPerChannel1;
/***************************************************************************
static variables
***************************************************************************/
static 	string ValueExtraType[numExtraTypes];
/***************************************************************************
	public member functions for WaveFile
***************************************************************************/

WaveFile::WaveFile():
	readFile(0),
	writeFile(0),
	CCittFile(0),
	formatType(0),
	numChannels(0),
	sampleRate(0),
	bytesPerSecond(0),
	bytesPerSample(0),
	bitsPerChannel(0),
	dataLength(0),
	ExtraLength(0),
	ExtFmtLength(0),
	ExtFmt(0),
	error(0),
	changed(true)
{
}

WaveFile::~WaveFile()
{
	Close();
}

/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
bool WaveFile::OpenRead(const char* name, char* sender, char * str_entity)
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */
{
   unsigned long greetings_storage_amount = 0;
   unsigned int filesize = 0;
   unsigned int max_greetings_size = 0;
   unsigned int wave_duration = 0;


	if (readFile) {
		delete readFile;  // closes the file before it's destroyed
		readFile = 0;
	}

	try {
		// open the RIFF file
		print_debug("Open the RIFF file");
		readFile = new RiffFile(name, sender);

		if (!readFile->filep())
			throw error = "Couldn't open file";

		// read the header information
		print_debug( "Read header" );
		if (strcmp(readFile->chunkName(), "RIFF")
			|| strcmp(readFile->subType(), "WAVE")
			|| !readFile->push("fmt "))
			throw error = "Couldn't find RIFF, WAVE, or fmt";

		size_t dwFmtSize = size_t(readFile->chunkSize());
		char* fmtChunk = new char[dwFmtSize];
		try {
			if (fread(fmtChunk, dwFmtSize, 1, readFile->filep()) != 1)
				throw error = "Error reading format chunk";
			readFile->pop();

//print_debug("In OpenRead -1: formatType=%d, numChannels=%d, sampleRate =%lu, bytesPerSample =%d,  bytesPerSecond=%lu, bitsPerChannel = %d  dwFmtSize=%lu",formatType, numChannels, sampleRate, bytesPerSecond, bytesPerSample, bitsPerChannel, dwFmtSize);
/*
         // set the format attribute members                         Microsoft notation
        	formatType     = __le16_to_cpu(*((short*) fmtChunk));			// wFormatTag
			numChannels    = __le16_to_cpu(*((short*) (fmtChunk + 2)));   // nChannels
			sampleRate     = __le32_to_cpu(*((long*) (fmtChunk + 4)));	   // nSamplesPerSec
			bytesPerSecond = __le32_to_cpu(*((long*) (fmtChunk + 8)));	   // nAvgBytesPerSec
			bytesPerSample = __le16_to_cpu(*((short*) (fmtChunk + 12)));  // nBlockAligne
			bitsPerChannel = __le16_to_cpu(*((short*) (fmtChunk + 14)));  // wBitsPerSample
*/
/* @BD@OXO@CROXOC-6592@191009@degtoun1@ */
 #ifdef REVERSE_ENDIANISM
 			formatType = SWAP_16(*((short*) fmtChunk));			 // wFormatTag
			numChannels = SWAP_16(*((short*) (fmtChunk + 2)));	 // nChannels
 			sampleRate = SWAP_32(*((long*) (fmtChunk + 4)));		 // nSamplesPerSec
 			bytesPerSecond = SWAP_32(*((long*) (fmtChunk + 8)));	 // nAvgBytesPerSec
 			bytesPerSample = SWAP_16(*((short*) (fmtChunk + 12)));// nBlockAligne
 			bitsPerChannel = SWAP_16(*((short*) (fmtChunk + 14)));// wBitsPerSample
 #else
 			formatType = *((short*) fmtChunk);			 // wFormatTag
 			numChannels = *((short*) (fmtChunk + 2));	 // nChannels
 			sampleRate = *((long*) (fmtChunk + 4));		 // nSamplesPerSec
 			bytesPerSecond = *((long*) (fmtChunk + 8));	 // nAvgBytesPerSec
 			bytesPerSample = *((short*) (fmtChunk + 12));// nBlockAligne
 			bitsPerChannel = *((short*) (fmtChunk + 14));// wBitsPerSample
 #endif
//print_debug("In OpenRead -2: ENDIAN CONV: formatType=%d, numChannels=%d, sampleRate =%ld, bytesPerSecond=%ld, bytesPerSample =%d, bitsPerChannel = %d dwFmtSize=%lu",formatType, numChannels, sampleRate, bytesPerSecond, bytesPerSample, bitsPerChannel, dwFmtSize);
//formatType1= formatType, numChannels1=numChannels, sampleRate1=sampleRate, bytesPerSecond1=bytesPerSecond, bytesPerSample1=bytesPerSample, bitsPerChannel1=bitsPerChannel;
/* @ED@OXO@CROXOC-6592@191009@degtoun1@ */
			// eba_
			// Check format
         // eba_
			// Ext fmt management
			// Ext fmt = <size ext fmt> [<data ext fmt>]
			//
			if (dwFmtSize>16)	// Extended fmt
#ifdef REVERSE_ENDIANISM
            ExtFmtLength = SWAP_16(*((short*) (fmtChunk + 16)));
#endif
         if (ExtFmtLength)   // Extended Fmt not empty
			{
				size_t dwExtFmtSize = ExtFmtLength;
				ExtFmt = new char [dwExtFmtSize];
				for (int i=0; i< ExtFmtLength;i++)
					ExtFmt[i] = fmtChunk[i+18];
			}

			// position at the data chunk
			if (!readFile->push("data"))
				throw error = "Couldn't find data chunk";

			// get the size of the data chunk
			dataLength = readFile->chunkSize();
			readFile->pop();
         print_debug( "OpenRead: Wave file dataLength=%lu", dataLength);

// Check size
/* @BA@OXO@CROXOC-6563@191120@degtoun1@ */

//Check first if file exits
//rechercher la taille du fichier
//dans le call_handling
//git clone https://mdegt@bitbucket.org/mdegt/hardhardgame.git
//
/https://mdegt@bitbucket.org/mdegt/hardhardgame.git/
	 filesize= dataLength+46;
         wave_duration = filesize*8/(sampleRate * numChannels *bitsPerChannel);
         print_debug("filesize=%d, wave_duration=%d", filesize, wave_duration);

         if(strcmp(destination, "moh")==0)
         {
            wave_duration = (dataLength+46)*8/(sampleRate * numChannels *bitsPerChannel);

            if(wave_duration > nb_moh_capacity*60)
            {
               global_wav_size_send = filesize;

               if(strcmp(sender, "api")==0)
                  print_api_error( API_ERR_POST, API_ERR_POST_WAV_SIZE, sender );
               else
                  print_error( ERR_POST, ERR_POST_WAV_SIZE, sender );
            }
         }
         else if(strcmp(destination, "greetings")==0)
         {
            char result[256] = {0};

            print_debug("CheckSize: Reading the system greetings storage amount");
            FILE *fp = popen("/usr/bin/wdt restapi_audio_files_mgt readGreetingsSize","r");

            if(fp != NULL)
            {
               if(fgets(result, sizeof(result), fp) != NULL)
               {
                  char* token;
                  char * s = ":";

                  print_debug( "result=%s", result);
                  token=strstr(result,(char*) s);
                  if(token != NULL)
                  {
                     strcpy(result,token+1);
                     greetings_storage_amount=atol(result);
                  }
                  else
                     print_debug( "token=NULL");
               }
               pclose(fp);
            }
            else
               print_debug( "Error -> popen");

            wave_duration = (dataLength+46)*8/(sampleRate * numChannels *bitsPerChannel);
            max_greetings_size = PCM_SIZE_PER_MINUTE * (nb_prea_capacity/60); //320s max for all greetings=5mn

            if((wave_duration > nb_prea_capacity) || ((unsigned int)greetings_storage_amount >  max_greetings_size))
            {
               print_debug("Greetings wave_duration(%d) > %d", wave_duration, nb_prea_capacity);

               global_wav_size_send = filesize;

               if(strcmp(sender, "api")==0)
                  print_api_error( API_ERR_POST, API_ERR_POST_WAV_SIZE, sender );
               else
                  print_error( ERR_POST, ERR_POST_WAV_SIZE, sender );
            }
         }
/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
         else if(strcmp(destination, "aa")==0 ||
            strcmp(destination, "audiotexts")==0 ||
            strcmp(destination, "mailboxes")==0 )
            //AA & AT have same constraints : num=1 & num=2 are limited to 120s, num=3 is limited to 20s.
            //For mailboxes there are only num 1 & 2
         {
            int num = atoi(str_entity);
            unsigned int welcome_capacity = nb_aa_welcome_capacity = nb_at_welcome_capacity = nb_gal_audio_capacity = 120;
            unsigned int goodbye_capacity = nb_aa_goodbye_capacity = nb_at_goodbye_capacity = 20;

            wave_duration = (dataLength+46)*8/(sampleRate * numChannels *bitsPerChannel);

            print_debug("CheckSize: str_entity=%s - entity_num=%d - duration=%d", str_entity, num, wave_duration);

            if(num==1 || num ==2)
            {
               if(wave_duration > welcome_capacity)
               {
                  global_wav_size_send = filesize;

                  if(strcmp(sender, "api")==0)
                     print_api_error(API_ERR_POST, API_ERR_POST_WAV_SIZE, sender);
                  else
                     print_error(ERR_POST, ERR_POST_WAV_SIZE, sender);
               }
            }
            else if(num ==3)
            {
               if(wave_duration >  goodbye_capacity)
               {
                  global_wav_size_send = filesize;

                  if(strcmp(sender, "api")==0)
                     print_api_error(API_ERR_POST, API_ERR_POST_WAV_SIZE, sender);
                  else
                     print_error(ERR_POST, ERR_POST_WAV_SIZE, sender);
               }
            }
            else
            {
               if(strcmp(sender, "api")==0)
                  print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
               else
                  print_error(ERR_POST, ERR_POST_ENTITY_NUM, sender);
            }
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */
         }
         else
         {
               if(strcmp(sender, "api")==0)
                  print_api_error(API_ERR_POST, API_ERR_POST_UNKNOWN_FEATURE, sender );
               else
                  print_error(ERR_POST, ERR_POST_UNKNOWN_FEATURE, sender );
         }
// Check format
         if(strcmp(destination, "moh")==0)
         {
            if( !(numChannels == 1 && sampleRate == 8000 && bitsPerChannel == 8)
               && !(formatType == WAVE_FORMAT_G726_ADPCM && numChannels == 1 && sampleRate == 8000 && bitsPerChannel == 4) )
            {
               global_channels = numChannels;
               global_SamplesPerSec = sampleRate;
               global_BitsPerSample = bitsPerChannel;

               if(strcmp(sender, "api")==0)
               {
                  print_debug( "api" );
                  print_api_error(API_ERR_POST, API_ERR_POST_FORMAT, sender);
               }
               else
               {
                  print_debug( "moh" );
                  print_error(ERR_POST, ERR_POST_FORMAT, sender);
               }
            }
         }
/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
         else if(strcmp(destination, "greetings")==0 ||
            strcmp(destination, "aa")==0 ||
            strcmp(destination, "audiotexts")==0 ||
            strcmp(destination, "mailboxes")==0)
/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
         {
            //print_debug( "Check greetings file format" );
            if ( (numChannels > 1) || ( !(formatType == WAVE_FORMAT_PCM)  && !(formatType == WAVE_FORMAT_MULAW)  && !(formatType == WAVE_FORMAT_ALAW)) )
            {
               global_channels = numChannels;
               global_SamplesPerSec = sampleRate;
               global_BitsPerSample = bitsPerChannel;
               print_api_error(API_ERR_POST, API_ERR_POST_FORMAT, sender);
            }
         }
/* @EA@OXO@CROXOC-6563@191120@degtoun1@ */
         // Se for LIST/INFO  chunk
			/* // eba_
			if (readFile->push("LIST")) {
				do {
					if (strcmp(readFile->subType(),"INFO")==0) {
						ExtraLength = readFile->chunkSize();

						break;
					}
				} while (readFile->push());
			}
			readFile->pop();

			// copy actual info value
			string type,value;
			if (GetFirstExtraItem(type, value)) {
				while (GetNextExtraItem(type, value));
			}

			for (int i = 0; i < numExtraTypes; i++) {
				if (readFile->GetExtraItemOfType(extraTypes[i].typeName,value))
					ValueExtraType[i]=value;
			}
			*/ // eba_

			delete[] fmtChunk;
		} catch (...) {
			delete[] fmtChunk;
			throw error;
		}
	} catch (...) {
		Close();
		return false;
	}
	return true;
}

bool WaveFile::OpenWrite(const char* name)
{
	// eba_ : Begin Add
//	int writeFileFD;	// File descriptor
	// eba_ : End   Add
	if (writeFile) {
		print_debug( "An output file is already open" );
		// write the header information at the start of the file, if necessary
		if (changed) {
         print_debug( "yes changed" );

			long currentSpot = ftell(writeFile);  // save the position
			WriteHeaderToFile(writeFile);
			fseek(writeFile, currentSpot, SEEK_SET);  // restore the old position
				// this is necessary so the file gets the right length--otherwise,
				// all the data we wrote would be truncated.
		}

		// close the file
		fclose(writeFile);
		writeFile = 0;
	}

	// open the file
	print_debug( "Open the output file %s", name );
	writeFile = fopen(name, "wb");
	//writeFile = _fsopen(name, "wb", _SH_DENYRW);		// _eba
	if (!writeFile) {
		error = "Couldn't open output file";
		return false;
	}
	// _eba : Begin Add
	// Apply the exclusive lock
/*
	writeFileFD = fileno( writeFile );
	if( flock( writeFileFD, LOCK_EX | LOCK_NB ) == -1 )
	{	// Error on lock the file
		fclose( writeFile );
		if( errno == EWOULDBLOCK )
			print_error( ERR_IO, ERR_FILE_LOCKED );
		else
			print_error( ERR_IO, ERR_FILE_ON_LOCK );
	}
*/
	// _eba : End   Add


	// write the header
	return WriteHeaderToFile(writeFile);
}


bool WaveFile::OpenCCitt(const char* name,bool ReadOnly)
{
	if (CCittFile){
		// close the file
		fclose(CCittFile);
		CCittFile = 0;
	}


	// open the file
	if (ReadOnly)
		CCittFile = fopen(name, "rb");
	else
		CCittFile = fopen(name, "wb");

	if (!CCittFile) {
		error = "Couldn't open output file";
		return false;
	}
	return true;
}
bool WaveFile::ResetToStart()
{
	if (readFile) {
		// pop out of the data chunk
		if (!readFile->rewind()	|| !readFile->push("data"))
		{
			error = "Couldn't find data chunk on reset";
			return false;
		} else
			return true;
	} else if (writeFile) {
		return fseek(writeFile, waveHeaderLength, SEEK_SET) == 0;
	} else
		return false;
}

bool WaveFile::Close()
{
	bool retval = true;
	// eba_ : Begin Add
//	int writeFileFD;	// File descriptor
	// eba_ : End   Add

	if (readFile) {
		delete readFile;  // closes the file before it's destroyed
		readFile = 0;
	} else if (writeFile) {
		// write the header information at the start of the file, if necessary
		if (changed) {
			long currentSpot = ftell(writeFile);  // save the position
			retval = WriteHeaderToFile(writeFile);
			fseek(writeFile, currentSpot, SEEK_SET);  // restore the old position
				// this is necessary so the file gets the right length--otherwise,
				// all the data we wrote would be truncated.
		}

		// close the file
		// _eba : Begin Add
		// Suppress the exclusive lock
/*
		writeFileFD = fileno( writeFile );
		if( flock( writeFileFD, LOCK_UN ) == -1 )
		{	// Error on unlock the file
			print_debug( "Error while unlocking the file" );
		}
*/
		// _eba : End   Add
		fclose(writeFile);
		writeFile = 0;
	} else if (CCittFile) {
		// close the file
		fclose(CCittFile);
		CCittFile = 0;
	}

	return retval;
}

bool WaveFile::FormatMatches(const WaveFile& other)
{
	return formatType == other.formatType
		&& numChannels == other.numChannels
		&& sampleRate == other.sampleRate
		&& bytesPerSecond == other.bytesPerSecond
		&& bytesPerSample == other.bytesPerSample
		&& bitsPerChannel == other.bitsPerChannel;
}

void WaveFile::CopyFormatFrom(const WaveFile& other)
{

//   print_debug("1- In CopyFormatFrom: Incomming info: formatType=%d, numChannels=%d, sampleRate =%ld, bytesPerSecond=%ld, bytesPerSample =%d, bitsPerChannel = %d",other.formatType, other.numChannels, other.sampleRate, other.bytesPerSecond, other.bytesPerSample, other.bitsPerChannel);
   formatType = other.formatType;
   numChannels = other.numChannels;
   sampleRate = other.sampleRate;
   bytesPerSecond = other.bytesPerSecond;
   bytesPerSample = other.bytesPerSample;
   bitsPerChannel = other.bitsPerChannel;
}

bool WaveFile::GetFirstExtraItem(string& type, string& value)
{
	if (readFile && readFile->rewind())
		// Find first LIST chunk
		return readFile->push("LIST") && readFile->getNextExtraItem(type, value);
	else
		return false;
}

bool WaveFile::GetNextExtraItem(string& type, string& value)
{
	if (readFile)
		return readFile->getNextExtraItem(type, value);
	else
		return false;
}

bool WaveFile::CopyFrom(WaveFile& other)
{
	const size_t transferBufSize = 4096;

	if (!writeFile) {
		error = "Copy to an unopened file";
		return false;
	} else if (!other.readFile) {
		error = "Copy from an unopened file";
		return false;
	}

	try {
		// allocate the transfer buffer
		char* transferBuffer = new char[transferBufSize];
		unsigned long bytesRead = 0;

		try {
			/* // eba_
			if (!other.ResetToStart())
				throw error = "Couldn't reset input file to start";
			*/ // eba_

			while (bytesRead < other.dataLength) {
				// calculate the size of the next buffer
				size_t bytesToRead = (size_t) min(transferBufSize,
					size_t(other.dataLength - bytesRead));

				// read the buffer
				if (fread(transferBuffer, 1, bytesToRead, other.readFile->filep())
					!= bytesToRead)
					throw error = "Error reading samples from input file";
				bytesRead += bytesToRead;

				// write the buffer
				if (fwrite(transferBuffer, 1, bytesToRead, writeFile) != bytesToRead)
					throw error = "Error writing samples to output file";
				dataLength += bytesToRead;
				changed = true;
			}

			// delete the transfer buffer
			delete[] transferBuffer;
		} catch (...) {
			delete[] transferBuffer;
			throw error;
		}
	} catch (...) {
		return false;
	}

	return true;
}

bool WaveFile::WriteHeaderToFile(FILE* fp)
{
   /* @BA@OXO@CROXOC-6592@191009@degtoun1@ */

   unsigned short _formatType;
   unsigned short _numChannels;
	unsigned long  _sampleRate;
	unsigned long  _bytesPerSecond;
	unsigned short _bytesPerSample;
	unsigned short _bitsPerChannel;
   unsigned short _ExtFmtLength;
	unsigned long  _dataLength;
   unsigned long _wholeLength;
   unsigned long _chunkLength;
   /* @EA@OXO@CROXOC-6592@191009@degtoun1@ */

//print_debug("In WriteHeaderToFile 1: Incomming info: formatType=%d, numChannels=%d, sampleRate =%ld, bytesPerSecond=%ld, bytesPerSample =%d, bitsPerChannel = %d",formatType, numChannels, sampleRate, bytesPerSecond, bytesPerSample, bitsPerChannel);

// seek to the start of the file
	if (fseek(fp, 0, SEEK_SET) != 0)
		return false;

	// write the file header
/* @BD@OXO@CROXOC-6592@191009@degtoun1@ */

 	unsigned long wholeLength = (waveHeaderLength-8) + ExtFmtLength + dataLength + ExtraLength;
   unsigned long chunkLength = fmtChunkLength;

/* @ED@OXO@CROXOC-6592@191009@degtoun1@ */

//   unsigned long _wholeLength = __cpu_to_le32((waveHeaderLength-8) + ExtFmtLength + dataLength + ExtraLength);
//	unsigned long _chunkLength = __cpu_to_le32(fmtChunkLength);

//print_debug("WriteHeaderToFile: calcul intermediiare et conversion: wholeLength=%ld != _wholeLength=%ld, chunkLength=%ld != _chunkLength=%ld",((waveHeaderLength-8) + ExtFmtLength + dataLength + ExtraLength),_wholeLength, fmtChunkLength, _chunkLength);

/* @BD@OXO@CROXOC-6592@191009@degtoun1@ */
#ifdef REVERSE_ENDIANISM
 		_wholeLength = SWAP_32(wholeLength);
 		_chunkLength = SWAP_32(chunkLength);
 		_formatType = SWAP_16(formatType);
 		_numChannels = SWAP_16(numChannels);
 		_sampleRate = SWAP_32(sampleRate);
 		_bytesPerSecond = SWAP_32(bytesPerSecond);
 		_bytesPerSample = SWAP_16(bytesPerSample);
 		_bitsPerChannel = SWAP_16(bitsPerChannel);
 		_ExtFmtLength = SWAP_16(ExtFmtLength);
 		_dataLength = SWAP_32(dataLength);
 #endif
//   print_debug("WriteHeaderToFile 2: conversion before writing to file: formatType=%d, numChannels=%d, sampleRate =%lu, bytesPerSecond=%lu, bytesPerSample =%d, bitsPerChannel = %d dataLength=%lu",_formatType, _numChannels, _sampleRate, _bytesPerSecond, _bytesPerSample, _bitsPerChannel, _dataLength);

	if (fputs("RIFF", fp) == EOF
/* @BD@OXO@CROXOC-6592@191014@degtoun1@ */
/* 		|| fwrite(&wholeLength, sizeof(wholeLength), 1, fp) != 1*/
/* @ED@OXO@CROXOC-6592@191014@degtoun1@ */
/* @BA@OXO@CROXOC-6592@191014@degtoun1@ */
		|| fwrite(&_wholeLength, sizeof(_wholeLength), 1, fp) != 1
/* @EA@OXO@CROXOC-6592@191014@degtoun1@ */
		|| fputs("WAVE", fp) == EOF
		|| fputs("fmt ", fp) == EOF
/* @BA@OXO@CROXOC-6592@191009@degtoun1@ */
      || fwrite(&_chunkLength, sizeof(_chunkLength), 1, fp) != 1
		|| fwrite(&_formatType,       sizeof(_formatType), 1, fp) != 1
		|| fwrite(&_numChannels,      sizeof(_numChannels), 1, fp) != 1
		|| fwrite(&_sampleRate,       sizeof(_sampleRate), 1, fp) != 1
		|| fwrite(&_bytesPerSecond,   sizeof(_bytesPerSecond), 1, fp) != 1
		|| fwrite(&_bytesPerSample,   sizeof(_bytesPerSample), 1, fp) != 1
		|| fwrite(&_bitsPerChannel,   sizeof(_bitsPerChannel), 1, fp) != 1
		|| fwrite(&_ExtFmtLength,     sizeof(_ExtFmtLength), 1, fp) != 1)
/* @EA@OXO@CROXOC-6592@191009@degtoun1@ */
	{
		error = "Error writing header";
		return false;
	}
/* @BA@OXO@CROXOC-6592@191014@degtoun1@ */
	if (ExtFmtLength) {
/* @EA@OXO@CROXOC-6592@191014@degtoun1@ */
/* @BD@OXO@CROXOC-6592@191014@degtoun1@ */
/*
 *	if (ExtFmtLength) {
 */
/* @ED@OXO@CROXOC-6592@191014@degtoun1@ */
		if (fwrite(&ExtFmt, sizeof(ExtFmt), 1, fp) != 1)
		{
			error = "Error writing Extended fmt header";
			return false;
		}
	}

	if (fputs("data", fp) == EOF
/* @BD@OXO@CROXOC-6592@191009@degtoun1@ */
/* 		|| fwrite(&dataLength, sizeof(dataLength), 1, fp) != 1)*/
/* @ED@OXO@CROXOC-6592@191009@degtoun1@ */
		|| fwrite(&_dataLength, sizeof(_dataLength), 1, fp) != 1)
	{
		error = "Error writing Data header";
		return false;
	}

	// if it's the same file, now we don't have to write it again unless it's
	// been changed.
	if (fp == writeFile)
		changed = false;

	return true;
}

bool WaveFile::WriteExtraItemToFile(void)
{
	int i;		// eba_

	// write the file header
	unsigned long wholeExtraLength = ExtraLength;
	unsigned long chunkLength = fmtChunkLength;

	if (!writeFile) {
		error = "Copy to an unopened file";
		return false;
	}

	// calculate length of chunk LIST/INFO
	ExtraLength = 0;
	for (/*int */i = 0; i < numExtraTypes; i++)			// eba_
		if (!ValueExtraType[i].empty()) {
				ExtraLength += (ValueExtraType[i].length()+8);
				if (ExtraLength&1) ExtraLength++; // Word alignement
		}

	if (!ExtraLength)
		return false;

	ExtraLength += 12; // "List"+<chunk length>+"INF0"
	changed |= (ExtraLength != wholeExtraLength);
	chunkLength = ExtraLength-4;

	if (dataLength&1)  // Word alignement
		fwrite("\0", 1, 1, writeFile);

	if (fputs("LIST", writeFile) == EOF
		|| fwrite(&chunkLength, sizeof(chunkLength), 1, writeFile) != 1
		|| fputs("INFO", writeFile) == EOF)
	{
		error = "Error writing header";
		return false;
	}

	for (i = 0; i < numExtraTypes; i++)
		if (!ValueExtraType[i].empty()) {
			chunkLength=ValueExtraType[i].length()+1; // +1 for \0

			if (fputs(extraTypes[i].typeName, writeFile) == EOF
				|| fwrite(&chunkLength, sizeof(chunkLength), 1, writeFile) != 1
				|| fputs(ValueExtraType[i].c_str(), writeFile) == EOF)
			{
				error = "Error writing Extra item ";
				return false;
			}
			chunkLength=(ValueExtraType[i].length()+1)&1;
			fwrite("\0\0", 1+chunkLength, 1, writeFile);
		}

	return true;
}

#define ADDLEVEL 5  /* Pour passer de 8 bits a 12 bits		    */
		    /* normalement 4 mais trop de perte de niveau   */

void WaveFile::ConvertFromPCM(const char *infilename, const char *outfilename)
{
  char command[256];
  sprintf(command, "ffmpeg -i %s -map_metadata -1 -fflags +bitexact -acodec pcm_alaw -ar 8000 -ac 1 -loglevel panic -y %s", infilename, outfilename);
  print_debug ("Executing command %s", command);
  system(command);
}

void WaveFile::ConvertFromADPCM(const char *infilename, const char *outfilename)
{
   char command[256];
   sprintf(command, "ffmpeg -i %s -map_metadata -1 -fflags +bitexact -acodec g726 -loglevel panic -y %s", infilename, outfilename);
   print_debug ("Executing command %s", command);
   system(command);
}


bool WaveFile::ConvertFrom(WaveFile& other, bool ToADPCM, int law,int rate)
{
	unsigned long i;
	int j;		// eba_
	const size_t transferBufSize = 3*5*256;  // multiple de 2,3,5 pour etre sur d'aligner les samples
	const size_t convertBufSize = 3*5*256;

	unsigned int Sample;

	if (!writeFile) {
		error = "Copy to an unopened file";
		return false;
	} else if (!other.readFile) {
		error = "Copy from an unopened file";
		return false;
	}

	try {
		// allocate the transfer buffer
		unsigned char* transferBuffer = new unsigned char[transferBufSize];
		unsigned char* convertBuffer = new unsigned char[convertBufSize];
		//int nbSamplesPerBlock = (bytesPerSample*8)/bitsPerChannel;
		//char* BitsSample = new char [bytesPerSample*8];

		unsigned long bytesRead = 0;
		unsigned long bytesToWrite;
		unsigned long bytesToRead;
		char ValRead=0;
		char ValWrite=0;

		try {
			/* // eba_
			if (!other.ResetToStart())
				throw error = "Couldn't reset input file to start";
			*/ // eba_


			if (ToADPCM)
				SetHeader(WAVE_FORMAT_G726_ADPCM,rate);
			else
				SetHeader(law,rate);

			// Addapter la loi aux constantes du CODEUR G726
			if (law==WAVE_FORMAT_ALAW) law=LAW_A;
			else if (law==WAVE_FORMAT_MULAW) law=LAW_MU;
			else law = LAW_NONE;

			while (bytesRead < other.dataLength) {
				// calculate the size of the next buffer
				bytesToRead = (size_t) min(transferBufSize, size_t(other.dataLength - bytesRead));
				bytesRead += bytesToRead;

				if (bytesToRead != transferBufSize) { // il reste moins d'un buffer
													  // on aligne sur un nombre multiple
					                                  // d'echantillons quitte a perdre les
													  // derniers.

					bytesToRead = (bytesToRead / bytesPerSample) * bytesPerSample;
				}

				// read the buffer
				if (fread(transferBuffer, 1, bytesToRead, other.readFile->filep())
					!= bytesToRead)
					throw error = "Error reading samples from input file";


				// CALL ADPCM TRANSLATOR

				bytesToWrite = 0;
				if (ToADPCM) {
					// CODEUR(ENTREE, LOI, DEBIT, RAZ)
					// convertBuffer[0] = CodeurG726 (transferBuffer[0],
					//					law,
					//					rate,
					//					bytesRead?0:1);

					ValWrite=0;
					for (/*int */i =0,j=0;i< bytesToRead;i++) {		// eba_

						// on calcule les N echantillons compresses qui forment un Sample
/* RA PCM
						 *
						 * Modif pour convertir directement depuis le PCM
						 *

						ValRead = CodeurG726 (transferBuffer[i],
										law,
										rate,
										false);
*/
						Sample = unsigned (transferBuffer[i]);
						if (law==LAW_NONE)
							Sample = Sample << ADDLEVEL;



						ValRead = CodeurG726 (Sample,
										law,
										rate,
										false);
/* RA PCM END */

						int k=bitsPerChannel;
						if (k==8) {
							convertBuffer[bytesToWrite++]=ValRead;
						}
						else {
							while (k++<8)
								ValRead <<=1;

							for (k=0;k<bitsPerChannel;k++) {    // On recompose bit a bit
								if (ValRead&0x80)
									ValWrite |= 1;				// chaque echantillon
								ValRead <<=1;
								if (++j==8) {                   // lorsqu'un octet est complet
									convertBuffer[bytesToWrite++]=ValWrite;
									j=0;
									ValWrite = 0;
								}
								ValWrite*=2;
							}
						}
					}
				}
				else {
					//convertBuffer[0] = DeCodeurG726 (transferBuffer[0],
					//					law,
					//					DEBIT_32KBS,
					//					bytesRead?0:1);

					int FrombitsPerChannel =  other.GetBitsPerChannel();

					for (/*int */i =0,j=0;i< bytesToRead;i++) {		// eba_
						ValRead =transferBuffer[i];
						if (FrombitsPerChannel==8) {
							convertBuffer[bytesToWrite++]=DeCodeurG726 (ValRead,law,rate,false);;
/* RA PCM
						 *
						 * Modif pour convertir directement depuis le PCM
						 *

							convertBuffer[bytesToWrite++]=DeCodeurG726 (ValRead,law,rate,false);;
*/
							Sample = 	DeCodeurG726 (ValRead,law,rate,false);
							convertBuffer[bytesToWrite++]=
								law==LAW_NONE ? Sample >> ADDLEVEL : Sample;
/* RA PCM END */
						}
						else {
							for (int k=0;k<8;k++) {                 // On recompose bit a bit
								if (ValRead&0x80) ValWrite |= 1;	// chaque echantillon
								ValRead <<=1;
								if (++j==FrombitsPerChannel) {
									convertBuffer[bytesToWrite++]
										= DeCodeurG726 (ValWrite,law,rate,false);
/* RA PCM
									convertBuffer[bytesToWrite++]
										= DeCodeurG726 (ValWrite,law,rate,false);
*/
									Sample = DeCodeurG726 (ValWrite,law,rate,false);
									if (law==LAW_NONE)
										 Sample = Sample >> ADDLEVEL;

									convertBuffer[bytesToWrite++] = Sample;
/* RA PCM END */
									j=0;
									ValWrite = 0;
									if (bytesToWrite >= convertBufSize) {
										if (fwrite(convertBuffer, 1, bytesToWrite, writeFile) != bytesToWrite)
											throw error = "Error writing samples to output file";
										dataLength += bytesToWrite;
										changed = true;
										bytesToWrite=0;
									}
								}
								ValWrite<<=1;
							}
						}
					}
				}
				// write the buffer
				if (bytesToWrite) {
					if (fwrite(convertBuffer, 1, bytesToWrite, writeFile) != bytesToWrite)
						throw error = "Error writing samples to output file";
					dataLength += bytesToWrite;
					changed = true;
				}
			}
			// delete the transfer buffer
			delete[] transferBuffer;
			delete[] convertBuffer;
		} catch (...) {
			delete[] transferBuffer;
			delete[] convertBuffer;
			throw error;
		}
	} catch (...) {
		return false;
	}

	return true;
}

bool WaveFile::SetHeader(int fmt,int rate)
{
		SetFormatType(fmt);
		SetSampleRate(8000);
		SetNumChannels(1);
		SetBytesPerSample(1);

		if (fmt == WAVE_FORMAT_G726_ADPCM) {
				if (rate ==DEBIT_32KBS) {
					SetBytesPerSecond(4000);
					SetBitsPerChannel(4);
				}
				else if (rate ==DEBIT_40KBS) {
					SetBytesPerSecond(5000);
					SetBitsPerChannel(5);
					SetBytesPerSample(5);
				}
				else if (rate ==DEBIT_24KBS) {
					SetBytesPerSecond(3000);
					SetBitsPerChannel(3);
					SetBytesPerSample(3);
				}
				else if (rate ==DEBIT_16KBS) {
					SetBytesPerSecond(2000);
					SetBitsPerChannel(2);
				}

			}
			else {
				SetBytesPerSecond(8000);
				SetBitsPerChannel(8);
			}

		return true;
}


#define Asc2hex(n) ((n <='9')?n-'0':(n<'A')?n-'A'+10:n-'a'+10)
#define Hex2Asc(a) (((a)<10)?(a)+'0':(a)+'a'-10)

bool WaveFile::ConvertFromCCitt(void)
{
	unsigned long i;
	int j;		// eba_
	const size_t transferBufSize = 66 + 1; // ligne de 64 car + 0x0D 0x0A
	const size_t convertBufSize = 64;

	if (!writeFile) {
		error = "Copy to an unopened file";
		return false;
	} else if (!CCittFile) {
		error = "Copy from an unopened file";
		return false;
	}

	try {
		// allocate the transfer buffer
		char* transferBuffer = new char[transferBufSize+1];
		char* convertBuffer = new char[convertBufSize+1];
		char car,ValRead,ValWrite;
		unsigned long bytesToRead = 0;
		unsigned long bytesToWrite = 0;
		dataLength=0;
		bool EndInit = false;

		try {
			if (fseek(CCittFile, 0, SEEK_SET)!=0)
				throw error = "Couldn't reset input file to start";

			while (!feof(CCittFile)) {

				// read the buffer
				// bytesToRead=fread(transferBuffer, 1, transferBufSize, CCittFile);
				fgets(transferBuffer,transferBufSize, CCittFile);

				bytesToRead = string(transferBuffer).length();

				if (bytesToRead != transferBufSize-1) {  //last line
					if (bytesToRead >4)
						bytesToRead-=2; // skip Checksum
					else
						bytesToRead=0;
					EndInit = true;
				}

// @BA@OXO_8.0@XTSce95780@070319@dpe@
				if (bytesToRead<2)
				  bytesToRead = 0;
				else
				  bytesToRead -= 2; // skip
// @EA@OXO_8.0@XTSce95780@070319@dpe@
// @BD@OXO_8.0@XTSce95780@070319@dpe@
// 				bytesToRead= bytesToRead<2?0:bytesToRead-=2; // skip
// @ED@OXO_8.0@XTSce95780@070319@dpe@

				// TRANSLATOR CCITT format -> wav Data format
				// CCitt line = 32 samples in Ascii code +
				ValWrite=0;

				for (/*int */i =0,j=0;i< bytesToRead;i+=2) {		// eba_
					car = Asc2hex(transferBuffer[i])*16;
					ValRead = car + Asc2hex(transferBuffer[i+1]);

					int k=bitsPerChannel;
					while (k++<8)
						ValRead <<=1;

					for (k=0;k<bitsPerChannel;k++) {    // On recompose bit a bit
						if (ValRead&0x80)
							ValWrite |= 1;					// chaque echantillon
						ValRead <<=1;
						if (++j==8) {                   // lorsqu'un octet est complet
								convertBuffer[bytesToWrite++]=ValWrite;
								j=0;
								ValWrite = 0;
						}
						ValWrite <<=1;
					}
				}

				if (bytesToWrite) {
					bytesToRead = bytesToWrite;
					bytesToWrite = (bytesToWrite/bytesPerSample)*bytesPerSample;
					dataLength += bytesToWrite;

					// write the buffer
					if (fwrite(convertBuffer, 1, bytesToWrite, writeFile) != bytesToWrite)
						throw error = "Error writing samples to output file";

					i=0;
					if (bytesToWrite < bytesToRead) {
						bytesToWrite++;
						while (bytesToWrite <= bytesToRead)
							convertBuffer[i++] = convertBuffer[bytesToWrite++];
					}

					bytesToWrite=i;
					changed = true;
				}
			}

			// delete the transfer buffer
			delete[] transferBuffer;
			delete[] convertBuffer;
		} catch (...) {
			delete[] transferBuffer;
			delete[] convertBuffer;
			throw error;
		}
	} catch (...) {
		return false;
	}

	return true;
}

bool WaveFile::ConvertToCCitt(int SamplesToSuppress)
{
	const size_t transferBufSize = 2*3*5; // pour etre sur d'avoir un nombre complet de samples
	const size_t convertBufSize = 66;

	if (!CCittFile) {
		error = "Copy to an unopened file";
		return false;
	} else if (!readFile) {
		error = "Copy from an unopened file";
		return false;
	}

	if (fseek(CCittFile, 0, SEEK_SET) != 0)
		return false;

	try {
		// allocate the transfer buffer
		char* transferBuffer = new char[transferBufSize];
		char* convertBuffer = new char[convertBufSize];

		unsigned long bytesToWrite = 0;
		unsigned long bytesRead = 0;
		char checksum= '\0';
		char ValRead=0;
		char ValWrite=0;

		try {
			size_t bytesToRead = 0, i;
			int j;

			if (!ResetToStart())
				throw error = "Couldn't reset input file to start";

			SamplesToSuppress = SamplesToSuppress * bitsPerChannel / 8;
			while (SamplesToSuppress && bytesRead < dataLength ) {
				// calculate the size of the next buffer
				bytesToRead = (size_t) min(transferBufSize,size_t(SamplesToSuppress));

				// read the buffer
				if (fread(transferBuffer, 1, bytesToRead, readFile->filep())!= bytesToRead)
					throw error = "Error reading samples from input file";

				bytesRead += bytesToRead;
				SamplesToSuppress -= bytesToRead;
			}

			while (bytesRead < dataLength) {
				// calculate the size of the next buffer
				size_t bytesToRead = (size_t) min(transferBufSize,
					size_t(dataLength - bytesRead));

				// read the buffer
				if (fread(transferBuffer, 1, bytesToRead, readFile->filep())!= bytesToRead)
					throw error = "Error reading samples from input file";


				// Convert 32 binary stream to 32 ASCII coded
				for (i =0,j=0;i< bytesToRead;i++) {
					ValRead =transferBuffer[i];

					int k=bitsPerChannel;
					if (k!=8) {
						for (int k=0;k<8;k++) {         // On recompose bit a bit
							if (ValRead&0x80)
								ValWrite |= 1;			// chaque echantillon
							ValRead <<=1;
							if (++j==bitsPerChannel) {
									convertBuffer[bytesToWrite++]
										= Hex2Asc((ValWrite>>4)&0x0F);
									convertBuffer[bytesToWrite++]
										= Hex2Asc(ValWrite&0x0F);
									//checksum ^= convertBuffer[(i*2)];
									//checksum ^= convertBuffer[(i*2)+1];
									j=0;
									ValWrite = 0;
							}
							ValWrite<<=1;
						}

					}
			     	else {
						convertBuffer[bytesToWrite++]= Hex2Asc((ValRead>>4)&0x0F);
						convertBuffer[bytesToWrite++]= Hex2Asc(ValRead&0x0F);
					}

					if (bytesToWrite >= 64) {
						// ADD �M
						convertBuffer[bytesToWrite++]= '\015';
						convertBuffer[bytesToWrite++]= '\012';
						// write the buffer
						if (fwrite(convertBuffer, 1, bytesToWrite, CCittFile)
									!= bytesToWrite)
							throw error = "Error writing samples to output file";
						bytesToWrite=0;
					}
				}
				bytesRead += bytesToRead;
			}
			if (bytesToWrite) {
						// ADD �M
				convertBuffer[bytesToWrite++]= '\015';
				convertBuffer[bytesToWrite++]= '\012';
						// write the buffer
				if (fwrite(convertBuffer, 1, bytesToWrite, CCittFile)
					!= bytesToWrite)
						throw error = "Error writing samples to output file";
				bytesToWrite=0;
			}

			convertBuffer[0]= Hex2Asc((checksum>>4)&0x0F);
			convertBuffer[1]= Hex2Asc(checksum&0x0F);
			convertBuffer[2]= '\015';
			convertBuffer[3]= '\012';
			bytesToWrite = 4;

			// write the buffer
			if (fwrite(convertBuffer, 1, bytesToWrite, CCittFile) != bytesToWrite)
					throw error = "Error writing samples to output file";

			// delete the transfer buffer
			delete[] transferBuffer;
			delete[] convertBuffer;

		} catch (...) {
			delete[] transferBuffer;
			delete[] convertBuffer;
			throw error;
		}
	} catch (...) {
		return false;
	}

	return true;
}

/***************************************************************************
	private member functions for WaveFile
***************************************************************************/

/***************************************************************************
	main()
***************************************************************************/

//#ifdef TEST_WAVE		// eba_

#include <iostream>

/*
static void reportProblem()
{
	cout << "  *** ERROR: Result incorrect." << endl;
}
*/

/*
static void checkResult(bool got, bool expected)
{
	if (got)
		cout << "success." << endl;
	else
		cout << "fail." << endl;

	if (got != expected)
		reportProblem();
}
*/

/*
static void pause()
{
	cout << "Press Enter to continue." << endl;
	cin.get();
}
*/

static void ShowErrors(WaveFile& from, WaveFile& to)
{
   print_debug("In showErrors");
	//bool any = from.GetError() || to.GetError();

	if (from.GetError())
		strncpy( global_main_error, from.GetError(), MAX_MAIN_ERROR_STRING_LENGTH );	// eba_
		//cout << "Error on input: " << from.GetError() << "." << endl;			// eba_

	if (to.GetError())
		strncpy( global_main_error, to.GetError(), MAX_MAIN_ERROR_STRING_LENGTH );	// eba_
		//cout << "Error on output: " << to.GetError() << "." << endl;			// eba_

	//if (!any)
	//	cout << "Success." << endl;
}

//generate warnings as not used
/* @BD@OXOC_3.2@CROXOC-4249@190325@degtoun1@ */
/*

static void ShowFormat(WaveFile& wave)
{
    cout
		<< "------------------------------------------------------" << endl
		<< "Format:           " << wave.GetFormatType()
		<< (wave.IsCompressed()? " (compressed)" : " (PCM)") << endl
		<< "Channels:         " << wave.GetNumChannels() << endl
		<< "Sample rate:      " << wave.GetSampleRate() << endl
		<< "Bytes per second: " << wave.GetBytesPerSecond() << endl
		<< "Bytes per sample: " << wave.GetBytesPerSample() << endl
		<< "Bits per channel: " << wave.GetBitsPerChannel() << endl
		<< "Bytes:            " << wave.GetDataLength() << endl
		<< "Samples:          " << wave.GetNumSamples() << endl
		<< "Seconds:          " << wave.GetNumSeconds() << endl
		<< "File pointer:     " << (wave.GetFile()? "good" : "null") << endl
		<< "------------------------------------------------------" << endl;
}

static void ShowExtraItem(WaveFile& wave)
{
	string type, value;
	if (wave.GetFirstExtraItem(type, value)) {
		cout << "Extra data:" << endl;
		do {
			cout << "  " << type << ": " << value << endl;
		} while (wave.GetNextExtraItem(type, value));

		wave.ResetToStart();
	    cout << "------------------------------------------------------" << endl;
	}
}

static void ShowRiffFormat(RiffFile* file)
{

		if (!file->filep()) {
			cout << "No RIFF form in file " << "." << endl;
		} else {
			file->rewind();
			showChunk(*file, 0, true);
		}
}

static void showCommand (string InFileName, string OutFileName,char convert,char law,char rate)
{
		cout
		  << "--------------------------------------------------------" << endl
		  << "Input File : " <<InFileName                               << endl
	      << "i : show Riff struct                                    " << endl
          << "f : show fmt                                            " << endl
          << "x : show eXtra Info                                     " << endl;

	if (OutFileName != "UNDEFINED") {
		cout
		  << "Input File : " <<InFileName << " will be "                 << endl;
		if (convert == ' ' ) {
		cout
		  << " Copied to wave  file " << OutFileName << endl
		  << " Copied to CCitt file " << OutFileName <<".ccitt" << endl;
		}
		else if (convert == 'd' ) {
		cout
		  << " Compressed to ADPCM wave  file " << OutFileName ;

		cout
		  << " rate " ;
			switch (rate) {
			case '1' : cout << "16kb/s" << endl; break;
			case '2' : cout << "24kb/s" << endl; break;
			case '4' : cout << "40kb/s" << endl; break;
			default  : cout << "32kb/s" << endl; break;
			}
		cout
		  << " then translated to CCitt file " << OutFileName <<".ccitt" << endl;
		}
		else {
		cout
		  << " UnCompressed to PCM wave  file " << OutFileName <<" law " ;
		if (law =='a')
						cout << "A " ;
		else            cout << "Mu ";

		cout << endl;
		cout
		  << " then translated to CCitt file " << OutFileName <<".ccitt" << endl;
		}


		cout
     	  << "--------------------------------------------------------" << endl;
		cout
		  << "CONVERT to a(d)pcm (p)cm  actual = " << convert  << endl
		  << "PCM law  (N)one (A) (M)u  actual = " << law      << endl;
		cout
		  << "r : change rate                    z : add  extra Info  " << endl
	      << "w : write wave output file         c : write CCitt file " << endl;
	}
		cout
	      << "t : New ouTput file                 q : quit            " << endl
     	  << "--------------------------------------------------------" << endl;
}
*/
/* @BD@OXOC_3.2@CROXOC-4249@190325@degtoun1@ */

#define showArg() {cout << "wave [-[b][amdp][i[wc]][o[wc]][r[1234][s####] FileName1 [filename2]." << endl;}
#define ConstRate(r) ((r=='1')?DEBIT_16KBS:(r=='2')?DEBIT_24KBS:(r=='4')?DEBIT_40KBS:DEBIT_32KBS)
#define PcmFormat(law) ((law=='a')?WAVE_FORMAT_ALAW:(law=='m')?WAVE_FORMAT_MULAW:WAVE_FORMAT_PCM)

/* @BA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */
int isfilexists(const char * filename){
    /* try to open file to read */
    FILE *file;
    if (file = fopen(filename, "r")){
        fclose(file);
        return 1;
    }
    return 0;
}

char *get_filename_n_path(char *str_entity, char *sender )
{
	int entity_num=0;
	static char destination_filename[26];

   memset(destination_filename, '\0', sizeof(destination_filename));
   entity_num = atoi(str_entity);

   if(strcmp(destination, "greetings") == 0)
   {
      if(entity_num>0 && entity_num <=20)
      {
         sprintf(destination_filename, "%s%03d%s", "/current/vmu/greet", entity_num,".wav");
      }
      else
      {
          print_debug( "entity_num out of range: %d",entity_num );
          print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
      }
   }
/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
   else if(strcmp(destination, "audiotexts") == 0 )
   {
      switch(entity_num)
      {
         case 1:/*AT greetings opening hour-atociergr.wav*/
            strcpy(destination_filename, "/current/vmu/atociegr.wav");

         break;
          case 2:/*AT greetings closing hour-atcciergr.wav*/
            strcpy(destination_filename, "/current/vmu/atcciegr.wav");
         break;
         case 3:/*AT greetings good bye-atgoodby.wav*/
            strcpy(destination_filename, "/current/vmu/atgoodby.wav");
         break;
         default:{
            print_debug("AT index out of range" );
            print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
            exit(0);
         }
      }
   }
   else if(strcmp(destination, "mailboxes") == 0 )
   {
      switch(entity_num)
      {
         case 1:/*galmbx audio-galmbxgr.wav*/
            strcpy(destination_filename, "/current/vmu/galmbxgr.wav");
         break;
          case 2:/*galmbxL-notification-vmoutgr.wav*/
            strcpy(destination_filename, "/current/vmu/vmuoutgr.wav");
         break;
         default:{
            print_debug("GALMBX index out of range" );
            print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
            exit(0);
         }
      }
   }
   else if(strcmp(destination, "aa") == 0)
   {
      switch(entity_num)
      {
         case 1:/*aa greetings opening hour-aaociegr.wav*/
            strcpy(destination_filename, "/current/vmu/aaociegr.wav");
         break;
          case 2:/*aa greetings closing hour-aacciegr.wav*/
            strcpy(destination_filename, "/current/vmu/aacciegr.wav");
         break;
         case 3:/*aa greetings good bye-aagoodby.wav*/
            strcpy(destination_filename, "/current/vmu/aagoodby.wav");
         break;
         default:{
            print_debug( "aa index out of range" );
            print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
            exit(0);
         }
      }
   }
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */
   else if(strcmp(destination, "moh") == 0 )
   {
      if(entity_num>0 && entity_num <5)
      {
         switch(entity_num)
         {
            case 1:strcpy(destination_filename,"/current/vmu/rec_moh.wav");
            break;
             case 2:strcpy(destination_filename,"/current/vmu/rec_moh2.wav");
             break;
             case 3:strcpy(destination_filename,"/current/vmu/rec_moh3.wav");
             break;
             case 4:strcpy(destination_filename,"/current/vmu/rec_moh4.wav");
             break;
         }
      }
      else
      {
          print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
      }
   }

   return destination_filename;
}

void get_audio_file_from_system(char *str_entity, char *sender)
{
   char filename[26];
   int fd;

   memset(filename, '\0', sizeof(filename));
   strcpy(filename,get_filename_n_path(str_entity, sender));

   if ((fd = open(filename, O_RDONLY)) == -1 )
   {
     print_debug( "Unable to open file %s", filename );
     print_api_error( API_ERR_IO, API_ERR_MISSING_FILE, sender);
   }
   else
   {
      close(fd);
      print_debug( "File path :%s written successfuly to stdout", filename);
      printf("{\"error\":%d, \"msg\":\"%s\"}", API_SUCCESS, filename);
   }
}

void delete_audio_file_from_system(char *str_entity, char *sender)
{
	char filename_path[64];
   char result[255] = { 0 };
	BOOLEAN status;
   FILE *fp = (FILE *) NULL;

	memset(filename_path, '\0', sizeof(filename_path));
	strcpy(filename_path,get_filename_n_path(str_entity, sender));

   if(isfilexists(filename_path))
   {
      status=remove(filename_path);//remove file from the disk
/* @BA@OXOC_3.2@CROXOC-6559@191031@degtoun1@ */
     if (status == 0)
	  {
         if(strcmp(destination, "greetings") == 0 )
            fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateGreetingsInfo","r");
         else if(strcmp(destination, "moh") == 0 )
            fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateMohInfo","r");
/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
         else if(strcmp(destination, "aa") == 0)
            fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateAAInfo","r");
         else if(strcmp(destination, "audiotexts") == 0 )
            fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateATInfo","r");
         else if(strcmp(destination, "mailboxes") == 0 )
            fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateGalMbxInfo","r");
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */

         if(fp != NULL)
         {
            if(fgets(result, sizeof(result), fp) != NULL)
               print_debug( "result of updating information =%s", result);

            pclose(fp);
         }
         else
            print_debug( "Error -> popen");

         print_debug( "File: %s removal succeeds", filename_path);
         print_api_error( API_SUCCESS, API_SUCCESS, sender );
     }
	  else
	  {
         print_debug( "remove file %s failed", filename_path);
         print_api_error( API_ERR_IO, API_ERR_REMOVING_FILE, sender );
	  }
/* @EA@OXOC_3.2@CROXOC-6559@191031@degtoun1@ */
   }
   else
   {
       print_debug( "file %s not exist", filename_path);
       print_api_error( API_ERR_IO, API_ERR_MISSING_FILE, sender );
   }
}

int put_audio_file_to_system(char *str_entity, char * sender)
{
	char convert = 'd';
	char rate = '3';
	WaveFile From, To;
	string OutFileName ="UNDEFINED";
	string InFileName  ="UNDEFINED";
   WaveFile tmpWaveFile;
   char result[255] = { 0 };
   FILE *fp;
	int entity_num;
	static char destination_filename[64];
	int fd;
	char expected_format_type = WAVE_FORMAT_G726_ADPCM;

	memset(destination_filename, '\0', sizeof(destination_filename));

	entity_num = atoi(str_entity);

	print_debug( "Open the STDIN and read the WAV file header" );
/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
   From.OpenRead("STDIN", sender, str_entity);
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */

	switch( From.GetFormatType() )
	{
		case WAVE_FORMAT_PCM: { print_debug( "The law is : LAW NONE" ); break;	} // PCM
		case WAVE_FORMAT_ALAW: { print_debug( "The law is : A LAW" ); break;	} // A LAW
		case WAVE_FORMAT_MULAW: { print_debug( "The law is : MU LAW" ); break;	} // MU LAW
		case WAVE_FORMAT_G726_ADPCM: { convert = ' '; print_debug("Format is already ADPCM, copy only the file"); break; }
		default: { print_debug( "Unknown wave format (use LAW NONE)" ); break; }
	}

	if (From.GetError()) {
      print_debug( "An error happened" );

		   ShowErrors(From, To);
         if(strcmp(sender, "webapp")!=0)
            print_api_error( API_ERR_INTERNAL, API_ERR_INT_MAIN_ERROR, sender);
         else
            print_error( ERR_INTERNAL, ERR_INT_MAIN_ERROR, sender);

		return 0;
	}
       print_debug( "Open the WAV output file" );

      if(strcmp(destination, "greetings") == 0)
      {
         sprintf(destination_filename, "%s%03d%s", "/current/vmu/greet", entity_num,".wav");
         print_debug( "IF greetings: %s", destination_filename);
         OutFileName=destination_filename;
      }
/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
      else if(strcmp(destination, "audiotexts") == 0 )
      {
         switch(entity_num)
         {
            case 1:/*AT greetings opening hour-atociegr.wav*/
               OutFileName = "/current/vmu/atociegr.wav";
            break;
             case 2:/*AT greetings closing hour-atcciegr.wav*/
               OutFileName = "/current/vmu/atcciegr.wav";
            break;
            case 3:/*AT greetings good bye-atgoodby.wav*/
               OutFileName = "/current/vmu/atgoodby.wav";
            break;
            default:{
               print_debug("AT index out of range" );
               print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
               exit(0);
            }
         }
      }
      else if(strcmp(destination, "mailboxes") == 0 )
      {
         switch(entity_num)
         {
            case 1:/*GAL audio-galmbxgr.wav*/
               OutFileName = "/current/vmu/galmbxgr.wav";
            break;
             case 2:/*GAL-notification-vmoutgr.wav*/
               OutFileName = "/current/vmu/vmuoutgr.wav";
            break;
            default:{
               print_debug("GalMbx index out of range" );
               print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
               exit(0);
            }
         }

      }
      else if(strcmp(destination, "aa") == 0 )
      {
         switch(entity_num)
         {
            case 1:/*AA greetings opening hour-aaociegr.wav*/
               OutFileName = "/current/vmu/aaociegr.wav";
            break;
             case 2:/*AA greetings closing hour-aacciegr.wav*/
               OutFileName = "/current/vmu/aacciegr.wav";
            break;
            case 3:/*AA greetings good bye-aagoodby.wav*/
               OutFileName = "/current/vmu/aagoodby.wav";
            break;
            default:{
               print_debug("AA index out of range" );
               print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
               exit(0);
            }
         }
      }
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */
      else if(strcmp(destination, "moh") == 0 )
      {
         switch(entity_num)
         {
            case 1:OutFileName = "/current/vmu/rec_moh.wav";
            break;
            case 2:OutFileName = "/current/vmu/rec_moh2.wav";
            break;
            case 3:OutFileName = "/current/vmu/rec_moh3.wav";
            break;
            case 4:OutFileName = "/current/vmu/rec_moh4.wav";

            break;
            default:{
               print_debug( "MOH index out of range" );
               print_api_error(API_ERR_POST, API_ERR_POST_ENTITY_NUM, sender);
               exit(0);
            }
         }
      }
      else if((strlen(destination)==0) && strcmp(sender, "webapp")==0)
      {//webapp
         switch(entity_num)
         {
            case 1:OutFileName = "/current/vmu/rec_moh.wav";
            break;
            case 2:OutFileName = "/current/vmu/rec_moh2.wav";
            break;
            case 3:OutFileName = "/current/vmu/rec_moh3.wav";
            break;
            case 4:OutFileName = "/current/vmu/rec_moh4.wav";
            break;
            default:{
               print_debug( "MOH index out of range" );
               print_error(ERR_POST, ERR_POST_ENTITY_NUM, sender);
               exit(0);
            }
         }
      }
      else
      {
         print_debug( "Unknown feature in put_audio");

         if(strcmp(sender, "api")==0)
            print_api_error(API_ERR_POST, API_ERR_POST_UNKNOWN_FEATURE, sender );
         else
            print_error(ERR_POST, ERR_POST_UNKNOWN_FEATURE, sender );
      }
      print_debug( "Copy the WAV attributes" );
      print_debug("Conversion required : %s dest=%s",OutFileName.c_str(), destination);

         if(convert != ' ')
         {
            if(strcmp(destination, "moh") == 0 )
            {
/* @BA@OXOC_3.2@CROXOC-6928@191219@degtoun1@ */
               tmpWaveFile.CopyFormatFrom(From);
               string tmpFileName = "/current/vmu/tmpMoh.wav";

               print_debug( "Write the WAV output file" );
               tmpWaveFile.OpenWrite(tmpFileName.c_str());
               if (!tmpWaveFile.GetError())
               {
                  tmpWaveFile.CopyFrom(From);
               }
               tmpWaveFile.Close();

               //using ffmpeg makes it possible to reduce the latency from 42s to 11s
               From.ConvertFromADPCM(tmpFileName.c_str(), OutFileName.c_str());
               //we do produce the expected ADPCM 4 bits 8Khz - Mono file using ffmpeg, but the file type is unknown.
               //consequently it is refused by the system
               //forcing the type name to ADPCM(G726) it is accepted and playable by the system
               /*
               v0001a01.wav => OK
               ---------------------
               ChunkID:         RIFF
               ChunkSize:       8484
               Format:          WAVE
               --------------------------------
               ChunkID:         fmt
               ChunkSize:       18
               Audio format:    100  (G726 ADPCM)
               Channels:        1  (Mono)
               Sample rate:     8000  (8 kHz)
               Byte rate:       4000
               Bloc align:      1
               Bits per Sample: 4 bits
               Extra size:      0

               --------------------------------
               ChunkID:         data
               ChunkSize:       8445

               rec_moh.wav ==> NOK regarding to Audio format:    69  (Unknown)
               ----------------------------------------------------------------

               ChunkID:         RIFF
               ChunkSize:       25650
               Format:          WAVE
               --------------------------------
               ChunkID:         fmt
               ChunkSize:       18
               Audio format:    69  (Unknown)   ==> the modification below is to write the audio format=100 (G726 ADPCM) and it is working fine
               Channels:        1  (Mono)
               Sample rate:     8000  (8 kHz)
               Byte rate:       4000
               Bloc align:      1
               Bits per Sample: 4 bits
               Extra size:      0

               --------------------------------
               ChunkID:         fact
               ChunkSize:       4
               */
               if ((fd = open(OutFileName.c_str(), O_RDWR)) != -1 )
               {
                  lseek(fd, 20, SEEK_SET);
                  write(fd, &expected_format_type, 1);
                  close(fd);
               }
/* @EA@OXOC_3.2@CROXOC-6928@191219@degtoun1@ */

               fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateMohInfo","r");

               if(fp != NULL)
               {
                  if(fgets(result, sizeof(result), fp) != NULL)
                     print_debug( "result of updating information=%s", result);

                  pclose(fp);
               }
               else
                  print_debug( "Error -> popen");


               remove(tmpFileName.c_str());

               if (To.GetError())
                     ShowErrors(From, To);

               if (!To.GetError())
               {
                  fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateGreetingsInfo","r");

                  if(fp == NULL)
                  {
                     if (fgets(result, sizeof(result), fp) != NULL)
                        print_debug( "result of updating information =%s", result);

                      pclose(fp);
                  }
                  else
                     print_debug( "Error -> popen");

               }
               else
               {
                  ShowErrors(From, To);
                  if(strcmp(sender, "api")==0)
                  {
                     strcpy( global_main_error, "Output file error" );
                     print_api_error( API_ERR_INTERNAL, API_ERR_INT_MAIN_ERROR, sender );
                  }
                  else
                  {
                     strcpy( global_main_error, "Output file error" );
                     print_error(ERR_INTERNAL, ERR_INT_MAIN_ERROR, sender );
                  }

               }
            }
            else if(strcmp(destination, "greetings") == 0 ||
               strcmp(destination, "aa") == 0 ||
               strcmp(destination, "audiotexts") == 0 ||
               strcmp(destination, "mailboxes") == 0)
            {
               print_debug( "Greetings: conversion required to PCM format");
               print_debug( "Copy the WAV attributes" );
               tmpWaveFile.CopyFormatFrom(From);
               string tmpFileName = "/current/vmu/tmpMoh.wav";
               print_debug( "Write the WAV output file" );
               tmpWaveFile.OpenWrite(tmpFileName.c_str());

               if (!tmpWaveFile.GetError())
               {
                  tmpWaveFile.CopyFrom(From);
               }
               tmpWaveFile.Close();
               From.ConvertFromPCM(tmpFileName.c_str(), OutFileName.c_str());
               remove(tmpFileName.c_str());

              if(strcmp(destination, "greetings") == 0 )
                 fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateGreetingsInfo","r");
              else if(strcmp(destination, "moh") == 0)
                 fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateMohInfo","r");
/* @BA@OXO@OXOCDEV40-100@200113@degtoun1@ */
              else if(strcmp(destination, "aa") == 0)
                 fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateAAInfo","r");
              else if(strcmp(destination, "audiotexts") == 0)
                 fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateATInfo","r");
              else if(strcmp(destination, "mailboxes") == 0)
                 fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateGalMbxInfo","r");
/* @EA@OXO@OXOCDEV40-100@200113@degtoun1@ */

              if(fp != NULL)
              {
                 if(fgets(result, sizeof(result), fp) != NULL)
                    print_debug( "result of updating information =%s", result);

                 pclose(fp);
              }
              else
                 print_debug( "Error -> popen");

            }
         }
         else
         {//ADPCM
            print_debug("ADPCM already");
            To.CopyFormatFrom(From);
            To.OpenWrite(OutFileName.c_str());
            To.CopyFrom(From);
            print_debug( "MOH ADPCM files: calling the plugin to update moh recorded files");
            fp = popen("/usr/bin/wdt restapi_audio_files_mgt updateGreetingsInfo","r");

            if(fp != NULL)
            {
               if(fgets(result, sizeof(result), fp) != NULL)
                  print_debug( "result of updating information =%s", result);

               pclose(fp);
            }
            else
               print_debug( "Error -> popen");
         }
   print_debug( "Close all" );
   To.Close();
   From.Close();
   ShowErrors(From, To);

   if(strcmp(sender, "api")==0)
      print_api_error( API_SUCCESS, API_SUCCESS, sender );
   else
      print_error( SUCCESS, SUCCESS, sender );

	return 0;
}

/*wave.cgi argments:
   - wave.cgi line argument
      api or nothing
   - Expected PHP POST parameters:
      operation (PUT/GET/DELETE)
      destination: MOH or Pre-announcement(greetings) feature
      Entity(resource):  fileData
 */

int main(int argc, const char* argv[])
{
   static char sender[8];
	static char str_entity[4];
	static char oper[7];

   memset(str_entity, '\0', sizeof(str_entity));
   memset(oper, '\0', sizeof(oper));
   memset(sender, '\0', sizeof(sender));

  if (argc ==1) {//WebApp
		print_debug( "Web based Tool request");
      strcpy(sender, "webapp");
	}
	else if (argc==2)
	{
		print_debug( "API REST request");

		strcpy(sender, trim((char*)argv[1], NULL));
      if(strcmp(sender, "api")!=0)
      {
            print_debug( "Unknown: argument=%s", argv[1]);
            exit(1);
      }
	}
	else
	{
		print_debug( "CGI Argument missing");
		exit(1);
	}

   if(strcmp(sender, "api")==0)
   {
      strcpy(oper, get_api_operation(sender));
      strcpy(str_entity, mainProg1(sender, oper));

      if(strcmp(oper, "PUT")==0)
      {
         put_audio_file_to_system(str_entity, sender);
      }
      else if(strcmp(oper, "GET")==0)
      {
         get_audio_file_from_system(str_entity, sender);
      }
      else if(strcmp(oper, "DELETE")==0)
      {
          delete_audio_file_from_system(str_entity, sender);
      }
      else
      {
		   print_api_error( API_ERR_POST, API_ERR_POST_UNKNOWN_OPERATION, sender );
      }
   }
   else
   {//WebApp
      strcpy(str_entity,mainProg1(sender, "PUT"));
      strcpy(destination, "moh");
      put_audio_file_to_system(str_entity, /*"moh",*/ sender);
   }

   return 0;
}
/* @EA@OXOC_3.2@CROXOC-4249@190322@degtoun1@ */


